//
//  CourseMapViewController.m
//  Frolfer
//
//  Created by Drew Bombard on 11/3/12.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import "CourseMapViewController.h"

@interface CourseMapViewController ()

@end

@implementation CourseMapViewController

#define METERS_PER_MILE 1609.344


#pragma mark - View lifecycle

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}
- (void)viewDidUnload {
	_mapView = nil;
	[super viewDidUnload];
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	CLLocationCoordinate2D zoomLocation;
	zoomLocation.latitude = 39.281516;
	zoomLocation.longitude= -76.580806;
 
	MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5 * METERS_PER_MILE, 0.5 * METERS_PER_MILE);
	
	[_mapView setRegion:viewRegion animated:YES];
}
- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];

	[self plotCourseData];
}
- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
}
- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
}
- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Release any cached data, images, etc that aren't in use.
}


#
#pragma mark

-(void)plotCourseData {
	for (id<MKAnnotation> annotation in _mapView.annotations) {
		[_mapView removeAnnotation:annotation];
	}
	
	NSNumber *latitude = [NSNumber numberWithInteger: [_locLat integerValue]];
	NSNumber *longitude = [NSNumber numberWithInteger: [_locLong integerValue]];
	NSString *address = _locCityState;
 
	CLLocationCoordinate2D coordinate;
	coordinate.latitude = latitude.doubleValue;
	coordinate.longitude = longitude.doubleValue;

	CourseLocation *annotation = [[CourseLocation alloc]
								  initWithName:_courseName
								  address:address coordinate:coordinate] ;
	[_mapView addAnnotation:annotation];
}


-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
	
	static NSString *identifier = @"CourseLocation";
	
	if ([annotation isKindOfClass:[CourseLocation class]]) {
//		MKAnnotationView *annotationView = (MKAnnotationView *) [_mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
		
		MKPinAnnotationView *annotationView = (MKPinAnnotationView *) [_mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
		
		
		if (annotationView == nil) {
			annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
		} else {
			annotationView.annotation = annotation;
		}
		
		annotationView.enabled = YES;
		annotationView.canShowCallout = YES;
//		annotationView.animatesDrop = YES;
		
		// Add to mapView:viewForAnnotation: after setting the image on the annotation view
		annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
		
		return annotationView;
	}
 
	return nil;
}


- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
	CourseLocation *location = (CourseLocation*)view.annotation;
	
	NSLog(@"location: %@", location);
	
	double destinationLatitude, destinationLongitude;
	destinationLatitude = location.coordinate.latitude;
	destinationLongitude = location.coordinate.longitude;
	
	CLLocationCoordinate2D coordinate =	CLLocationCoordinate2DMake(destinationLatitude,destinationLongitude);
	
	MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coordinate addressDictionary:nil];
	MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
	[mapItem setName:_courseName];
	
	// Set the directions mode to "Driving"
	// Can use MKLaunchOptionsDirectionsModeDriving instead
	NSDictionary *launchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving};
	
	// Get the "Current User Location" MKMapItem
	MKMapItem *currentLocationMapItem = [MKMapItem mapItemForCurrentLocation];
	
	// Pass the current location and destination map items to the Maps app
	// Set the direction mode in the launchOptions dictionary
	[MKMapItem openMapsWithItems:@[currentLocationMapItem, mapItem] launchOptions:launchOptions];
}



//-(void)plotCoursePosition {
//
//	for (id<MKAnnotation> annotation in _mapView.annotations) {
//		[_mapView removeAnnotation:annotation];
//	}
//		
//	CLLocationCoordinate2D coordinate;
//	coordinate.latitude = [_locLat doubleValue];
//	coordinate.longitude = [_locLong doubleValue];
//	CourseLocation *annotation = [[CourseLocation alloc]
//							  initWithName:_courseName
//							  address:_locCityState
//							  coordinate:coordinate];
//	
//	[_mapView addAnnotation:annotation];
//}

@end
