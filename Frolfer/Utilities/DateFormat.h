//
//  DateFormat.h
//  DiscGolf+
//
//  Created by Drew Bombard on 4/6/17.
//  Copyright © 2017 default_method. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateFormat : NSObject

+(NSString *) getLocale;
+(NSString *) getLanguage;
+(NSString *) getTodayDate;
+(NSString *) getCurrentYear;

@end
