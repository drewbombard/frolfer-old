//
//  IntroPages.m
//  Frolfer
//
//  Created by Drew Bombard on 4/17/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import "IntroPages.h"

@interface IntroPages ()

@end

@implementation IntroPages {
	NSArray *myViewControllers;
}

-(void)viewDidLoad {
	[super viewDidLoad];
	
	self.delegate = self;
	self.dataSource = self;
	
	UIViewController *p1 = [self.storyboard
							instantiateViewControllerWithIdentifier:@"Intro1ID"];
	UIViewController *p2 = [self.storyboard
							instantiateViewControllerWithIdentifier:@"Intro2ID"];
	UIViewController *p3 = [self.storyboard
							instantiateViewControllerWithIdentifier:@"Intro3ID"];
	UIViewController *p4 = [self.storyboard
							instantiateViewControllerWithIdentifier:@"Intro4ID"];
	
	for (UIGestureRecognizer *recognizer in self.gestureRecognizers) {
		recognizer.enabled = YES;
	}
	
	myViewControllers = @[p1,p2,p3,p4];
	
	[self setViewControllers:@[p1]
				   direction:UIPageViewControllerNavigationDirectionReverse
					animated:YES completion:nil];
	
	NSLog(@"loaded!");
}

-(UIViewController *)viewControllerAtIndex:(NSUInteger)index {
	return myViewControllers[index];
}

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
	NSUInteger currentIndex = [myViewControllers indexOfObject:viewController];
	
	--currentIndex;
	currentIndex = currentIndex % (myViewControllers.count);
	return [myViewControllers objectAtIndex:currentIndex];
}

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
	NSUInteger currentIndex = [myViewControllers indexOfObject:viewController];
	
	++currentIndex;
	currentIndex = currentIndex % (myViewControllers.count);
	return [myViewControllers objectAtIndex:currentIndex];
}

-(NSInteger)presentationCountForPageViewController: (UIPageViewController *)pageViewController {
	return myViewControllers.count;
}

-(NSInteger)presentationIndexForPageViewController: (UIPageViewController *)pageViewController {
	return 0;
}

@end
