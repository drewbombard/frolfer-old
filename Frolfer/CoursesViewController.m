//
//  CoursesViewController.m
//  Frolfer
//
//  Created by Drew Bombard on 4/1/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import "CoursesViewController.h"

@interface CoursesViewController ()

@end

@implementation CoursesViewController


- (void)viewDidLoad {
	
	[super viewDidLoad];
	// Do any additional setup after loading the view.
		
	_appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [_appDelegate managedObjectContext];
	[context setUndoManager:nil];
	_managedObjectContext = context;


	
	_lblLocalStateLong.text = _selectedState;
	_imgLocalState.image = [UIImage imageNamed: [_selectedStateAbbreviation lowercaseString]];

	NSLog(@"_selectedStateAbbreviation: %@",_selectedStateAbbreviation);

	[self customizeInterface];
}


-(void)viewWillAppear:(BOOL)animated
{	
	[super viewWillAppear:animated];
	
	NSError *error;
	if (![[self fetchedResultsController] performFetch:&error]) {
		// Update to handle the error appropriately.
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		exit(-1);  // Fail
	}

	if (_fetchedResultsController != nil) {
		_lblCourseCount.text = [NSString stringWithFormat:@"%lu", (unsigned long)_fetchedResultsController.fetchedObjects.count];
	} else {
		_lblCourseCount.text = @"- -";
	}
}


- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
	self.fetchedResultsController = nil;
}

-(void)customizeInterface { }







#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	// Return the number of sections.
	return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
	
	if ([[_fetchedResultsController sections] count] > 0) {
		id <NSFetchedResultsSectionInfo> sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
		return [sectionInfo numberOfObjects];
	} else {
		return 0;
	}
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
	return [sectionInfo name];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
	return [self.fetchedResultsController sectionIndexTitles];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
	return [self.fetchedResultsController sectionForSectionIndexTitle:title atIndex:index];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *simpleTableIdentifier = @"stateCell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
	
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
	}
	
	// Configure the cell...
	Courses *course_info = [_fetchedResultsController objectAtIndexPath: indexPath];
	cell.textLabel.text = course_info.courseName;
	
	return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSLog(@"\n didSelectRowAtIndexPath ()\n");
}




#pragma makr - Search methods
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope {
	NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchText];
	searchResults = [courses filteredArrayUsingPredicate:resultPredicate];
}



#pragma mark - Table data setup
#pragma mark - Data Retreival (fetchedResultsController)
-(NSFetchedResultsController *)fetchedResultsController {
	
	if (_fetchedResultsController != nil) {
		return _fetchedResultsController;
	}
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription
								   entityForName:@"Courses" 
								   inManagedObjectContext:_managedObjectContext];
	[fetchRequest setEntity:entity];
	
	// Sort it
	NSSortDescriptor *sort = [[NSSortDescriptor alloc]
							  initWithKey:@"courseName"
							  ascending:YES
							  selector:@selector(localizedCaseInsensitiveCompare:)];
	
	[fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
	[fetchRequest setFetchBatchSize:20];

	// Filter it
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"locState==%@",_selectedState];
	
	// Setup and fetch
	NSFetchedResultsController *theFetchedResultsController =
	[[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
										managedObjectContext:_managedObjectContext
										  sectionNameKeyPath:@"courseSectionHead"
												   cacheName:nil];
	
	// Fetch it and set results
	self.fetchedResultsController = theFetchedResultsController;
	_fetchedResultsController.delegate = self;
	
	
	return _fetchedResultsController;
}



#pragma mark - View Transitions

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	
	//NSLog(@"Let's move, we are in the segue....");
	NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
	
	
	if ([segue.identifier isEqualToString:@"showCourseDetail"]) {
		
		Courses *selectedCourse = [_fetchedResultsController objectAtIndexPath: indexPath];
		
		CourseDetailVC *courseDetailsVC = segue.destinationViewController;
		courseDetailsVC.selectedCourse = selectedCourse;
		
	} else {
		NSLog(@"Unidentified Segue Attempted!");
	}
}

@end
