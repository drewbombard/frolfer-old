//
//  CourseLocation.h
//  Frolfer
//
//  Created by Drew Bombard on 11/4/12.
//  Copyright (c) 2015 default_method All rights reserved.
//

#import "CourseLocation.h"

@implementation CourseLocation


- (id)initWithName:(NSString*)name address:(NSString*)address coordinate:(CLLocationCoordinate2D)coordinate {
	if ((self = [super init])) {
		if ([name isKindOfClass:[NSString class]]) {
			_name = name;
		} else {
			_name = @"Unknown course";
		}
		_address = address;
		_theCoordinate = coordinate;
	}
	return self;
}

- (NSString *)title {
	if ([_name isKindOfClass:[NSNull class]])
		return @"Unknown charge";
	else
		return _name;
}

- (NSString *)subtitle {
	return _address;
}

- (CLLocationCoordinate2D)coordinate {
	return _theCoordinate;
}

- (MKMapItem*)mapItem {


	
		
	NSDictionary *addressDict = @{(NSString*)CNPostalAddressStreetKey : _address};
 
	MKPlacemark *placemark = [[MKPlacemark alloc]
							  initWithCoordinate:self.coordinate
							  addressDictionary:addressDict];
 
	MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
	mapItem.name = self.title;
 
	return mapItem;
}






@end
