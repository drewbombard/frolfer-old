//
//  ScoringCell.h
//  Frolfer
//
//  Created by Drew Bombard on 2/22/13.
//  Copyright (c) 2015 default_method. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "AppDelegate.h"

// Data
#import "Games+CoreDataClass.h"
#import "Players+CoreDataClass.h"
#import "Scoring.h"


@interface ScoringCell : UITableViewCell

// Visual

@property (strong, nonatomic) IBOutlet UILabel *lblPlayerName;
@property (strong, nonatomic) IBOutlet UILabel *lblPlayerNickName;
@property (strong, nonatomic) IBOutlet UILabel *lblPlayerScore;
@property (strong, nonatomic) IBOutlet UIImageView *imgPlayerPhoto;
@property (strong, nonatomic) IBOutlet UIImageView *imgLeadPlayerCheck;

@property (strong, nonatomic) NSIndexPath *btnIndexPath;
@property (weak, nonatomic) IBOutlet UIButton *btnIncreaseScore;
@property (weak, nonatomic) IBOutlet UIButton *btnDecreaseScore;

@end
