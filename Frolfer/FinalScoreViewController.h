//
//  FinalScoreViewController.h
//  Frolfer
//
//  Created by Drew Bombard on 1/23/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScoringCompleteCell.h"

// Data
#import "CoreDataCheck.h"
#import "FetchDataArray.h"
#import "Players+CoreDataClass.h"
#import "Scoring.h"
#import "Courses.h"

// Utilities
#import "Colors.h"
#import "FetchDataArray.h"
#import "GameStats.h"
#import "DeleteData.h"


@interface FinalScoreViewController : UIViewController <
										UITableViewDataSource,
										UITableViewDelegate,
										UIAlertViewDelegate,
										NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) IBOutlet UIButton *btnStartOver;
- (IBAction)startOver:(id)sender;

@property NSUInteger pageIndex;
@property (strong,nonatomic) IBOutlet UITableView* tableView;

@property (strong, nonatomic) IBOutlet UIImageView *imgPlayer;
@property (weak, nonatomic) IBOutlet UILabel *lblPlayerName;
@property (weak, nonatomic) IBOutlet UILabel *lblPlayerNickName;

@property (strong, nonatomic) IBOutlet UILabel *lblParLow;
@property (strong, nonatomic) IBOutlet UILabel *lblParAverage;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalPoints;

@property (strong, nonatomic) IBOutlet UIView *winnerCard;

@property (assign, nonatomic) int totalHoles;

@property (strong, nonatomic) AppDelegate *appDelegate;

@property (strong, nonatomic) NSMutableDictionary *gameStatsDict;
@property (strong, nonatomic) Games *game_info;
@property (strong, nonatomic) NSMutableArray *gameDataArray;


@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;


@end
