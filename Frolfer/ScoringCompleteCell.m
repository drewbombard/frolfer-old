//
//  ScoringCompleteCell.m
//  Frolfer
//
//  Created by Drew Bombard on 10/22/14.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import "ScoringCompleteCell.h"

@implementation ScoringCompleteCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self) {
		// Initialization code
	
		NSLog(@"do something...!");
		NSLog(@"\n");

	}
	return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	[super setSelected:selected animated:animated];

	// Configure the view for the selected state
	
	AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	
	CGRect lblRectPlayerScore = _lblPlayerScore.frame;
	lblRectPlayerScore.origin.x = appDelegate.screenWidth - (_lblPlayerScore.frame.size.width + 10);
	_lblPlayerScore.frame = lblRectPlayerScore;
}

@end
