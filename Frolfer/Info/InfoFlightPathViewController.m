//
//  InfoFlightPathViewController.m
//  Frolfer
//
//  Created by Drew Bombard on 4/14/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import "InfoFlightPathViewController.h"

@interface InfoFlightPathViewController ()

@end

@implementation InfoFlightPathViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	
	[self customizeInterface];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

-(void)customizeInterface {
	
	self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
}

@end
