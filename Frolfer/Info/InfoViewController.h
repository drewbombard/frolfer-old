//
//  InfoViewController.h
//  Frolfer
//
//  Created by Drew Bombard on 8/21/13.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>


// Utilities
#import "AppStats.h"
#import "DateFormat.h"

@interface InfoViewController : UITableViewController <MFMailComposeViewControllerDelegate>


@property (strong, nonatomic) IBOutlet UIImageView *imgLogo;
@property (strong, nonatomic) IBOutlet UILabel *lblCopyright;
@property (strong, nonatomic) IBOutlet UILabel *lblVersionNum;



-(IBAction)openMail:(id)sender;
-(IBAction)linkTwitter:(id)sender;
-(IBAction)linkFacebook:(id)sender;
-(IBAction)linkWebsite:(id)sender;



@end
