//
//  InfoStabilityViewController.h
//  Frolfer
//
//  Created by Drew Bombard on 4/13/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoStabilityViewController : UIViewController


@property (strong, nonatomic) NSURL *url;
@property (strong, nonatomic) NSString *htmlFile;
@property (strong, nonatomic) NSString *htmlString;
@property (strong, nonatomic) IBOutlet UIWebView *webview;

@end
