//
//  InfoRimTypesViewController.h
//  Frolfer
//
//  Created by Drew Bombard on 4/12/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoRimTypesViewController : UIViewController

@property (strong, nonatomic) NSURL *url;
@property (strong, nonatomic) NSString *htmlFile;
@property (strong, nonatomic) NSString *htmlFileName;
@property (strong, nonatomic) NSString *htmlString;
@property (strong, nonatomic) IBOutlet UIWebView *webview;

@end
