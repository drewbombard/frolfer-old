//
//  InfoCourseViewController.m
//  Frolfer
//
//  Created by Drew Bombard on 4/14/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import "InfoCourseViewController.h"

@interface InfoCourseViewController ()

@end

@implementation InfoCourseViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	
	[self customizeInterface];
	
	_url =[[NSBundle mainBundle] bundleURL];
	_htmlFile = [[NSBundle mainBundle] pathForResource:@"course" ofType:@"html"];
	_htmlString = [NSString stringWithContentsOfFile:_htmlFile encoding:NSUTF8StringEncoding error:nil];
	
	self.webview.delegate = self;
	[_webview loadHTMLString:_htmlString baseURL:_url];
}

-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
	if ( inType == UIWebViewNavigationTypeLinkClicked ) {
		[[UIApplication sharedApplication] openURL:[inRequest URL]];
		return NO;
	}
	return YES;
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

-(void)customizeInterface {
	self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
}

@end
