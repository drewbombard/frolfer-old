//
//  InfoPlasticsViewController.m
//  Frolfer
//
//  Created by Drew Bombard on 4/13/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import "InfoPlasticsViewController.h"

@interface InfoPlasticsViewController ()

@end

@implementation InfoPlasticsViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	
	[self customizeInterface];
	
	_url =[[NSBundle mainBundle] bundleURL];
	_htmlFile = [[NSBundle mainBundle] pathForResource:@"plasticTypes" ofType:@"html"];
	_htmlString = [NSString stringWithContentsOfFile:_htmlFile encoding:NSUTF8StringEncoding error:nil];
}

-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];

	[_webview loadHTMLString:_htmlString baseURL:_url];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}


-(void)customizeInterface {	
	self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
}

@end
