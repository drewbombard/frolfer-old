//
//  InfoViewController.m
//  Frolfer
//
//  Created by Drew Bombard on 8/21/13.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import "InfoViewController.h"

@interface InfoViewController ()

@end

@implementation InfoViewController


- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

	
	[self customizeInterface];
	_lblVersionNum.text = [AppStats versionNumberWithName];
	_lblCopyright.text = [NSString stringWithFormat: @"© %@ Default Method, LLC", [DateFormat getCurrentYear]];
	
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)customizeInterface {
	
	_imgLogo.image = [_imgLogo.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
	_imgLogo.tintColor = [[Colors get] darkBlue];
	
	// Disable the inset that iOS 7 adds by default
//	self.automaticallyAdjustsScrollViewInsets = NO;
	
	//[UINavigationBar appearance].backgroundColor = [UIColor greenColor];
}


-(void)viewWillAppear:(BOOL)animated {
	
	[super viewWillAppear:animated];
	
	// Google Analytics
//	id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//	[tracker set:kGAIScreenName value:@"Info"];
//	[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}


- (IBAction)linkTwitter:(id)sender {
	[self openExternalLink:@"https://twitter.com/default_method"];
}

- (IBAction)linkFacebook:(id)sender {
	[self openExternalLink:@"https://www.facebook.com/defaultmethod/"];
}

-(IBAction)linkWebsite:(id)sender {
	[self openExternalLink:@"http://www.defaultmethod.com"];
}

-(IBAction)linkAppStore:(id)sender
{
	[self openExternalLink:@"https://itunes.apple.com/us/app/discgolf/id988392294?ls=1&mt=8"];
}
- (IBAction)linkPrivacy:(id)sender
{
	[self openExternalLink:@"http://www.defaultmethod.com/privacy.html"];
}


-(void)openExternalLink:(NSString *)externalLink {	
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString: externalLink]];
}



-(IBAction)tellYourFriends:(id)sender {
	
	NSMutableArray *sharingItems = [NSMutableArray new];

	NSString *share_text = @"Hey, you should checkout DiscGolf+... it's pretty sweet.\n";
	NSString *share_url = @"http://www.defaultmethod.com";
	UIImage *share_img = [UIImage imageNamed:@"dm_logo"];
	
	[sharingItems addObject: share_text];
	[sharingItems addObject: share_url];
	[sharingItems addObject: share_img];
	

	UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];

	
	[self presentViewController:activityController animated:YES completion:nil];
}

-(IBAction)openMail:(id)sender {
	
	if ([MFMailComposeViewController canSendMail]) {
		MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
		
		mailer.mailComposeDelegate = self;
		
		[mailer setSubject:@"Feedback"];
		
		NSArray *toRecipients = [NSArray arrayWithObjects:@"support@defaultmethod.com", nil];
		[mailer setToRecipients:toRecipients];
		
//		UIImage *myImage = [UIImage imageNamed:@"dm_logo.png"];
//		NSData *imageData = UIImagePNGRepresentation(myImage);
//		[mailer addAttachmentData:imageData mimeType:@"image/png" fileName:@"mobiletutsImage"];
		
		//NSString *emailBody = @"Have a question or comment?\n\n";
		NSString *emailBody = @"";
		[mailer setMessageBody:emailBody isHTML:NO];
		
		[self presentViewController:mailer animated:YES completion:nil];
		
	} else {
				
		UIAlertController *alert = [UIAlertController
								 alertControllerWithTitle:@"Failure"
								 message:@"Your device doesn't support the composer sheet"
								 preferredStyle:UIAlertControllerStyleAlert];

		[self presentViewController:alert animated:YES completion:nil];

		
		UIAlertAction *cancelAction = [UIAlertAction
									   actionWithTitle:NSLocalizedString(@"OK", @"Cancel action")
									   style:UIAlertActionStyleCancel
									   handler:^(UIAlertAction *action)
									   {
										   NSLog(@"Cancel action");
									   }];

		[alert addAction:cancelAction];
	}
 
}


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	switch (result)
	{
		case MFMailComposeResultCancelled:
			NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
			break;
		case MFMailComposeResultSaved:
			NSLog(@"Mail saved: you saved the email message in the drafts folder.");
			break;
		case MFMailComposeResultSent:
			NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
			break;
		case MFMailComposeResultFailed:
			NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
			break;
		default:
			NSLog(@"Mail not sent.");
			break;
	}
 
	// Remove the mail view
	[self dismissViewControllerAnimated:YES completion:nil];
}

@end
