//
//  ScoringCell.m
//  Frolfer
//
//  Created by Drew Bombard on 10/22/14.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import "ScoringCell.h"

@implementation ScoringCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self) {
		// Initialization code
	
		NSLog(@"do something...!");
		NSLog(@"\n");
	}
	return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	[super setSelected:selected animated:animated];

	// Configure the view for the selected state
	
	AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	
	
	NSLog(@"\n");
	
	CGRect btnRectIncrease = _btnIncreaseScore.frame;
	btnRectIncrease.origin.x = appDelegate.screenWidth - (_btnIncreaseScore.frame.size.width);
	_btnIncreaseScore.frame = btnRectIncrease;
	
	CGRect btnRectDecrease = _btnDecreaseScore.frame;
	btnRectDecrease.origin.x = _btnIncreaseScore.frame.origin.x - (_btnDecreaseScore.frame.size.width + 1);
	_btnDecreaseScore.frame = btnRectDecrease;
	
	CGRect lblRectPlayerScore = _lblPlayerScore.frame;
	lblRectPlayerScore.origin.x = _btnDecreaseScore.frame.origin.x - (_lblPlayerScore.frame.size.width + 2);
	_lblPlayerScore.frame = lblRectPlayerScore;
	
}

@end
