 //
//  ScoringViewController.m
//  Frolfer
//
//  Adapted and modified by Drew Bombard
//  Copyright (c) 2015 default_method All rights reserved.

//  Created (originally) by Simon on 24/11/13.
//  Copyright (c) 2015 Appcoda. All rights reserved.
//


#import "ScoringViewController.h"

@interface ScoringViewController ()

@end

@implementation ScoringViewController


- (void)viewDidLoad {

	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

	
	NSLog(@"\n\n\nScoringViewController: viewDidLoad()");
	
	[self customizeInterface];
	
	_appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [_appDelegate managedObjectContext];
	[context setUndoManager:nil];
	_managedObjectContext = context;
	
	
	NSLog(@"\n()");
	
	[self checkResultsController];

	[self resetPageContentLoaded];
	
}


-(void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
	
	self.fetchedResultsController = nil;
}

-(void)viewWillAppear:(BOOL)animated {
	
	[super viewWillAppear:animated];
	
	NSLog(@"\n\n\n\nScoringViewController: viewWillAppear()\n");
	
	NSLog(@"_gameDataArray count: %lu", (unsigned long)[_gameDataArray count]);
	NSLog(@"_gameDataArray: %@", _gameDataArray);
	NSLog(@"\n");
	NSLog(@"\n");
	NSLog(@"\n");
	NSLog(@"\n");

	NSLog(@"_game_info: %@", _game_info);
	NSLog(@"\n");
	NSLog(@"\n");
	NSLog(@"\n");
	NSLog(@"\n");

	

	if ([self isViewLoaded] && [self.view window] == nil) {
		NSLog(@"\n");
		NSLog(@"view has already been loaded.");
		NSLog(@"\n");
	} else {
		NSLog(@"view has already been loaded.");
		NSLog(@"\n");
	}

	[self gameDataCheck];
	
}

-(void)gameDataCheck {
	
	NSLog(@"\n\n\n gameDataCheck()\n\n\n");
	NSLog(@"\n");

	
	[self checkResultsController];
	
	
	int playerCount = (int)[[FetchDataArray dataFromEntity:@"Players" predicateName:nil predicateValue:nil predicateType:nil sortName:nil sortASC:nil] count];

	
// DEBUG Info
//	NSLog(@"\n");
//	NSLog(@"\n");
//	NSLog(@"_gameDataArray count: %lu", (unsigned long)[_gameDataArray count]);
//	NSLog(@"_gameDataArray: %@", _gameDataArray);
//	NSLog(@"\n");
//	NSLog(@"\n");
//	NSLog(@"\n");
//	NSLog(@"\n");
//	NSLog(@"playerCount: %d", playerCount);
//	NSLog(@"pageContentLoaded: %@", _game_info.pageContentLoaded);
//
//	NSLog(@"_game_info.course: %@", _game_info.course.courseName);
//	NSLog(@"_game_info: %@", _game_info);
//	NSLog(@"\n");
//	NSLog(@"\n");
//	NSLog(@"\n");

	

	if (
		_game_info &&
		(_game_info.course != nil && playerCount > 0)
		) {
		
		_btnNextHole.hidden = NO;
		_btnPreviousHole.hidden = NO;
		
		
		if ([_game_info.pageContentLoaded isEqual:@(0)]) {


			NSLog(@"\n");
			NSLog(@"We have game data... but no pages loaded.");
			NSLog(@"\n");
			[self resetPageContentLoaded];

			[self loadPages];
			
			NSLog(@"\n");
			NSLog(@"Pages have been loaded.");
			NSLog(@"\n");


			_game_info.gameStarted = [NSNumber numberWithInt:1];
			[self.managedObjectContext save:nil];  // write to database
		}
		

	} else {
		
		NSLog(@"\n");
		NSLog(@"We have NO GAME DATA... do something safe.");
		NSLog(@"\n");
		
		[self resetPageContentLoaded];
		
		_btnNextHole.hidden = YES;
		_btnPreviousHole.hidden = YES;
		
		[self gameSetupAlert:nil];
	}

}


#pragma mark - Config and Misc. Actions
-(void)customizeInterface {
	self.navigationItem.rightBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.hidesBackButton = YES;
}


#pragma mark - Load Page Data
-(void)loadPages {
	
	NSLog(@"ScoringViewController: loadPages()");

		
	_currentHole = [_game_info.currentHole intValue];
	
	Courses *courseObj = _game_info.course;
	_totalHoles = [courseObj.numHoles intValue];
	
	NSLog(@"currentHole %d", _currentHole);
	NSLog(@"_currentHoleIndex %d", _currentHoleIndex);

	/*
	 * Take the total holes for the course and add 1.  This will be
	 * the final scoring screen with game totals and what-not.
	 */
	_totalPageCount = _totalHoles + 1;

	// Create page view controller
	self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
	self.pageViewController.dataSource = self;
	
	PageContentViewController *startingViewController = [self viewControllerAtIndex:0];
	NSArray *viewControllers = @[startingViewController];
	
	// Advance to next page ("hole");
	[self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
	
	// Change the size of page view controller
	self.pageViewController.view.frame = CGRectMake(
													0,
													0,
													self.view.frame.size.width,
													self.view.frame.size.height);
	
	[self addChildViewController:_pageViewController];
	[self.view insertSubview:_pageViewController.view atIndex:0];
	[self.pageViewController didMoveToParentViewController:self];
	
	
	_game_info.pageContentLoaded = [NSNumber numberWithInt:1];
	_currentHoleIndex = 1;
}






#pragma mark - Data Fetching...

-(void)checkResultsController {
	
	NSLog(@"_gameDataArray: %@", _gameDataArray);
	
	
	// TEMP: To-Do, is the correct?
	// ALWAYS fetch most up-to-date game data..
	
	_gameDataArray = [FetchDataArray dataFromEntity:@"Games" predicateName:nil predicateValue:nil predicateType:nil sortName:nil sortASC:nil];
	
	if ([_gameDataArray count] >= 1) {
		_game_info = [_gameDataArray objectAtIndex:0];
	} else { _game_info = nil; }
	
	NSLog(@"_gameDataArray: %@", _gameDataArray);
}


-(NSFetchedResultsController *)fetchedResultsController {
	
	NSLog(@"NSFetchedResultsController: %@", _fetchedResultsController);
	NSLog(@"_gameDataArray count: %lu", (unsigned long)[_gameDataArray count]);
	NSLog(@"_gameDataArray: %@", _gameDataArray);
	NSLog(@"_game_info: %@", _game_info);
	
	int gameHasStarted = 0;
	int courseHasChanged = 0;
	if (_game_info) {
		if (_game_info.gameStarted != nil || _game_info.gameStarted == 0) {
			gameHasStarted = [_game_info.gameStarted intValue];
		}
		if (_game_info.courseChanged != nil || _game_info.courseChanged == 0) {
			courseHasChanged = [_game_info.courseChanged intValue];
		}
	}
	NSLog(@"gameStarted: %d", gameHasStarted);
	NSLog(@"courseChanged: %d", courseHasChanged);
	
	/**
	 * If we have previous results or the course has not changed,
	 * return previous result data set.
	 */
	if (
			(_fetchedResultsController != nil || [_gameDataArray count] >= 1)
		
			|| ( gameHasStarted == 1 && courseHasChanged == 0 )
		
		){
		return _fetchedResultsController;
	}
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription
								   entityForName:@"Games" inManagedObjectContext:_managedObjectContext];
	[fetchRequest setEntity:entity];
	
	NSSortDescriptor *sort = [[NSSortDescriptor alloc]
							  initWithKey:@"gameDate" ascending:YES];
	
	[fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
	
	NSFetchedResultsController *theFetchedResultsController =
	[[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
										managedObjectContext:_managedObjectContext
										  sectionNameKeyPath:nil
												   cacheName:nil];
	
	self.fetchedResultsController = theFetchedResultsController;
	_fetchedResultsController.delegate = self;
	
	NSError *error;
	NSArray *results = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
	_gameDataArray = [[NSMutableArray alloc]initWithArray:results];
	
	NSLog(@"_gameDataArray count: %lu", (unsigned long)[_gameDataArray count]);
	NSLog(@"NSFetchedResultsController: %@", _fetchedResultsController);
	NSLog(@"results count: %lu", (unsigned long)[results count]);
	NSLog(@"_gameDataArray: %@", _gameDataArray);
	NSLog(@"_game_info: %@", _game_info);

	NSLog(@"results: %@", results);

	if ( [results count] >0 ) {

		_game_info = [[[NSMutableArray alloc]initWithArray:results] objectAtIndex:0];
	}
	

	NSLog(@"_game_info: %@", _game_info);
	
	
	return _fetchedResultsController;
}




# pragma mark - PageView Setup and Load
-(PageContentViewController *)viewControllerAtIndex:(NSUInteger)index {
	
	int holeCheck = (int)(index + 1);
	
	NSLog(@"\n\n\n\n\n\n");
	NSLog(@"PAGE INDEX ------>: %lu",(unsigned long)index);
	NSLog(@"holeCheck: %d", holeCheck);
	NSLog(@"\n");
	NSLog(@"_totalHoles: %d", _totalHoles);
	NSLog(@"_totalPageCount: %d", _totalPageCount);
	NSLog(@"\n");

	if (holeCheck >= _totalPageCount) {
		[_btnPreviousHole setImage:[UIImage imageNamed:@"btn_arrow_previous"] forState:UIControlStateNormal];
		[_btnNextHole setImage:[UIImage imageNamed:@"btn_arrow_next-off"] forState:UIControlStateNormal];
		[_btnPreviousHole setEnabled:YES];
		[_btnNextHole setEnabled:NO];
	}
	if (holeCheck == 1) {
		[_btnPreviousHole setImage:[UIImage imageNamed:@"btn_arrow_previous-off"] forState:UIControlStateNormal];
		[_btnNextHole setImage:[UIImage imageNamed:@"btn_arrow_next"] forState:UIControlStateNormal];
		[_btnPreviousHole setEnabled:NO];
		[_btnNextHole setEnabled:YES];
	}
	if ((holeCheck > 1 && holeCheck < _totalHoles) || holeCheck == _totalHoles  ) {
		[_btnPreviousHole setImage:[UIImage imageNamed:@"btn_arrow_previous"] forState:UIControlStateNormal];
		[_btnNextHole setImage:[UIImage imageNamed:@"btn_arrow_next"] forState:UIControlStateNormal];
		[_btnPreviousHole setEnabled:YES];
		[_btnNextHole setEnabled:YES];
	}
	if ( (holeCheck) == _totalPageCount ) {
		[_btnPreviousHole setImage:[UIImage imageNamed:@"btn_arrow_previous-off"] forState:UIControlStateNormal];
		_btnNextHole.hidden = YES;
		_btnPreviousHole.hidden = YES;
	} else {
		_btnNextHole.hidden = NO;
		_btnPreviousHole.hidden = NO;
	}

	if ( (_totalPageCount == 0) || (index >= _totalPageCount) ) {
		return nil;
	}
	
	// Create a new view controller and pass suitable data.
	PageContentViewController *pageContentViewController = [self.storyboard
															instantiateViewControllerWithIdentifier:@"PageContentViewController"];

	
	if (index == _totalHoles) {
		pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FinalScoreViewController"];
	} else {
		pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentViewController"];
	}
	pageContentViewController.pageIndex = (int)index;
	
	return pageContentViewController;
}


-(void)changePage:(UIPageViewControllerNavigationDirection)direction
{
	_currentHoleIndex = ((PageContentViewController *) [_pageViewController.viewControllers objectAtIndex:0]).pageIndex;
	
	if (direction == UIPageViewControllerNavigationDirectionForward) {
		_currentHoleIndex++;
	} else {
		_currentHoleIndex--;
	}
	
	PageContentViewController *viewController = [self viewControllerAtIndex:_currentHoleIndex];
	
	if (viewController == nil) {
		return;
	}
	
	[_pageViewController setViewControllers:@[viewController]
								  direction:direction
								   animated:YES
								 completion:nil];
}


#pragma mark - PageViewController Data Source

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
	NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
	
	if ((index == 0) || (index == NSNotFound)) {
		return nil;
	}
	
	index--;
	
	return [self viewControllerAtIndex:index];
}


- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
	_currentHoleIndex = ((PageContentViewController*) viewController).pageIndex;
	NSUInteger index = ((PageContentViewController*) viewController).pageIndex;


	if (index == NSNotFound) {
		return nil;
	}
	
	index++;
	if (index == _totalPageCount) {
		return nil;
	}
	
	return [self viewControllerAtIndex:index];
}

//- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
//	
//	return _totalPageCount;
//}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
	return 0;
}

- (IBAction)tempClear:(id)sender
{
	[self resetPageContentLoaded];
	[self gameDataCheck];
}

-(void)resetPageContentLoaded {
	
	[_pageViewController removeFromParentViewController];
	[_pageViewController.view removeFromSuperview];
	
	if (_game_info) {
		
		if (_game_info.pageContentLoaded != nil) {
			_game_info.pageContentLoaded = [NSNumber numberWithInt:0];
			_game_info.gameStarted = [NSNumber numberWithInt:0];
			
			[self.managedObjectContext save:nil];  // write to database
		}
	}
}

- (IBAction)previousHole:(id)sender {

	NSLog(@"previousHole()");
	[self changePage:UIPageViewControllerNavigationDirectionReverse];
}

- (IBAction)nextHole:(id)sender {
	
	NSLog(@"nextHole()");
	[self changePage:UIPageViewControllerNavigationDirectionForward];
}




#pragma mark - Alert(s)
-(void)gameSetupAlert:(id)sender
{
	NSString *alertMessage = @"Game not setup yet. Select a course and add at least one player.";
	
	UIAlertController *alert = [UIAlertController
								alertControllerWithTitle:@"Ummmmm..."
								message:alertMessage
								preferredStyle:UIAlertControllerStyleAlert];
	
	
	[self presentViewController:alert animated:YES completion:nil];
	
	
	UIAlertAction *okAction = [UIAlertAction
							   actionWithTitle:NSLocalizedString(@"OK, I promise not to screw up.", @"OK action")
							   style:UIAlertActionStyleDefault
							   handler:^(UIAlertAction *action)
							   {
								   NSLog(@"OK button clicked");
								   
								   //[self setupGame];
								   self.tabBarController.selectedIndex = 1;
							   }];
	
	alertMessage = nil;
	[alert addAction:okAction];
}


@end
