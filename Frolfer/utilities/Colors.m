//
//  Colors.m
//  Frolfer
//
//  Created by Drew Bombard on 2/5/12.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import "Colors.h"

@implementation Colors

-(id)init {
	self = [super init];
	if (self) {
		
		_btnDisabled = [Colors colorWithHexString:@"F5F5F5"];

		
		_tabTextBlue = [Colors colorWithHexString:@"80DCFF"];
		_lightBlue = [Colors colorWithHexString:@"73C6FD"];
		_medBlue = [Colors colorWithHexString:@"1B8FF2"];
		_darkBlue = [Colors colorWithHexString:@"007AFF"];
		
		_lightYelllow = [Colors colorWithHexString:@"FFDD87"];
		_medYelllow = [Colors colorWithHexString:@"FFCE54"];
		_darkYelllow = [Colors colorWithHexString:@"F6BB42"];
		
		_darkPurple = [Colors colorWithHexString:@"987FB6"];
		
		_btnRed = [Colors colorWithHexString:@"E14D46"];
		_lightRed = [Colors colorWithHexString:@"F2838F"];
		_medRed = [Colors colorWithHexString:@"ED5565"];
		_darkRed = [Colors colorWithHexString:@"E92348"];
		
		_lightGreen = [Colors colorWithHexString:@"B9DF90"];
		_medGreen = [Colors colorWithHexString:@"71B340"];
		_darkGreen = [Colors colorWithHexString:@"8CC152"];
		
		
		_btnGreen = [Colors colorWithHexString:@"70BF1D"];
		

		
		_tabTextOrange = [Colors colorWithHexString:@"FFDA88"];
		_lightOrange = [Colors colorWithHexString:@"FFDA88"];
		_medOrange = [Colors colorWithHexString:@"FFCB57"];
		_darkOrange = [Colors colorWithHexString:@"F79C00"];
		
		_grayCCC = [Colors colorWithHexString:@"CCCCCC"];
		_gray999 = [Colors colorWithHexString:@"999999"];
		_gray666 = [Colors colorWithHexString:@"666666"];
		_gray555 = [Colors colorWithHexString:@"555555"];
		_gray333 = [Colors colorWithHexString:@"333333"];
		
		_lightGray = [Colors colorWithHexString:@"EFEFEF"];
		_medGray = [Colors colorWithHexString:@"D6D6D6"];
		_darkGray = [Colors colorWithHexString:@"A9A9A9"];
		_charcoalGray = [Colors colorWithHexString:@"797979"];
		_charcoalGray28 = [Colors colorWithHexString:@"464646"];
	}
	return self;
}

+(Colors *)get {
	static Colors* colors = nil;
	if (!colors) {
		colors = [[Colors alloc] init];
	}
	return colors;
}

+(UIColor *) colorWithHexString: (NSString *) hex {

	/*
	 * Performs conversion from hex string to color.
	 */
	NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];  

	// String should be 6 or 8 characters  
	if ([cString length] < 6) return [UIColor grayColor];  

	// strip 0X if it appears  
	if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];  

	if ([cString length] != 6) return  [UIColor grayColor];  

	// Separate into r, g, b substrings  
	NSRange range;  
	range.location = 0;  
	range.length = 2;  
	NSString *rString = [cString substringWithRange:range];  

	range.location = 2;  
	NSString *gString = [cString substringWithRange:range];  

	range.location = 4;  
	NSString *bString = [cString substringWithRange:range];  

	// Scan values  
	unsigned int r, g, b;  
	[[NSScanner scannerWithString:rString] scanHexInt:&r];  
	[[NSScanner scannerWithString:gString] scanHexInt:&g];  
	[[NSScanner scannerWithString:bString] scanHexInt:&b];  

	return [UIColor colorWithRed:((float) r / 255.0f)  
						   green:((float) g / 255.0f)  
							blue:((float) b / 255.0f)  
						   alpha:1.0f];  
}

@end
