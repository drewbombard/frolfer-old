//
//  DeleteData.h
//  Frolfer
//
//  Created by Drew Bombard on 2/13/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"


@interface DeleteData : NSObject


+(void)deleteFromEntity:(NSString *)entityName predicate:(NSString *)predicate predicateValue:(NSString *)predicateValue;

@end
