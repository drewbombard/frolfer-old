//
//  CLPlacemark+ShortState.h
//  Frolfer
//
//  Created by Drew Bombard on 4/2/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>

@interface CLPlacemark (ShortState)

-(NSString *)shortState;

@end
