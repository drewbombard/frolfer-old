//
//  CLPlacemark+ShortState.m
//  Frolfer
//
//  Created by Drew Bombard on 4/2/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import "CLPlacemark+ShortState.m"

@interface CLPlacemark (ShortStatePrivate)

-(NSDictionary *)nameAbbreviations;

@end


@implementation CLPlacemark (ShortState)

-(NSString *)shortState {
	
	NSString *state = self.administrativeArea;
	
	if (state.length==0) {
		return nil;
	} else {
		NSLog(@"state: %@",[[self nameAbbreviations] objectForKey:state]);

		NSLog(@"\n");
		return [[self nameAbbreviations] objectForKey:state];
	}
}


-(NSDictionary *)nameAbbreviations {
	
	static NSDictionary *nameAbbreviations = nil;
	
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		
		nameAbbreviations = [NSDictionary dictionaryWithObjectsAndKeys:
							 @"Alabama",@"AL",
							 @"Alaska",@"AK",
							 @"Arizona",@"AZ",
							 @"Arkansas",@"AR",
							 @"California",@"CA",
							 @"Colorado",@"CO",
							 @"Connecticut",@"CT",
							 @"Delaware",@"DE",
							 @"District of Columbia",@"DC",
							 @"Florida",@"FL",
							 @"Georgia",@"GA",
							 @"Hawaii",@"HI",
							 @"Idaho",@"ID",
							 @"Illinois",@"IL",
							 @"Indiana",@"IN",
							 @"Iowa",@"IA",
							 @"Kansas",@"KS",
							 @"Kentucky",@"KY",
							 @"Louisiana",@"LA",
							 @"Maine",@"ME",
							 @"Maryland",@"MD",
							 @"Massachusetts",@"MA",
							 @"Michigan",@"MI",
							 @"Minnesota",@"MN",
							 @"Mississippi",@"MS",
							 @"Missouri",@"MO",
							 @"Montana",@"MT",
							 @"Nebraska",@"NE",
							 @"Nevada",@"NV",
							 @"New Hampshire",@"NH",
							 @"New Jersey",@"NJ",
							 @"New Mexico",@"NM",
							 @"New York",@"NY",
							 @"North Carolina",@"NC",
							 @"North Dakota",@"ND",
							 @"Ohio",@"OH",
							 @"Oklahoma",@"OK",
							 @"Oregon",@"OR",
							 @"Pennsylvania",@"PA",
							 @"Rhode Island",@"RI",
							 @"South Carolina",@"SC",
							 @"South Dakota",@"SD",
							 @"Tennessee",@"TN",
							 @"Texas",@"TX",
							 @"Utah",@"UT",
							 @"Vermont",@"VT",
							 @"Virginia",@"VA",
							 @"Washington",@"WA",
							 @"West Virginia",@"WV",
							 @"Wisconsin",@"WI",
							 @"Wyoming",@"WY",
							 nil];
	});
	return nameAbbreviations;
}

@end
