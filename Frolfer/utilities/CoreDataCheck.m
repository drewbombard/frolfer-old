//
//  CoreDataCheck.m
//  Score500
//
//  Created by Drew Bombard on 4/24/14.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import "CoreDataCheck.h"

@implementation CoreDataCheck

+(int)checkEntity:(NSString *)entityName {
	
	NSLog(@"check_local_data(%@)",entityName);
	
	AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [appDelegate managedObjectContext];
	[context setUndoManager:nil];
	
	// Check the Employees entity for local data...
	NSFetchRequest *fetch_request = [[NSFetchRequest alloc] init];
	
	fetch_request.entity = [NSEntityDescription
							  entityForName:entityName
							  inManagedObjectContext:context];

	fetch_request.includesSubentities = NO;
	fetch_request.returnsDistinctResults = YES;

	
	NSError *err;
	NSUInteger count = [context countForFetchRequest:fetch_request error:&err];
	if (count == NSNotFound) {
		//Handle error
	}
	
	if (count > 0) {
		NSLog(@"yay......");
	} else {
		NSLog(@"NO records...");
	}
	
	return (int)count;
}

@end