//
//  UIImage+fixOrientation.h
//  Frolfer
//
//  Created by Drew Bobmard on 8/1/12.
//  Copyright (c) 2015 default_method All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (fixOrientation)

- (UIImage *)fixOrientation;

@end
