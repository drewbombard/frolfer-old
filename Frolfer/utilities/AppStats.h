//
//  AppStats.h
//  Frolfer
//
//  Created by Drew Bombard on 2/25/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppStats : NSObject

+(NSString *) build;
+(NSString *) version;
+(NSString *) versionNumber;
+(NSString *) versionAndBuild;
+(NSString *) strVersionAndBuild;
+(NSString *) versionNumberFullString;
+(NSString *) versionNumberWithName;

@end
