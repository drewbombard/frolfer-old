//
//  GameStats.h
//  Frolfer
//
//  Created by Drew Bombard on 1/26/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

// Utilities
#import "FetchDataArray.h"

// Data
#import "Players+CoreDataClass.h"
#import "Scoring.h"
#import "Courses.h"
#import "Games+CoreDataClass.h"


@interface GameStats : NSObject


+(int)highestCompletedHole:(Players *)selected_player;
+(int)percentCompleteByPlayer:(Players *)selected_player;

+(int)holeLeader:(NSString *)scoreBeingChecked currentHole:(NSNumber *)currentHole fetchedResults:(NSFetchedResultsController *)fetchedResultsController;

+(NSMutableDictionary *)gameLeader:(NSFetchedResultsController *)fetchedResultsController;

+(NSMutableDictionary *)profileStats:(Players *)selected_player;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@end
