//
//  GameStats.m
//  Frolfer
//
//  Created by Drew Bombard on 1/26/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import "GameStats.h"

@implementation GameStats


-(id)init {
	self = [super init];
	if (self) { }
	return self;
}


+(int)highestCompletedHole:(Players *)selected_player {

	int highestCompleteHole = 0;
	
	NSLog(@"\n\n\nSTART loop through player score sets for %@ ----->", selected_player.playerName);
	for (Scoring *scoringObject in selected_player.scores) {
		
		NSLog(@"hole number: %@", scoringObject.holeNumber);
		if (scoringObject.score && [scoringObject.score intValue] != 0) {
			
			if (highestCompleteHole == 0) {
				highestCompleteHole = [scoringObject.holeNumber intValue];
			} else {
				if ([scoringObject.holeNumber intValue] > highestCompleteHole) {
					highestCompleteHole = [scoringObject.holeNumber intValue];
				}
			}
		}
	}
	NSLog(@"\n<----- END loop through player score sets\n\n\n");
	NSLog(@"highestCompleteHole: %d", highestCompleteHole);

	return highestCompleteHole;
}

+(int)percentCompleteByPlayer:(Players *)selected_player {
	
	int percentComplete = 0;
	
	NSMutableArray *gameDataArray = [FetchDataArray dataFromEntity:@"Games" predicateName:nil predicateValue:nil predicateType:nil sortName:nil sortASC:nil];
	Games *game_info = [gameDataArray objectAtIndex:0];
	Courses *courseObj = game_info.course;
	
	NSLog(@"\n");
	
	int completedHoles = 0;
	
	NSLog(@"\n\n\nSTART loop through player score sets for %@ ----->", selected_player.playerName);
	for (Scoring *scoringObject in selected_player.scores) {
		if (scoringObject.score && [scoringObject.score intValue] != 0) {
			completedHoles = completedHoles+1;
		}
	}
	NSLog(@"\n<----- END loop through player score sets\n\n\n");

	percentComplete = (completedHoles * 100) / [courseObj.numHoles intValue];

	NSLog(@"percentComplete: %d%%",percentComplete);
	return percentComplete;
}



+(NSMutableDictionary *)profileStats:(Players *)selected_player {
	
	int profileScoreLow = 0;
	int profileScoreTotal = 0;
	int profileScoreLowHole = 0;
	
	NSMutableDictionary *playerProfileStats = [[NSMutableDictionary alloc] init];

// TRASH
//	NSLog(@"selected_player: %@", selected_player);
//	NSLog(@"selected_player: %@", selected_player.objectID);
//	NSLog(@"playerProfileStats: %@", playerProfileStats);

	
	NSMutableArray *gameDataArray = [FetchDataArray dataFromEntity:@"Games" predicateName:nil predicateValue:nil predicateType:nil sortName:nil sortASC:nil];

	Games *game_info = [gameDataArray objectAtIndex:0];
	
	int totalHoles;
	Courses *courseObj = game_info.course;
	totalHoles = [courseObj.numHoles intValue];

	



	
	NSLog(@"\n\n\nSTART loop through player score sets for %@ ----->", selected_player.playerName);
	for (Scoring *scoringObject in selected_player.scores) {
		
		NSLog(@"scoringObject.score: %d",[scoringObject.score intValue]);
		if (scoringObject.score &&  [scoringObject.score intValue] != 0) {
			profileScoreTotal = profileScoreTotal + [[scoringObject.score stringValue] intValue];
			NSLog(@"profileScoreTotal in loop: %d",profileScoreTotal);
			NSLog(@"\n");
			
			if (profileScoreLow == 0) {
				profileScoreLow = [scoringObject.score intValue];
				profileScoreLowHole = [scoringObject.holeNumber intValue];
			} else {
				if ([scoringObject.score intValue] <= profileScoreLow) {
					profileScoreLow = [scoringObject.score intValue];
					profileScoreLowHole = [scoringObject.holeNumber intValue];
				}
			}
		}
	}
	NSLog(@"\n<----- END loop through player score sets\n\n\n");
	
	
	NSLog(@"totalHoles: %d", totalHoles);
	NSLog(@"profileScoreLow: %d", profileScoreLow);
	NSLog(@"profileScoreTotal: %d", profileScoreTotal);
	NSLog(@"profileScoreLowHole: %d", profileScoreLowHole);

	
	NSString* formattedNumber = [NSString stringWithFormat:@"%.02f",(float)profileScoreTotal / totalHoles];
	
	[playerProfileStats setObject: formattedNumber forKey:@"profileScoreAverage"];
	[playerProfileStats setObject: [NSNumber numberWithInt:profileScoreLow] forKey:@"profileScoreLow"];
	[playerProfileStats setObject: [NSNumber numberWithInt:profileScoreLowHole] forKey:@"profileScoreLowHole"];
	[playerProfileStats setObject: [NSNumber numberWithInt:profileScoreTotal] forKey:@"profileScoreTotal"];

	
	return playerProfileStats;
}


+(int)holeLeader:(NSString *)scoreBeingChecked currentHole:(NSNumber *)currentHole fetchedResults:(NSFetchedResultsController *)fetchedResultsController {
	
	NSLog(@"currentHole: %@", currentHole);
	NSLog(@"scoreBeingChecked: %@", scoreBeingChecked);

	NSMutableArray *scoringArr;
	scoringArr = [FetchDataArray dataFromEntity:@"Scoring" predicateName:@"holeNumber" predicateValue:[currentHole stringValue] predicateType:@"number" sortName:nil sortASC:nil];
	
	// Set temporary values to check against..
	int playerIsLeading = 0;
	int lowScoreCheck = 0;
	
	NSLog(@"\n\n\nSTART Loop through low scores ----->");
	for (Scoring *scoringObject in scoringArr) {
	
		NSLog(@"lowScoreCheck BEFORE: %d",lowScoreCheck);
		NSLog(@"scoringObject.score: %d",[scoringObject.score intValue]);
		NSLog(@"\n");
		
		if (!lowScoreCheck || lowScoreCheck == 0) {
			lowScoreCheck = [scoringObject.score intValue];
			NSLog(@"lowScoreCheck AFTER: %d",lowScoreCheck);
		} else {
			//NSLog(@"arr item: %@",scoringObject.score);
			if ( [scoringObject.score intValue] != 0 && ([scoringObject.score intValue] < lowScoreCheck) ) {
				lowScoreCheck = [scoringObject.score intValue];
				NSLog(@"lowScoreCheck AFTER: %d",lowScoreCheck);
			}
		}
	}
	NSLog(@"\n<----- END Loop through low scores\n\n\n");
	NSLog(@"scoreBeingChecked: %@",scoreBeingChecked);
	NSLog(@"lowScoreCheck: %d",lowScoreCheck);
	NSLog(@"playerIsLeading: %d",playerIsLeading);
	NSLog(@"\n\n");
	
	if ([scoreBeingChecked intValue] != 0 && [scoreBeingChecked intValue] <= lowScoreCheck) {
		playerIsLeading = 1;
	}
	return playerIsLeading;
}


+(NSMutableDictionary *)gameLeader:(NSFetchedResultsController *)fetchedResultsController {
	
	int cnt = 0;
	int highScoreTotal = 0;
	int tieGame = 0;
	int gameStarted = 0;
	int lowScoreTotal = 0;
	int lowScore = 0;

	Players *highScoringPlayer;
	Players *lowScoringPlayer;
	
	NSLog(@"\n\n\nSTART loop through all Players ----->");
	for (Players *playerObject in fetchedResultsController.fetchedObjects) {
		
		// running count of players we are comparing
		cnt = cnt+1;
		int playerTotalScore = 0;
		NSSet *scoreSet = playerObject.scores;
		NSLog(@"Count: %d", cnt);
		
		NSLog(@"\n\n\nSTART loop through player score sets for %@ ----->", playerObject.playerName);
		for (Scoring *scoringObject in scoreSet) {
			
			NSLog(@"scoringObject.score: %d",[scoringObject.score intValue]);
			if (scoringObject.score &&  [scoringObject.score intValue] != 0) {
				playerTotalScore = playerTotalScore + [[scoringObject.score stringValue] intValue];
				NSLog(@"playerTotalScore in loop: %d",playerTotalScore);
				NSLog(@"\n");
				
				if (lowScore == 0) {
					lowScore = [scoringObject.score intValue];
				} else {
					if ([scoringObject.score intValue] <= lowScore) {
						lowScore = [scoringObject.score intValue];
					}
				}
			}
		}
		NSLog(@"\n<----- END loop through player score sets\n\n\n");
		
		// Grab score from loop above, and check for low value..
		if (lowScoreTotal == 0) {
			lowScoreTotal = playerTotalScore;
			lowScoringPlayer = playerObject;
			gameStarted = 1;
		} else {
		
			if (playerTotalScore != 0) {
				
				// Check for tie game..
				if ( lowScoreTotal != 0
					&& cnt != 1
					&& (playerTotalScore == lowScoreTotal)  ) {

					NSLog(@"TIE Game!");
					tieGame = 1;
					gameStarted = 1;
				}
				
				if (playerTotalScore < lowScoreTotal) {
					NSLog(@"\n");
					lowScoreTotal = playerTotalScore;
					lowScoringPlayer = playerObject;
					tieGame = 0;
					gameStarted = 1;
				}
			}
		}
		if (lowScoreTotal == 0 && playerTotalScore == 0) {
			gameStarted = 0;
		}
		
//################################################
		
		if (playerTotalScore > highScoreTotal) {
			highScoreTotal = playerTotalScore;
			highScoringPlayer = playerObject;
		}
		if ( highScoreTotal != 0 && (playerTotalScore == highScoreTotal) ) {
			gameStarted = 1;
		}
		if (highScoreTotal == 0 && playerTotalScore == 0) {
			gameStarted = 0;
		}
	
		NSLog(@"playerTotalScore: %d", playerTotalScore);
		NSLog(@"lowScoreTotal: %d", lowScoreTotal);
		NSLog(@"highScoreTotal: %d", highScoreTotal);
		NSLog(@"\n");
	}
	NSLog(@"\n<----- END loop through all Players\n\n\n");

	NSLog(@"lowScore: %d", lowScore);

//	NSLog(@"\n######################\n");
//	NSLog(@"High Scoring Player: %@", highScoringPlayer);
//	NSLog(@"High Scoring Player: %@", highScoringPlayer.playerName);
//	NSLog(@"High Score: %d", highScore);
//	NSLog(@"\n######################\n");
//	NSLog(@"\n");

	NSMutableDictionary *scoreLeaderDict = [[NSMutableDictionary alloc] init];
	if (gameStarted == 1) {
		[scoreLeaderDict setObject: [NSNumber numberWithInt:highScoreTotal] forKey:@"highScore"];
		[scoreLeaderDict setObject: highScoringPlayer.playerName forKey:@"highScoringPlayerName"];
		[scoreLeaderDict setObject: highScoringPlayer.playerNickName forKey:@"highScoringPlayerNickName"];
		[scoreLeaderDict setObject: highScoringPlayer forKey:@"highScoringPlayerObject"];
		
		[scoreLeaderDict setObject: [NSNumber numberWithInt:lowScore] forKey:@"lowScore"];
		[scoreLeaderDict setObject: [NSNumber numberWithInt:lowScoreTotal] forKey:@"lowScoreTotal"];
		[scoreLeaderDict setObject: lowScoringPlayer.playerName forKey:@"lowScoringPlayerName"];
		[scoreLeaderDict setObject: lowScoringPlayer.playerNickName forKey:@"lowScoringPlayerNickName"];
		[scoreLeaderDict setObject: lowScoringPlayer forKey:@"lowScoringPlayerObject"];
		
		[scoreLeaderDict setObject: [NSNumber numberWithInt:tieGame] forKey:@"tie"];
		[scoreLeaderDict setObject: [NSNumber numberWithInt:gameStarted] forKey:@"gameStarted"];
	} else {
		[scoreLeaderDict setObject: [NSNumber numberWithInt:gameStarted] forKey:@"gameStarted"];
	}
	return scoreLeaderDict;
}

@end
