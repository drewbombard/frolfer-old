//
//  CoreDataCheck.h
//  Score500
//
//  Created by Drew Bombard on 4/24/14.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

@interface CoreDataCheck : NSObject

+(int)checkEntity:(NSString *)entityName;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@end
