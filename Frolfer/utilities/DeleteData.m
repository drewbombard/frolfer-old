//
//  DeleteData.m
//  Frolfer
//
//  Created by Drew Bombard on 2/13/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import "DeleteData.h"

@implementation DeleteData


+(void)deleteFromEntity:(NSString *)entityName predicate:(NSString *)predicate predicateValue:(NSString *)predicateValue {
	
	
	AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *_managedObjectContext = [appDelegate managedObjectContext];
	[_managedObjectContext setUndoManager:nil];
	
	
	/*
	 * Delete all records of passed entity
	 */
	NSLog(@"entityName: %@",entityName);
	NSLog(@"predicate: %@",predicate);
	NSLog(@"predicateValue: %@",predicateValue);
	
	NSFetchRequest *deleteRequest = [[NSFetchRequest alloc] init];
	[deleteRequest setEntity:[NSEntityDescription entityForName:entityName
										 inManagedObjectContext:_managedObjectContext]];
	
	if (predicateValue != nil) {
		// 3 - Filter it (optional)
		deleteRequest.predicate = [NSPredicate predicateWithFormat:[[predicate stringByAppendingString:@" == "] stringByAppendingString:predicateValue]];
		NSLog(@"deleteRequest.predicate: %@", deleteRequest.predicate);
	}
	
	NSArray *deleteResult = [_managedObjectContext executeFetchRequest:deleteRequest error:nil];
	
	for (id object in deleteResult) {
		[_managedObjectContext deleteObject:object];
		NSLog(@"Deleting object: %@",object);
		NSLog(@"\n");
	}
	
	// write to database
	[_managedObjectContext save:nil];
}

@end
