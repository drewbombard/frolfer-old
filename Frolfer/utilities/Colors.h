//
//  Colors.h
//  Frolfer
//
//  Created by Drew Bombard on 2/5/12.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Colors: NSObject

@property (nonatomic,readonly) UIColor *btnDisabled;


@property (nonatomic,readonly) UIColor *tabTextBlue;
@property (nonatomic,readonly) UIColor *lightBlue;
@property (nonatomic,readonly) UIColor *medBlue;
@property (nonatomic,readonly) UIColor *darkBlue;

@property (nonatomic,readonly) UIColor *lightYelllow;
@property (nonatomic,readonly) UIColor *medYelllow;
@property (nonatomic,readonly) UIColor *darkYelllow;

//@property (nonatomic,readonly) UIColor *lightPurple;
//@property (nonatomic,readonly) UIColor *medPurple;
@property (nonatomic,readonly) UIColor *darkPurple;

@property (nonatomic,readonly) UIColor *btnRed;
@property (nonatomic,readonly) UIColor *lightRed;
@property (nonatomic,readonly) UIColor *medRed;
@property (nonatomic,readonly) UIColor *darkRed;

@property (nonatomic,readonly) UIColor *btnGreen;
@property (nonatomic,readonly) UIColor *lightGreen;
@property (nonatomic,readonly) UIColor *medGreen;
@property (nonatomic,readonly) UIColor *darkGreen;

@property (nonatomic,readonly) UIColor *tabTextOrange;
@property (nonatomic,readonly) UIColor *lightOrange;
@property (nonatomic,readonly) UIColor *medOrange;
@property (nonatomic,readonly) UIColor *darkOrange;

@property (nonatomic,readonly) UIColor *grayCCC;
@property (nonatomic,readonly) UIColor *gray999;
@property (nonatomic,readonly) UIColor *gray666;
@property (nonatomic,readonly) UIColor *gray555;
@property (nonatomic,readonly) UIColor *gray333;

@property (nonatomic,readonly) UIColor *lightGray;
@property (nonatomic,readonly) UIColor *medGray;
@property (nonatomic,readonly) UIColor *darkGray;
@property (nonatomic,readonly) UIColor *charcoalGray;
@property (nonatomic,readonly) UIColor *charcoalGray28;

+(Colors*)get;

+(UIColor*)colorWithHexString:(NSString*)hex;

@end
