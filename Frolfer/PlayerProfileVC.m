//
//  ProfileViewController.m
//  Frolfer
//
//  Created by Drew Bombard on 2/6/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import "PlayerProfileVC.h"



@interface PlayerProfileVC ()

@end

@implementation PlayerProfileVC


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

	[self customizeInterface];
	
	_appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [_appDelegate managedObjectContext];
	[context setUndoManager:nil];
	_managedObjectContext = context;

	NSError *error;
	if (![[self fetchedResultsController] performFetch:&error]) {
		// Update to handle the error appropriately.
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		exit(-1);  // Fail
	}
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
	self.fetchedResultsController = nil;
}



-(IBAction)dismissModal:(id)sender {
	[self dismissViewControllerAnimated:YES completion:nil];
}


-(void)gameDataCheck
{
	if (_gameDataArray == nil) {
		_gameDataArray = [FetchDataArray dataFromEntity:@"Games" predicateName:nil predicateValue:nil predicateType:nil sortName:nil sortASC:nil];
		
		if ([_gameDataArray count] >= 1) {
			_game_info = [_gameDataArray objectAtIndex:0];
		}
		
		Courses *courseObj = _game_info.course;
		_totalHoles = [courseObj.numHoles intValue];
	}
	
	
	NSLog(@"_totalHoles %d: ",_totalHoles);
	NSLog(@"\n");
}

-(void)customizeInterface
{
//	_modalTitleBar.backgroundColor = [[Colors get] medBlue];
//	self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
	
	self.view.backgroundColor = [UIColor clearColor];
	self.modalPresentationStyle = UIModalPresentationCurrentContext;
	self.modalPresentationStyle = UIModalPresentationFormSheet;
}


-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	[self gameDataCheck];
	
	[self getPlacement];
	
	[self displayPlayerInfo];
}



-(void)getPlacement
{
	_allPlayersArr = [FetchDataArray
					  dataFromEntity:@"Players"
					  predicateName:nil
					  predicateValue:nil
					  predicateType:nil
					  sortName:nil
					  sortASC:nil];
	
	NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"totalScore.score" ascending:YES];
	NSArray *sorted = [_allPlayersArr sortedArrayUsingDescriptors:
					   [NSArray arrayWithObject:sorter]];
	
	int currentPlace = 0;
	int playersPlace = 1;
	
	for (id sortedObj in sorted) {
		currentPlace++;
		if (_player == sortedObj) {
			playersPlace = currentPlace;
		}
	}

	// Ordinal number format
	NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
	numberFormatter.numberStyle = NSNumberFormatterOrdinalStyle;
	
	_lblRank.text = [NSString stringWithFormat:@"%@", [numberFormatter stringFromNumber:[NSNumber numberWithInt:playersPlace]]];
}


-(void)displayPlayerInfo {
	
	_lblPlayerName.text = _player.playerName;
	
	if ([_player.playerNickName isEqual: @""]) {
		_lblPlayerNickName.text = nil;
	} else {
		_lblPlayerNickName.text = [@"\"" stringByAppendingFormat:@"%@%@", _player.playerNickName, @"\""];
	}
	
	NSLog(@"player: %@",_player);
	
	[GameStats highestCompletedHole:_player];
	int intPercentComplete = [GameStats percentCompleteByPlayer:_player];
	float percentComplete = (float)intPercentComplete / 100;
	
	_progressView.transform = CGAffineTransformMakeScale(1.0, 3.0);
	[_progressView setProgress:percentComplete animated:YES];
	_progressLabel.text = [NSString stringWithFormat:@"%@%%",[@(intPercentComplete) stringValue]];
	
	
	NSMutableDictionary *dictPlayerProfile = [GameStats profileStats:_player];
	NSString *strLowParHole;
	if ([dictPlayerProfile objectForKey:@"profileScoreLowHole"] == [NSNumber numberWithInt:0] ) {
		strLowParHole = @"- - - - - -";
	} else {
	strLowParHole = [@"#" stringByAppendingFormat:@"%@%@%@", [[dictPlayerProfile objectForKey:@"profileScoreLowHole"] stringValue], @" with a par ",[dictPlayerProfile objectForKey:@"profileScoreLow"]];
	}
	
	_lblParAverage.text = [NSString stringWithFormat:@"%@", [dictPlayerProfile objectForKey:@"profileScoreAverage"]];
	_lblTotalPoints.text = [[dictPlayerProfile objectForKey:@"profileScoreTotal"] stringValue];
	_lblParLow.text = strLowParHole;

	
	
	UIImage *cdImage = [UIImage imageWithData: _player.playerPhoto];
	if (cdImage != nil) {
		_imgPlayer.image = cdImage;
		_imgPlayer.layer.cornerRadius = _imgPlayer.frame.size.width / 2;
		_imgPlayer.clipsToBounds = YES;
	}

}



#pragma mark - fetchedResultsController
-(NSFetchedResultsController *)fetchedResultsController
{
	NSLog(@"NSFetchedResultsController()");
	
	if (_fetchedResultsController != nil) {
		return _fetchedResultsController;
	}
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription
								   entityForName:@"Players"
								   inManagedObjectContext:_managedObjectContext];
	[fetchRequest setEntity:entity];
	
	NSSortDescriptor *sort = [[NSSortDescriptor alloc]
							  initWithKey:@"playerName" ascending:YES];
	
	[fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
	
	NSFetchedResultsController *theFetchedResultsController =
	[[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
										managedObjectContext:_managedObjectContext
										  sectionNameKeyPath:nil
												   cacheName:nil];
	
	self.fetchedResultsController = theFetchedResultsController;
	_fetchedResultsController.delegate = self;
	
	return _fetchedResultsController;
}

@end
