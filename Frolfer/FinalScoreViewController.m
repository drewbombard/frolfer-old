//
//  FinalScoreViewController.m
//  Frolfer
//
//  Created by Drew Bombard on 1/23/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import "FinalScoreViewController.h"

@interface FinalScoreViewController ()

@end

@implementation FinalScoreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	_appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [_appDelegate managedObjectContext];
	[context setUndoManager:nil];
	_managedObjectContext = context;
	
	self.tableView.delegate = self;
	
	[self customizeInterface];
	
}

- (void)viewWillAppear:(BOOL)animated {
	
	[super viewWillAppear:animated];
	
	
	NSError *error;
	if (![[self fetchedResultsController] performFetch:&error]) {
		// Update to handle the error appropriately.
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		exit(-1);  // Fail
	}

	[self gameDataCheck];

	[self gameLeaderCheck];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.

	self.fetchedResultsController = nil;
}

-(void)customizeInterface {
	_btnStartOver.adjustsImageWhenHighlighted = NO;
	_btnStartOver.layer.cornerRadius =  _btnStartOver.frame.size.height/6;
	_btnStartOver.layer.masksToBounds = YES;
	
	// make new layer to contain shadow and masked image
	CALayer *winnerCardShadowLayer = _winnerCard.layer;
	winnerCardShadowLayer.masksToBounds = NO;
	winnerCardShadowLayer.cornerRadius = 5.0;
	winnerCardShadowLayer.shadowOffset = CGSizeMake(0.0f, 3.0f);
	winnerCardShadowLayer.shadowRadius = 5.5;
	winnerCardShadowLayer.shadowOpacity = 0.23;
}


-(void)addImageOnTopOfTheNavigationBar {
	UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"player_empty"]];
	[self.navigationController.view addSubview:imageView];
}






-(void)gameDataCheck {
	if (_gameDataArray == nil) {
		_gameDataArray = [FetchDataArray dataFromEntity:@"Games" predicateName:nil predicateValue:nil predicateType:nil sortName:nil sortASC:nil];
		
		if ([_gameDataArray count] >= 1) {
			_game_info = [_gameDataArray objectAtIndex:0];
		}
		
		Courses *courseObj = _game_info.course;
		_totalHoles = [courseObj.numHoles intValue];
	}
}

-(void)gameLeaderCheck {
	
	_gameStatsDict = [GameStats gameLeader:_fetchedResultsController];
	
	NSLog(@"_gameStatsDict: %@", _gameStatsDict);
	NSLog(@"tie: %@", [_gameStatsDict objectForKey:@"tie"]);

	
//	Players *highScoringPlayer = [_gameStatsDict objectForKey:@"highScoringPlayerObject"];
	Players *lowScoringPlayer = [_gameStatsDict objectForKey:@"lowScoringPlayerObject"];
	
	NSString *strLowScoringPlayer;
	
	if (lowScoringPlayer) {
		
		if ( [[_gameStatsDict objectForKey:@"tie"] intValue] == 1 ) { //== [NSNumber numberWithInt:1] ) {
			strLowScoringPlayer = @"Tie Game!";
		} else {
			strLowScoringPlayer = [_gameStatsDict objectForKey:@"lowScoringPlayerName"];
		}
		
		_lblPlayerName.text = strLowScoringPlayer;
		
		if ([[_gameStatsDict objectForKey:@"lowScoringPlayerNickName"]  isEqual: @""]) {
			_lblPlayerNickName.text = nil;
		} else {
			_lblPlayerNickName.text = [@"\"" stringByAppendingFormat:@"%@%@", [_gameStatsDict objectForKey:@"lowScoringPlayerNickName"], @"\""];
		}
				
		NSString* formattedNumber = [NSString stringWithFormat:@"%.02f", [[_gameStatsDict objectForKey:@"lowScore"] floatValue] / _totalHoles];
		_lblParAverage.text = [NSString stringWithFormat:@"Average Par: %@", formattedNumber];
		_lblTotalPoints.text = [[_gameStatsDict objectForKey:@"lowScoreTotal"] stringValue];
		_lblParLow.text = [NSString stringWithFormat:@"Lowest Par: %@", [[_gameStatsDict objectForKey:@"lowScore"] stringValue]];

		UIImage *cdImage = [UIImage imageWithData: lowScoringPlayer.playerPhoto];
		if (cdImage != nil) {
			_imgPlayer.image = cdImage;
			_imgPlayer.layer.cornerRadius = _imgPlayer.frame.size.width / 2;
			_imgPlayer.clipsToBounds = YES;
		}
	} else {
		_lblPlayerName.text = nil;
		_lblPlayerNickName.text = nil;
		_lblTotalPoints.text = nil;
	}
	
}








#pragma mark - Table Data
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 60;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	id  sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
	return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *cellIdentifier = @"playerCell";
	ScoringCompleteCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	
	if (cell == nil) {
		cell = [[ScoringCompleteCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
	}
	if (indexPath.row % 2 == 0) {
		cell.backgroundColor = [UIColor colorWithRed:0.98 green:0.98 blue:0.98 alpha:1];
	} else {
		cell.backgroundColor = [UIColor whiteColor];
	}
	
	[self configureCell:cell atIndexPath:indexPath];
	return cell;
}
- (void)configureCell:(ScoringCompleteCell *)cell atIndexPath:(NSIndexPath *)indexPath {
	
	NSLog(@"\n\n\nconfigureCell()");
	
	NSLog(@"indexPath for cell: %@", indexPath);
	NSLog(@"\n");
	
	NSString *cell_score = 0;
	Players *player_info = [_fetchedResultsController objectAtIndexPath:indexPath];
	NSSet *scoreSet = player_info.scores;
	
	int playerTotalScore = 0;
	
	for (Scoring *scoreObj in scoreSet){
		if (scoreObj.score) {
			playerTotalScore = playerTotalScore + [[scoreObj.score stringValue] intValue];
		}
	}

	cell_score = [NSString stringWithFormat:@"%d", playerTotalScore];

	if (player_info == [_gameStatsDict objectForKey:@"lowScoringPlayerObject"]) {
		NSLog(@"player_info: %@", player_info.playerName);
		cell.imgWinnerCheck.image = [UIImage imageNamed:@"row_check_orange"];
	} else {
		cell.imgWinnerCheck.image = nil;
	}
	
	
	if ( [cell_score  isEqual: @"0"] ) {
		cell.lblPlayerScore.text = @"--";
	} else {
		cell.lblPlayerScore.text = cell_score;
	}
	cell.lblPlayerName.text = player_info.playerName;
	
	UIImage *cdImage = [UIImage imageWithData: player_info.playerPhoto];
	if (cdImage != nil) {
		cell.imgPlayerPhoto.image = cdImage;
		cell.imgPlayerPhoto.layer.cornerRadius = cell.imgPlayerPhoto.frame.size.width / 2;
		cell.imgPlayerPhoto.clipsToBounds = YES;
	} else {
		cell.imgPlayerPhoto.image = [UIImage imageNamed:@"player_row_empty"];
	}
		
	
	NSLog(@"\n");
}



#pragma mark - fetchedResultsController
-(NSFetchedResultsController *)fetchedResultsController {
	
	if (_fetchedResultsController != nil) {
		return _fetchedResultsController;
	}
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription
								   entityForName:@"Players"
								   inManagedObjectContext:_managedObjectContext];
	[fetchRequest setEntity:entity];
	
	NSSortDescriptor *sort = [[NSSortDescriptor alloc]
							  initWithKey:@"playerName" ascending:YES];
	
	[fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
	
	NSFetchedResultsController *theFetchedResultsController =
	[[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
										managedObjectContext:_managedObjectContext
										  sectionNameKeyPath:nil
												   cacheName:nil];
	
	self.fetchedResultsController = theFetchedResultsController;
	_fetchedResultsController.delegate = self;
	
	return _fetchedResultsController;
}



#pragma mark - Alert(s)

-(IBAction)startOver:(id)sender {
	
	_game_info.gameComplete = [NSNumber numberWithInt:1];
	[self.managedObjectContext save:nil];  // write to database

	[self.tabBarController setSelectedIndex:1];
}



@end
