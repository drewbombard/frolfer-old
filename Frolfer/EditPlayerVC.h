//
//  EditPlayerVC.h
//  Frolfer
//
//  Created by Drew Bombard on 8/7/12.
//  Copyright (c) 2015 default_method All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreImage/CoreImage.h>
#import <MobileCoreServices/UTCoreTypes.h>

// Data
#import "Players+CoreDataClass.h"

// Utilities
#import "Colors.h"
#import "BSKeyboardControls.h"
#import "UIImage+fixOrientation.h"


@class EditPlayerVC;
@protocol EditPlayerVCDelegate <NSObject>
-(void)theAddButtonOnEditPlayerVCWasTapped:(EditPlayerVC *)controller;
-(void)theDeleteButtonOnEditPlayerVCWasTapped:(EditPlayerVC *)controller;
-(void)theRemoveButtonOnEditPlayerVCWasTapped:(EditPlayerVC *)controller;
-(void)theSaveButtonOnThePlayerDetailTVCWasTapped:(EditPlayerVC *)controller;
@end

@protocol EditGamePlayerDelegate <NSObject>
-(void)theGamePlayerSaveButtonWasTapped:(EditPlayerVC *)controller;
@end


@interface EditPlayerVC:UIViewController <
						UIImagePickerControllerDelegate,
						UINavigationControllerDelegate,
						NSFetchedResultsControllerDelegate,
						UIActionSheetDelegate,
						BSKeyboardControlsDelegate,
						UITextFieldDelegate,
						UITextViewDelegate,UIActionSheetDelegate> {
	
	CGFloat animatedDistance;
	UIImage *playerImg;
}

-(void)displayPlayerInfo;



@property (strong, nonatomic) CALayer* containerLayer;



@property (nonatomic) IBOutlet UIView *overlayView;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *takePictureButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *startStopButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *delayedPhotoButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *doneButton;

@property (nonatomic) UIImagePickerController *imagePickerController;

@property (nonatomic) NSMutableArray *capturedImages;







@property (strong, nonatomic) IBOutlet UIControl *backgroundView;

@property (weak, nonatomic) id <EditPlayerVCDelegate> delegate;
@property (weak, nonatomic) id <EditGamePlayerDelegate> editDelegate;

@property (strong, nonatomic) Games *game_info;
@property (strong, nonatomic) NSArray *gameData;

@property (strong, nonatomic) Players *player;
@property (strong, nonatomic) NSArray *playerData;
@property (strong, nonatomic) NSArray *selectedGamePlayerData;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (strong, nonatomic) IBOutlet UILabel *lblEdit;
@property (strong, nonatomic) NSDate *today;

@property (nonatomic, weak) IBOutlet UIImageView *imageView;

@property (strong, nonatomic) IBOutlet UIView *imgPlayerContainer;
@property (strong, nonatomic) IBOutlet UIImageView *imgPlayer;
@property (strong, nonatomic) IBOutlet UITextField *txtPlayerName;
@property (weak, nonatomic) IBOutlet UITextField *txtPlayerNickName;

@property (strong, nonatomic) IBOutlet UIButton *btnTakePicture;
-(IBAction)savePlayer:(id)sender;
-(IBAction)showActionSheetDeleteConfirm:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnDeletePlayer;
@property (strong, nonatomic) IBOutlet UIButton *btnSavePlayer;

@property (assign) BOOL gamePlayer;


// used for interacting with the keyboard, and moving the view
@property (nonatomic, strong) BSKeyboardControls *keyboardControls;
-(IBAction)dismissKeyboard:(id)sender;
-(void)setupKeyboardControls;
-(void)scrollViewToTextField:(id)textField;

@end
