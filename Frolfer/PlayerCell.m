//
//  PlayerCell.m
//  Frolfer
//
//  Created by Drew Bombard on 2/22/13.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import "PlayerCell.h"

@implementation PlayerCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self) {
		// Initialization code
	}
	return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	[super setSelected:selected animated:animated];

	// Configure the view for the selected state
}

@end
