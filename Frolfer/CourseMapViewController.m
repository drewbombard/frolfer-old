//
//  CourseMapViewController.m
//  Frolfer
//
//  Created by Drew Bombard on 11/3/12.
//  Copyright (c) 2016 default_method. All rights reserved.
//

#import "CourseMapViewController.h"
#import "CourseLocation.h"

@interface CourseMapViewController ()

@end

@implementation CourseMapViewController


#define METERS_PER_MILE 1609.344


#pragma mark - View lifecycle
- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

	_locationManager = [[CLLocationManager alloc] init];
	_locationManager.delegate = self;
	
	
	[self.locationManager startUpdatingLocation];
	[self.locationManager requestWhenInUseAuthorization];
	
	[self.mapView setShowsUserLocation:YES];
	[self.mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
}


- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	// 1
	CLLocationCoordinate2D zoomLocation;
	zoomLocation.latitude = [_locLat doubleValue];
	zoomLocation.longitude = [_locLong doubleValue];	
	
	// 2
	MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(
																	   zoomLocation,
																	   10.5*METERS_PER_MILE,
																	   10.5*METERS_PER_MILE);
	//MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(zoomLocation, 800, 800);
	
	// 3
	MKCoordinateRegion adjustedRegion = [_mapView regionThatFits:viewRegion];
	
	// 4
	[_mapView setRegion:adjustedRegion animated:YES];
	
	_mapView.showsUserLocation = YES;
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	[self plotCoursePosition];
}

- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotate
{
	UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
	if (orientation == UIInterfaceOrientationPortrait) {
		// your code for portrait mode
	}
	return YES;
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Release any cached data, images, etc that aren't in use.

	_mapView = nil;
}


#
#pragma mark - Mapping

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
	MKCoordinateRegion region = { { 0.0, 0.0 }, { 0.0, 0.0 } };
	region.center.latitude = self.locationManager.location.coordinate.latitude;
	region.center.longitude = self.locationManager.location.coordinate.longitude;
	region.span.latitudeDelta = 0.0187f;
	region.span.longitudeDelta = 0.0137f;
	
	
	/*
	 * Un-comment this to automatically show the user's
	 * location as the default point on the map
	 */
	//[self.mapView setRegion:region animated:YES];
	//_initialPosition = NO;
}

-(void)plotCoursePosition {
	
	for (id<MKAnnotation> annotation in _mapView.annotations) {
		[_mapView removeAnnotation:annotation];
	}
		
	CLLocationCoordinate2D coordinate;
	coordinate.latitude = [_locLat doubleValue];
	coordinate.longitude = [_locLong doubleValue];
	CourseLocation *annotation = [[CourseLocation alloc]
							  initWithName:_courseName
							  address:_locCityState
							  coordinate:coordinate];
	
	[_mapView addAnnotation:annotation];
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
	
	static NSString *identifier = @"CourseLocation";
	if ([annotation isKindOfClass:[CourseLocation class]]) {
		
		MKPinAnnotationView *annotationView = (MKPinAnnotationView *) [_mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
		
		if (annotationView == nil) {
			annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
		} else {
			annotationView.annotation = annotation;
		}
		
		annotationView.enabled = YES;
		annotationView.canShowCallout = YES;
		annotationView.animatesDrop = YES;
		
		annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
		
		return annotationView;
	}
	
	return nil;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
	
	CourseLocation *location = (CourseLocation*)view.annotation;
	
	NSLog(@"location: %@", location);
	
	double destinationLatitude, destinationLongitude;
	destinationLatitude = location.coordinate.latitude;
	destinationLongitude = location.coordinate.longitude;
	
	CLLocationCoordinate2D coordinate =	CLLocationCoordinate2DMake(destinationLatitude,destinationLongitude);
	
	MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coordinate addressDictionary:nil];
	MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
	[mapItem setName:_courseName];
	
	// Set the directions mode to "Driving"
	// Can use MKLaunchOptionsDirectionsModeDriving instead
	NSDictionary *launchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving};
	
	// Get the "Current User Location" MKMapItem
	MKMapItem *currentLocationMapItem = [MKMapItem mapItemForCurrentLocation];
	
	// Pass the current location and destination map items to the Maps app
	// Set the direction mode in the launchOptions dictionary
	[MKMapItem openMapsWithItems:@[currentLocationMapItem, mapItem] launchOptions:launchOptions];
}


@end
