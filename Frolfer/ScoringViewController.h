//
//  ScoringViewController.h
//  Frolfer
//
//  Adapted and modified by Drew Bombard
//  Copyright (c) 2015 default_method All rights reserved.

//  Created (originally) by Simon on 24/11/13.
//  Copyright (c) 2015 Appcoda. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "PageContentViewController.h"

// Data
#import "Games+CoreDataClass.h"
#import "Courses.h"


// Utilities
#import "Colors.h"
#import "CoreDataCheck.h"
#import "FetchDataArray.h"



@interface ScoringViewController : UIViewController <UIPageViewControllerDataSource,NSFetchedResultsControllerDelegate,UIAlertViewDelegate>



//- (IBAction)startWalkthrough:(id)sender;
@property (strong, nonatomic) UIPageViewController *pageViewController;


@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) Games *game_info;
@property (strong, nonatomic) NSArray *gameDataArray;
@property (strong, nonatomic) NSSet *courseDataSet;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;



// DEBUG / TEMP / TRASH
@property (strong, nonatomic) IBOutlet UIButton *btnTEMPClear;
-(IBAction)tempClear:(id)sender;





-(IBAction)previousHole:(id)sender;
-(IBAction)nextHole:(id)sender;




@property (assign, nonatomic) int returnHoleIndex;



@property (strong, nonatomic) IBOutlet UIButton *btnPreviousHole;
@property (strong, nonatomic) IBOutlet UIButton *btnNextHole;

@property (assign, nonatomic) int currentHole; // Different from the page index, this is what the user see's.
@property (assign, nonatomic) int currentHoleIndex;
@property (assign, nonatomic) int totalHoles;
@property (assign, nonatomic) int totalPageCount;



@end
