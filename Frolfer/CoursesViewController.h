//
//  CoursesViewController.h
//  Frolfer
//
//  Created by Drew Bombard on 4/1/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"
#import "CourseDetailVC.h"


// Data
#import "CoreDataCheck.h"
#import "FetchDataArray.h"
#import "Courses.h"


@interface CoursesViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,NSFetchedResultsControllerDelegate> {
	
	
	
	NSArray *courses;
	NSArray *searchResults;
}


@property (nonatomic, strong) UISearchController *searchController;


@property (strong, nonatomic) IBOutlet UIImageView *imgLocalState;


@property (assign) BOOL gameCourse;

@property (strong, nonatomic) AppDelegate *appDelegate;

@property (strong, nonatomic) IBOutlet UILabel *lblLocalStateLong;
@property (strong, nonatomic) IBOutlet UILabel *lblLocalStateShort;

@property (strong, nonatomic) IBOutlet UILabel *lblCourseCount;

@property (strong,nonatomic) IBOutlet UITableView* tableView;
@property (strong, nonatomic) NSString *selectedState;
@property (strong, nonatomic) NSString *selectedStateAbbreviation;

@property (strong, nonatomic) NSArray *courseArray;
@property (strong, nonatomic) Courses *selectedCourse;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;



@end
