//
//  CourseLocation.h
//  Frolfer
//
//  Created by Drew Bombard on 11/4/12.
//  Copyright (c) 2015 default_method All rights reserved.
//

#import <MapKit/MapKit.h>
#import <Foundation/Foundation.h>
#import <Contacts/Contacts.h>


@interface CourseLocation : NSObject <MKAnnotation>


-(id)initWithName:(NSString*)name address:(NSString*)address coordinate:(CLLocationCoordinate2D)coordinate;

-(MKMapItem*)mapItem;

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, assign) CLLocationCoordinate2D theCoordinate;

@end
