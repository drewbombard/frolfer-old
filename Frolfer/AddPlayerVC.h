//
//  AddPlayerVC.h
//  Frolfer
//
//  Created by Drew Bombard on 8/7/12.
//  Copyright (c) 2015 default_method All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreImage/CoreImage.h>
#import <MobileCoreServices/UTCoreTypes.h>

// Data
#import "Players+CoreDataClass.h"

// Utilities
#import "Colors.h"
#import "BSKeyboardControls.h"
#import "UIImage+fixOrientation.h"


@class AddPlayerVC;
@protocol AddPlayerVCDelegate <NSObject>
- (void)theSaveAndAddButtonWasTapped:(AddPlayerVC *)controller;
- (void)theSaveButtonOnThePlayerAddTVCWasTapped:(AddPlayerVC *)controller;
@end

@interface AddPlayerVC:UIViewController <
						UIImagePickerControllerDelegate,
						UINavigationControllerDelegate,
						NSFetchedResultsControllerDelegate,
						BSKeyboardControlsDelegate,
						UITextFieldDelegate,
						UITextViewDelegate,UIActionSheetDelegate> {
							
	CGFloat animatedDistance;
	UIImage *playerImg;
}


@property (strong, nonatomic) IBOutlet UIControl *backgroundView;

@property (strong, nonatomic) Players *player;
@property (strong, nonatomic) id <AddPlayerVCDelegate> delegate;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) IBOutlet UIButton *btnSavePlayer;
@property (strong, nonatomic) IBOutlet UIImageView *imgPlayer;


@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic) NSMutableArray *capturedImages;

@property (nonatomic) UIImagePickerController *imagePickerController;
@property (strong, nonatomic) IBOutlet UIView *imgPlayerContainer;

@property (strong, nonatomic) IBOutlet UITextField *txtPlayerName;
@property (strong, nonatomic) IBOutlet UITextField *txtPlayerNickName;

@property (strong, nonatomic) IBOutlet UIButton *btnTakePicture;
-(IBAction)savePlayer:(id)sender;


// used for interacting with the keyboard, and moving the view
@property (nonatomic, strong) BSKeyboardControls *keyboardControls;
-(IBAction)dismissKeyboard:(id)sender;
-(void)setupKeyboardControls;
-(void)scrollViewToTextField:(id)textField;

@end
