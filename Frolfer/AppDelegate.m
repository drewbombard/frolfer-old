//
//  AppDelegate.m
//  Frolfer
//
//  Created by Drew Bombard on 8/21/13.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import "AppDelegate.h"

#import "LoginViewController.h"



@implementation AppDelegate

@synthesize window = _window;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
	
	//instantiate local context
	NSManagedObjectContext *context = [self managedObjectContext];
	if (!context) {
		// Handle the error.
		NSLog(@"Error: Context is null");
	}

	
	// Crash Reporting / Analytics
	[FIRApp configure];


	[self customizeInterface];

	LoginViewController *rootViewController = [LoginViewController alloc];
	rootViewController.managedObjectContext = context;
	
	// Override point for customization after application launch.
    return YES;
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {

	NSLog(@"\n");
	NSLog(@"\n");
	
}

- (void)customizeInterface {
	
	[self defineHeightWidth];
	[self defineDeviceSize];
	
	[self getCurrentLocation];

	
	
	_window.backgroundColor = [UIColor whiteColor];
	
	
	[[UINavigationBar appearance] setBarTintColor:[[Colors get] darkBlue]];


	[[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[UIToolbar class]]]
	 setTintColor:[UIColor whiteColor]];

	
	
	[[UITabBar appearance] setTintColor:[UIColor whiteColor]];
	[[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil]];
	//[[UINavigationBar appearance] setTintColor:[UIColor blueColor]];
	//[[UINavigationBar appearance] setBackgroundColor:[UIColor blueColor]];
	

	//#### TABBAR colors ####//
	// Add this if you only want to change Selected Image color
	// and/or selected image text
//	[[UITabBar appearance] setTintColor:[UIColor redColor]];
	
	[[UITabBar appearance] setBarTintColor:[[Colors get] darkBlue]];
	
	// Add this code to change StateNormal text Color,
	[UITabBarItem.appearance setTitleTextAttributes:
	 @{NSForegroundColorAttributeName : [[Colors get] tabTextBlue]}
										   forState:UIControlStateNormal];
	
	// then if StateSelected should be different, you should add this code
	[UITabBarItem.appearance setTitleTextAttributes:
	 @{NSForegroundColorAttributeName : [UIColor whiteColor]}
										   forState:UIControlStateSelected];
}


-(void)defineHeightWidth {
	CGRect screenRect = [[UIScreen mainScreen] bounds];
	_screenWidth = screenRect.size.width;
	_screenHeight = screenRect.size.height;
}

-(void)defineDeviceSize {
	
	UIDevice *device = [UIDevice currentDevice];
	//UIDeviceOrientation currentOrientation = device.orientation;
	BOOL isPhone = (device.userInterfaceIdiom == UIUserInterfaceIdiomPhone);
	
//	NSLog(@"mainScreen height: %f", [[UIScreen mainScreen] bounds].size.height);
//	NSLog(@"mainScreen width: %f", [[UIScreen mainScreen] bounds].size.width);
	
	int heightComparison = [[UIScreen mainScreen] bounds].size.height;
	NSLog(@"heightComparison: %d", heightComparison);

	if (isPhone == YES) {
		// Do Portrait Phone Things
		switch (heightComparison) {
			case 480:
				NSLog(@"Legacy Phone");
				_isPhoneLegacy = YES;
				break;
			case 568:
				NSLog(@"iPhone 5/iPhone SE");
				_isPhoneLegacy = YES;
				break;
			case 667:
				NSLog(@"iPhone 6/7/8");
				_isPhone6 = YES;
				break;
			case 736:
				NSLog(@"iPhone 6/7/8 Plus");
				_isPhone6Plus = YES;
				break;
			case 812:
				NSLog(@"iPhone X");
				_isPhoneX = YES;
				break;
			default:
				break;
		}
		_deviceHeight = heightComparison;
	}
}



-(void)getCurrentLocation {
	
	_locationFound = 0;
	
	geocoder = [[CLGeocoder alloc] init];
	
	locationManager = [[CLLocationManager alloc] init];
	[locationManager startUpdatingLocation];
	[locationManager requestWhenInUseAuthorization];
	locationManager.delegate = self;
	locationManager.desiredAccuracy = kCLLocationAccuracyBest;
	
	[locationManager startUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
	NSLog(@"didFailWithError: %@", error);
	
	_locationFound = 0;	
//	UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error"
//														 message:@"Failed to Get Your Location"
//														delegate:nil
//											   cancelButtonTitle:@"OK"
//											   otherButtonTitles:nil];
//	[errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
	CLLocation *currentLocation = [locations lastObject];
	if (currentLocation != nil) {
		_currentLat = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
		_currentLong = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
	}
	
	// Stop Location Manager
	[locationManager stopUpdatingLocation];
	
	// Reverse Geocoding
	NSLog(@"\n\n\nResolving the Address:");
	[geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
		NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
		if (error == nil && [placemarks count] > 0) {
			
			self->placemark = [placemarks lastObject];
			
			self->_locationFound = 1;
			
			self->_currentStateShort = self->placemark.administrativeArea;
			self->_currentStateLong = self->placemark.shortState;
			
			// NSLog(@"%@ ", self->placemark.shortState);
			// NSLog(@"\n");
			
		} else {
			NSLog(@"%@", error.debugDescription);
		}
	} ];
}



							
- (void)applicationWillResignActive:(UIApplication *)application {
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.

	NSLog(@"applicationWillResignActive");
	NSLog(@"\n");
	
	// TRASH / TEMP ?!?!
	_game_info.pageContentLoaded = [NSNumber numberWithInt:0];

}

- (void)applicationDidEnterBackground:(UIApplication *)application {
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	
	NSLog(@"applicationDidEnterBackground");
	NSLog(@"\n");
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.

	
	// TRASH / TEMP
	NSLog(@"\n\napplicationWillEnterForeground");
	
	NSLog(@"game_info: %@",_game_info);
	
	NSLog(@"\n");
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	
// TRASH / TEMP
	NSLog(@"\n\napplicationDidBecomeActive");
	NSLog(@"game_info: %@",_game_info);

	NSLog(@"\n");
}

- (void)applicationWillTerminate:(UIApplication *)application {
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
	
	NSLog(@"applicationWillTerminate");
	NSLog(@"\n");
}



#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
	// The directory the application uses to store the Core Data store file. This code uses a directory named "com.defaultmethod.Frolfer" in the application's documents directory.
	return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
	// The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
	if (_managedObjectModel != nil) {
		return _managedObjectModel;
	}
	NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Frolfer" withExtension:@"momd"];
	_managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
	return _managedObjectModel;
}

//*
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
	// The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
	if (_persistentStoreCoordinator != nil) {
		return _persistentStoreCoordinator;
	}
	
	// Create the coordinator and store
	
	_persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
	NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Frolfer.sqlite"];
	NSError *error = nil;
	NSString *failureReason = @"There was an error creating or loading the application's saved data.";
	
//	if (![[NSFileManager defaultManager] fileExistsAtPath:[storeURL path]]) {
//		NSURL *preloadURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Frolfer" ofType:@"sqlite"]];
//		NSError* err = nil;
//		
//		if (![[NSFileManager defaultManager] copyItemAtURL:preloadURL toURL:storeURL error:&err]) {
//			NSLog(@"Oops, could copy preloaded data");
//		}
//	}
	
	// handle db upgrade
	NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
							 [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
							 [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
	
	
	if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
		// Report any error we got.
		NSMutableDictionary *dict = [NSMutableDictionary dictionary];
		dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
		dict[NSLocalizedFailureReasonErrorKey] = failureReason;
		dict[NSUnderlyingErrorKey] = error;
		error = [NSError errorWithDomain:@"com.defaultmethod" code:9999 userInfo:dict];
		// Replace this with code to handle the error appropriately.
		// abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		abort();
	}
	
	return _persistentStoreCoordinator;
}
//*/

/*
// Use this only when re-running and pre-building a new sqlite file
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
	if (_persistentStoreCoordinator != nil) {
		return _persistentStoreCoordinator;
	}
	
	NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Frolfer.sqlite"];
	
	NSError *error = nil;
	_persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
	if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		abort();
	}
	
	return _persistentStoreCoordinator;
}
//*/


- (NSManagedObjectContext *)managedObjectContext {
	// Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
	if (_managedObjectContext != nil) {
		return _managedObjectContext;
	}
	
	NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
	if (!coordinator) {
		return nil;
	}
	_managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
	[_managedObjectContext setPersistentStoreCoordinator:coordinator];
	return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
	NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
	if (managedObjectContext != nil) {
		NSError *error = nil;
		if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
			// Replace this implementation with code to handle the error appropriately.
			// abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
			NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
			abort();
		}
	}
}




@end
