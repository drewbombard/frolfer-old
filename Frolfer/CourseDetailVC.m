//
//  CourseDetailVC.m
//  Frolfer
//
//  Created by Drew Bombard on 7/17/12.
//  Copyright (c) 2015 default_method All rights reserved.
//

#import "CourseDetailVC.h"

@interface CourseDetailVC ()

@end

@implementation CourseDetailVC

-(void)viewDidLoad {
	
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	
	AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [appDelegate managedObjectContext];
	[context setUndoManager:nil];
	_managedObjectContext = context;
	
	[_scrollView setScrollEnabled:YES];
	[_scrollView setContentSize:CGSizeMake(300, 376)];
	
	[self customizeInterface];
	
	[self setupText];
	[self checkCurrentGame];
	
	
	
	NSLog(@"_game_info: %@",_game_info);

	
	
	// Hide the 'select' button if they are just in to look at course details..
	if (_gameCourse == NO) {
		//		_btnSaveCourse.hidden = YES;
		self.navigationItem.rightBarButtonItem = nil;
	}
	
	NSLog(@"_selectedCourse _locLat: %@", _selectedCourse.locLat);
	NSLog(@"_selectedCourse _locLong %@", _selectedCourse.locLong);
	NSLog(@"");
	
	NSLog(@"");
	NSLog(@"_courseName: %@",_courseName);
	NSLog(@"_gameCourse: %@",_gameCourse ? @"Yes" : @"No");
}


-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
	[self setLblNumHoles:nil];
	[self setLblTarget:nil];
	[self setLblTee:nil];
	[self setLblElevation:nil];
	[self setLblFoliage:nil];
	[self setLblLength:nil];
	[self setLblLengthAlt:nil];
	[self setLocCityStateZip:nil];
	[self setLblBathrooms:nil];
	[self setLblPrivateCourse:nil];
	[self setLblHandicap:nil];
	[self setLblCoursePhone:nil];
	[self setLblPayToPlay:nil];
	[self setLblTeeSigns:nil];
}


- (BOOL)shouldAutorotate
{
	UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
	if (orientation == UIInterfaceOrientationPortrait) {
		// your code for portrait mode
	}
	return YES;
}


//#################################################
//#################################################


-(void)customizeInterface {
	_btnSelectCourse.backgroundColor = [Colors get].btnGreen;
	_btnSelectCourse.titleLabel.textColor = [UIColor whiteColor];
	
	_btnSelectCourse.layer.cornerRadius =  _btnSelectCourse.frame.size.height/6;
	_btnSelectCourse.layer.masksToBounds = YES;
}


-(void)setupText {
	
	NSLog(@"selectedCourse: %@", _selectedCourse);
	NSLog(@"name from _courseName: %@",	_courseName);
	
//	NSLog(@"_selectedCourse.numHoles: %@" , _selectedCourse.numHoles);
//	NSLog(@"_selectedCourse.typeTarget: %@" , _selectedCourse.typeTarget);
//	NSLog(@"_selectedCourse.typeTee: %@" , _selectedCourse.typeTee);
//	NSLog(@"_selectedCourse.elevation: %@" , _selectedCourse.elevation);
//	NSLog(@"_selectedCourse.foliage: %@" , _selectedCourse.foliage);
//	NSLog(@"_selectedCourse.lengthMain: %@" , _selectedCourse.lengthMain);
//	NSLog(@"_selectedCourse.lengthAlternate: %@" , _selectedCourse.lengthAlternate);
//	NSLog(@"_selectedCourse.teeSigns: %@" , _selectedCourse.teeSigns);
//	NSLog(@"_selectedCourse.bathrooms: %@" , _selectedCourse.bathrooms);
//	NSLog(@"_selectedCourse.privateCourse: %@" , _selectedCourse.privateCourse);
//	NSLog(@"_selectedCourse.payToPlay: %@" , _selectedCourse.payToPlay);
//	NSLog(@"_selectedCourse.handicapAccessible: %@" , _selectedCourse.handicapAccessible);
//	NSLog(@"_selectedCourse.coursePhone: %@" , _selectedCourse.coursePhone);


	
	NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
	[numberFormatter setFormatterBehavior: NSNumberFormatterBehavior10_4];
	[numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
	
	NSString *cityState = [[_selectedCourse.locCity stringByAppendingString:@", "] stringByAppendingString:_selectedCourse.locStateAbbreviation];
	_cityStateZip = [[cityState stringByAppendingString:@", "] stringByAppendingString:_selectedCourse.locZip];
	
	
	// Set Course Name
	_lblCourseName.text = _selectedCourse.courseName;
	
	
	
	if ([_selectedCourse.locCity isEqual: @""]) {
		[_vTarget removeFromSuperview];
	} else {
		_lblLocCity.text = [NSString stringWithFormat:@"%@, %@", _selectedCourse.locCity, _selectedCourse.locStateAbbreviation];
	}
	
	
	// Remove views that don't have data
	if ([_selectedCourse.numHoles isEqual: @""]) {
		[_vElevation removeFromSuperview];
	} else {
		_lblNumHoles.text = [NSString stringWithFormat:@"%@", _selectedCourse.numHoles];
	}
	
	if ([_selectedCourse.typeTarget isEqual: @""]) {
		[_vTarget removeFromSuperview];
	} else {
		_lblTarget.text = _selectedCourse.typeTarget;
	}
	
	if ([_selectedCourse.typeTee isEqual: @""]) {
		[_vTee removeFromSuperview];
	} else {
		_lblTee.text = _selectedCourse.typeTee;
	}
	
	if ([_selectedCourse.elevation isEqual: @""]) {
		[_vElevation removeFromSuperview];
	} else {
		_lblElevation.text = _selectedCourse.elevation;
	}
	
	if ([_selectedCourse.foliage isEqual: @""]) {
		[_vFoliage removeFromSuperview];
	} else {
		_lblFoliage.text = _selectedCourse.foliage;
	}
	
	if ([_selectedCourse.lengthMain isEqual: @""] ||
		[[NSString stringWithFormat:@"%@",_selectedCourse.lengthMain] isEqualToString: @"0" ]){
		[_vLength removeFromSuperview];
	} else {
		_lblLength.text = [numberFormatter stringFromNumber: _selectedCourse.lengthMain];
	}
	
	if ([_selectedCourse.lengthAlternate isEqual: @""] ||
		[[NSString stringWithFormat:@"%@",_selectedCourse.lengthAlternate] isEqualToString: @"0" ]){
		[_vLengthAlt removeFromSuperview];
	} else {
		_lblLengthAlt.text = [numberFormatter stringFromNumber: _selectedCourse.lengthAlternate];
	}
	
	if ([_selectedCourse.teeSigns isEqual: @""]) {
		[_vTeeSigns removeFromSuperview];
	} else {
		_lblTeeSigns.text = _selectedCourse.teeSigns;
	}
	
	if ([_selectedCourse.bathrooms isEqual: @""]) {
		[_vBathrooms removeFromSuperview];
	} else {
		_lblBathrooms.text = _selectedCourse.bathrooms;
	}
	
	if ([_selectedCourse.privateCourse isEqual: @""]) {
		[_vPrivateCourse removeFromSuperview];
	} else {
		_lblPrivateCourse.text = _selectedCourse.privateCourse;
	}
	
	if ([_selectedCourse.payToPlay isEqual: @""]) {
		[_vPayToPlay removeFromSuperview];
	} else {
		_lblPayToPlay.text = _selectedCourse.payToPlay;
	}
	
	if ([_selectedCourse.handicapAccessible isEqual: @""]) {
		[_vHandicap removeFromSuperview];
	} else {
		_lblHandicap.text = _selectedCourse.handicapAccessible;
	}
	
	if ([_selectedCourse.coursePhone isEqual: @""]) {
		[_vCoursePhone removeFromSuperview];
		[_btnCoursePhone removeFromSuperview];
	} else {
		_lblCoursePhone.text = _selectedCourse.coursePhone;
	}
}


-(IBAction)linkPhone:(id)sender {
	
	NSString *phoneNumber = [@"tel://" stringByAppendingString:_selectedCourse.coursePhone];
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showCourseMap"]) {

        CourseMapViewController *destViewController = segue.destinationViewController;
		
		destViewController.courseName = _selectedCourse.courseName;
		destViewController.locCityState = _cityStateZip;
		destViewController.locLat = _selectedCourse.locLat;
		destViewController.locLong = _selectedCourse.locLong;
		
		NSLog(@"_selectedCourse Lat:  %@",_selectedCourse.locLat);
		NSLog(@"_selectedCourse Long: %@",_selectedCourse.locLong);
		NSLog(@"");
    } else if ([segue.identifier isEqualToString:@"segueSelectCourse"]) {
		
		NSLog(@"");
		NSLog(@"courseName %@", _selectedCourse.courseName);
		NSLog(@"");
		
	} else {
		NSLog(@"Hey! Unidentified segue attempted!");
	}
	

	
}
-(void)saveCourseData {
	// Select / Save game course...
	NSLog(@"\n\n\nselectCourse()\n\n\n");
	
	NSLog(@"");
	NSLog(@"_gameCourse: %@",_gameCourse ? @"Yes" : @"No");
	NSLog(@"Telling the CourseDetailVC Delegate that save was tapped on the CourseDetailVC");
	NSLog(@"_courseName: %@",_courseName);
	
	
	NSLog(@"_game_info: %@",_game_info);

	
	if (
		_game_info
		&& [_game_info.gameStarted isEqual:@(1)]
		&& ([_game_info.courseChanged isEqual:@(0)] || _game_info.courseChanged == nil) ) {
		NSLog(@"Current game...");

		
		[self changeCourseAlert:nil];
		
	} else {
		NSLog(@"New game...");
		NSLog(@"Saving %@ to the currently open Game...",_selectedCourse.courseName);

		_game_info.gameDate = [NSDate date];
		_game_info.course = _selectedCourse;
		_game_info.gameCourse = _selectedCourse.courseName;
		
		[_managedObjectContext save:nil];  // write to database
		
		
		NSLog(@"_game_info: %@",_game_info);
		
		// Send the user back to the status screen...
		[self performSegueWithIdentifier:@"segueSelectCourse" sender:self];
	}
}


- (IBAction)selectCourse:(id)sender {
	
	NSLog(@"\n\n savePlayer() \n\n");
	[self saveCourseData];
}


#pragma mark - Alert(s)
-(void)changeCourseAlert:(id)sender {
	NSString *alertMessage = @"Do you want to clear current\nprogess and start over?";
	
	
	UIAlertController *alert = [UIAlertController
								 alertControllerWithTitle:@"Game in progress"
								 message:alertMessage
								 preferredStyle:UIAlertControllerStyleAlert];
	
	
	[self presentViewController:alert animated:YES completion:nil];
	
	
	UIAlertAction *okAction = [UIAlertAction
							   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
							   style:UIAlertActionStyleDefault
							   handler:^(UIAlertAction *action)
							   {
								   [self changeCourse];
								   NSLog(@"OK action");
							   }];
	
	UIAlertAction *cancelAction = [UIAlertAction
								   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
								   style:UIAlertActionStyleCancel
								   handler:^(UIAlertAction *action)
								   {
									   NSLog(@"Cancel action");
								   }];
	
	
	alertMessage = nil;
	[alert addAction:okAction];
	[alert addAction:cancelAction];
}


-(void)changeCourse {
	
	/*
	 * Delete all records of GamePlayers activity
	 */
	[DeleteData deleteFromEntity:@"Scoring" predicate:nil predicateValue:nil];
	

	_game_info.course = _selectedCourse;
	_game_info.gameCourse = _selectedCourse.courseName;
	_game_info.courseChanged = [NSNumber numberWithInt:1];

	
	NSLog(@"Saving %@ to the currently open Game...",_selectedCourse.courseName);
	[_managedObjectContext save:nil];  // write to database
	
	// Send the user back to the status screen...
	[self performSegueWithIdentifier:@"segueSelectCourse" sender:self];

	
	
}













#pragma mark - Data saving (back to Core Data)


-(void)checkCurrentGame {
	
	// 1 - Decide what Entity you want
	NSString *entityName = @"Games"; // Put your entity name here
	NSLog(@"Setting up a Fetched Results Controller for the Entity named %@", entityName);
	
	// 2 - Request that Entity
	NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entityName];
	
	// 3 - Filter it (optional)
	request.predicate = [NSPredicate predicateWithFormat:@"gameComplete == 0"];
	
	// 4 - Sort it (optional)
	request.sortDescriptors = [NSArray arrayWithObject:
							   [NSSortDescriptor sortDescriptorWithKey:@"gameDate"
															 ascending:YES
															  selector:@selector(localizedCaseInsensitiveCompare:)]];
	
	// 5 - Fetch it
	self.fetchedResultsController = [[NSFetchedResultsController alloc]
									 initWithFetchRequest:request
									 managedObjectContext:_managedObjectContext
									 sectionNameKeyPath:nil
									 cacheName:nil];
	
	
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
	_gameDataArray = [[NSMutableArray alloc]initWithArray:results];
	
	NSLog(@"count: %lu", (unsigned long)[_gameDataArray count]);
	NSLog(@"\n");
	
	if ([_gameDataArray count] >0) {
		_game_info = [_gameDataArray objectAtIndex:0];
	}

}





@end
