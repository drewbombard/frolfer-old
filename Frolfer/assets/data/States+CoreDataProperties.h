//
//  States+CoreDataProperties.h
//  
//
//  Created by Drew Bombard on 5/16/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "States.h"

NS_ASSUME_NONNULL_BEGIN

@interface States (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *abbreviation;
@property (nullable, nonatomic, retain) NSString *section;
@property (nullable, nonatomic, retain) NSString *state;

@end

NS_ASSUME_NONNULL_END
