//
//  Players+CoreDataClass.h
//  
//
//  Created by Drew Bombard on 11/21/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Games, Scoring, TotalScore;

NS_ASSUME_NONNULL_BEGIN

@interface Players : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Players+CoreDataProperties.h"
