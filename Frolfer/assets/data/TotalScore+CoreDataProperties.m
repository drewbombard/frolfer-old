//
//  TotalScore+CoreDataProperties.m
//  
//
//  Created by Drew Bombard on 11/15/17.
//
//

#import "TotalScore+CoreDataProperties.h"

@implementation TotalScore (CoreDataProperties)

+ (NSFetchRequest<TotalScore *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"TotalScore"];
}

@dynamic score;
@dynamic scoredBy;
@dynamic onCourse;

@end
