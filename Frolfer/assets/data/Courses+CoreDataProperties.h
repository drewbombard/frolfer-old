//
//  Courses+CoreDataProperties.h
//  
//
//  Created by Drew Bombard on 6/30/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Courses.h"

NS_ASSUME_NONNULL_BEGIN

@interface Courses (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *bathrooms;
@property (nullable, nonatomic, retain) NSString *courseName;
@property (nullable, nonatomic, retain) NSString *coursePhone;
@property (nullable, nonatomic, retain) NSString *courseSectionHead;
@property (nullable, nonatomic, retain) NSString *elevation;
@property (nullable, nonatomic, retain) NSString *foliage;
@property (nullable, nonatomic, retain) NSString *handicapAccessible;
@property (nullable, nonatomic, retain) NSNumber *lengthAlternate;
@property (nullable, nonatomic, retain) NSNumber *lengthMain;
@property (nullable, nonatomic, retain) NSString *locAddress;
@property (nullable, nonatomic, retain) NSString *locCity;
@property (nullable, nonatomic, retain) NSString *locLat;
@property (nullable, nonatomic, retain) NSString *locLong;
@property (nullable, nonatomic, retain) NSString *locState;
@property (nullable, nonatomic, retain) NSString *locStateAbbreviation;
@property (nullable, nonatomic, retain) NSString *locZip;
@property (nullable, nonatomic, retain) NSNumber *numHoles;
@property (nullable, nonatomic, retain) NSString *payToPlay;
@property (nullable, nonatomic, retain) NSString *privateCourse;
@property (nullable, nonatomic, retain) NSString *teeSigns;
@property (nullable, nonatomic, retain) NSString *typeTarget;
@property (nullable, nonatomic, retain) NSString *typeTee;
@property (nullable, nonatomic, retain) CourseHoles *courseholes;
@property (nullable, nonatomic, retain) Games *inGames;

@end

NS_ASSUME_NONNULL_END
