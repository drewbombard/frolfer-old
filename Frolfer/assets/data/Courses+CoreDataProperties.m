//
//  Courses+CoreDataProperties.m
//  
//
//  Created by Drew Bombard on 6/30/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Courses+CoreDataProperties.h"

@implementation Courses (CoreDataProperties)

@dynamic bathrooms;
@dynamic courseName;
@dynamic coursePhone;
@dynamic courseSectionHead;
@dynamic elevation;
@dynamic foliage;
@dynamic handicapAccessible;
@dynamic lengthAlternate;
@dynamic lengthMain;
@dynamic locAddress;
@dynamic locCity;
@dynamic locLat;
@dynamic locLong;
@dynamic locState;
@dynamic locStateAbbreviation;
@dynamic locZip;
@dynamic numHoles;
@dynamic payToPlay;
@dynamic privateCourse;
@dynamic teeSigns;
@dynamic typeTarget;
@dynamic typeTee;
@dynamic courseholes;
@dynamic inGames;

@end
