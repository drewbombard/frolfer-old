//
//  Scoring+CoreDataProperties.m
//  
//
//  Created by Drew Bombard on 5/15/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Scoring+CoreDataProperties.h"

@implementation Scoring (CoreDataProperties)

@dynamic holeNumber;
@dynamic score;
@dynamic scoredBy;

@end
