//
//  Players+CoreDataProperties.h
//  
//
//  Created by Drew Bombard on 11/21/17.
//
//

#import "Players+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Players (CoreDataProperties)

+ (NSFetchRequest<Players *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *playerName;
@property (nullable, nonatomic, copy) NSString *playerNickName;
@property (nullable, nonatomic, retain) NSData *playerPhoto;
@property (nullable, nonatomic, retain) Games *inGames;
@property (nullable, nonatomic, retain) NSSet<Scoring *> *scores;
@property (nullable, nonatomic, retain) TotalScore *totalScore;

@end

@interface Players (CoreDataGeneratedAccessors)

- (void)addScoresObject:(Scoring *)value;
- (void)removeScoresObject:(Scoring *)value;
- (void)addScores:(NSSet<Scoring *> *)values;
- (void)removeScores:(NSSet<Scoring *> *)values;

@end

NS_ASSUME_NONNULL_END
