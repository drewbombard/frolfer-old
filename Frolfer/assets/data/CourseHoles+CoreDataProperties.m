//
//  CourseHoles+CoreDataProperties.m
//  
//
//  Created by Drew Bombard on 5/15/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CourseHoles+CoreDataProperties.h"

@implementation CourseHoles (CoreDataProperties)

@dynamic holeLong;
@dynamic holeNumber;
@dynamic holePar;
@dynamic holeShort;
@dynamic courses;

@end
