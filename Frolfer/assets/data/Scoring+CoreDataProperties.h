//
//  Scoring+CoreDataProperties.h
//  
//
//  Created by Drew Bombard on 5/15/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Scoring.h"

NS_ASSUME_NONNULL_BEGIN

@interface Scoring (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *holeNumber;
@property (nullable, nonatomic, retain) NSNumber *score;
@property (nullable, nonatomic, retain) Players *scoredBy;

@end

NS_ASSUME_NONNULL_END
