//
//  Games+CoreDataProperties.h
//  
//
//  Created by Drew Bombard on 11/15/17.
//
//

#import "Games+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Games (CoreDataProperties)

+ (NSFetchRequest<Games *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *courseChanged;
@property (nullable, nonatomic, copy) NSNumber *currentHole;
@property (nullable, nonatomic, copy) NSNumber *gameComplete;
@property (nullable, nonatomic, copy) NSString *gameCourse;
@property (nullable, nonatomic, copy) NSDate *gameDate;
@property (nullable, nonatomic, copy) NSNumber *gameStarted;
@property (nullable, nonatomic, copy) NSString *gameWinner;
@property (nullable, nonatomic, copy) NSString *leader;
@property (nullable, nonatomic, copy) NSNumber *pageContentLoaded;
@property (nullable, nonatomic, copy) NSNumber *archived;
@property (nullable, nonatomic, retain) Courses *course;
@property (nullable, nonatomic, retain) NSSet<Players *> *playedBy;

@end

@interface Games (CoreDataGeneratedAccessors)

- (void)addPlayedByObject:(Players *)value;
- (void)removePlayedByObject:(Players *)value;
- (void)addPlayedBy:(NSSet<Players *> *)values;
- (void)removePlayedBy:(NSSet<Players *> *)values;

@end

NS_ASSUME_NONNULL_END
