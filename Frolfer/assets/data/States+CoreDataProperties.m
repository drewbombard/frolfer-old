//
//  States+CoreDataProperties.m
//  
//
//  Created by Drew Bombard on 5/16/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "States+CoreDataProperties.h"

@implementation States (CoreDataProperties)

@dynamic abbreviation;
@dynamic section;
@dynamic state;

@end
