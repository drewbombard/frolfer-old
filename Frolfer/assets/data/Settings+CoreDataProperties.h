//
//  Settings+CoreDataProperties.h
//  
//
//  Created by Drew Bombard on 5/24/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Settings.h"

NS_ASSUME_NONNULL_BEGIN

@interface Settings (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *lastBuild;
@property (nullable, nonatomic, retain) NSDate *lastUpdated;

@end

NS_ASSUME_NONNULL_END
