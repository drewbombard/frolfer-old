//
//  TotalScore+CoreDataProperties.h
//  
//
//  Created by Drew Bombard on 11/15/17.
//
//

#import "TotalScore+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface TotalScore (CoreDataProperties)

+ (NSFetchRequest<TotalScore *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *score;
@property (nullable, nonatomic, retain) Players *scoredBy;
@property (nullable, nonatomic, retain) Courses *onCourse;

@end

NS_ASSUME_NONNULL_END
