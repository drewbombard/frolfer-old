//
//  TotalScore+CoreDataClass.h
//  
//
//  Created by Drew Bombard on 11/15/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Courses, Players;

NS_ASSUME_NONNULL_BEGIN

@interface TotalScore : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "TotalScore+CoreDataProperties.h"
