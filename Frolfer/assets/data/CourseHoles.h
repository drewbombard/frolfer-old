//
//  CourseHoles.h
//  
//
//  Created by Drew Bombard on 5/15/16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Courses;

NS_ASSUME_NONNULL_BEGIN

@interface CourseHoles : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "CourseHoles+CoreDataProperties.h"
