//
//  Settings+CoreDataProperties.m
//  
//
//  Created by Drew Bombard on 5/24/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Settings+CoreDataProperties.h"

@implementation Settings (CoreDataProperties)

@dynamic lastBuild;
@dynamic lastUpdated;

@end
