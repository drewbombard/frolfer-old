//
//  CourseHoles+CoreDataProperties.h
//  
//
//  Created by Drew Bombard on 5/15/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CourseHoles.h"

NS_ASSUME_NONNULL_BEGIN

@interface CourseHoles (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *holeLong;
@property (nullable, nonatomic, retain) NSNumber *holeNumber;
@property (nullable, nonatomic, retain) NSNumber *holePar;
@property (nullable, nonatomic, retain) NSNumber *holeShort;
@property (nullable, nonatomic, retain) Courses *courses;

@end

NS_ASSUME_NONNULL_END
