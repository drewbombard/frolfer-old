//
//  Scoring.h
//  
//
//  Created by Drew Bombard on 5/15/16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Players;

NS_ASSUME_NONNULL_BEGIN

@interface Scoring : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Scoring+CoreDataProperties.h"
