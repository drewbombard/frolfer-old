//
//  Players+CoreDataProperties.m
//  
//
//  Created by Drew Bombard on 11/21/17.
//
//

#import "Players+CoreDataProperties.h"

@implementation Players (CoreDataProperties)

+ (NSFetchRequest<Players *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Players"];
}

@dynamic playerName;
@dynamic playerNickName;
@dynamic playerPhoto;
@dynamic inGames;
@dynamic scores;
@dynamic totalScore;

@end
