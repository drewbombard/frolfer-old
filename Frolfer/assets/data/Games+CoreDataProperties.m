//
//  Games+CoreDataProperties.m
//  
//
//  Created by Drew Bombard on 11/15/17.
//
//

#import "Games+CoreDataProperties.h"

@implementation Games (CoreDataProperties)

+ (NSFetchRequest<Games *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Games"];
}

@dynamic courseChanged;
@dynamic currentHole;
@dynamic gameComplete;
@dynamic gameCourse;
@dynamic gameDate;
@dynamic gameStarted;
@dynamic gameWinner;
@dynamic leader;
@dynamic pageContentLoaded;
@dynamic archived;
@dynamic course;
@dynamic playedBy;

@end
