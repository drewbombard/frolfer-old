//
//  SetupViewController.m
//  Frolfer
//
//  Created by Drew Bombard on 8/21/13.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import "SetupViewController.h"

@interface SetupViewController ()

@end

@implementation SetupViewController


-(void)viewDidLoad {
	
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	
	[self customizeInterface];

	_appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

	if (_managedObjectContext == nil) {
		NSManagedObjectContext *context = [_appDelegate managedObjectContext];
		[context setUndoManager:nil];
		_managedObjectContext = context;
	}
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
	
	[super viewWillAppear:animated];
	NSLog(@"\n\n\n SetupViewController: viewWillAppear()");
	
	NSLog(@"location found: %d", _appDelegate.locationFound);
	if (_appDelegate.locationFound == 1) {
		_btnLocalState.hidden = NO;
		_btnLocalState.enabled = YES;
		
		_btnAllStates.hidden = YES;
		_btnAllStates.enabled = NO;
	} else {
		_btnLocalState.hidden = YES;
		_btnLocalState.enabled = NO;

		_btnAllStates.hidden = NO;
		_btnAllStates.enabled = YES;
	}
	
	
	// Setup data arrays for page
	[self fetchGameData];
	
	[self setResetGame];
	
//	// Set game_info
//	if ([_gameDataArray count] >0) {
//		_game_info = [_gameDataArray objectAtIndex:0];
//	} else {
//		_game_info = [NSEntityDescription
//					  insertNewObjectForEntityForName:@"Games"
//					  inManagedObjectContext:_managedObjectContext];
//	}
//	
//	
//	// Set game date
//	if (_game_info.gameDate == nil) {
//		_game_info.gameDate = [NSDate date];
//		[self.managedObjectContext save:nil];  // write to database
//	}
//	
//	if (_game_info.gameComplete == nil || _game_info.gameComplete == 0) {
//		_btnPlayAgain.enabled = NO;
//		[self showHidePlayAgain:0];
//	}
	
	NSLog(@"\n");
	NSLog(@"_gameDataArray: %@", _gameDataArray);
	NSLog(@"_game_info: %@", _game_info);

	[self checkGameStartStatus];
	
	
	
	// Google Analytics
//	id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//	[tracker set:kGAIScreenName value:@"Setup"];
//	[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}


-(void)customizeInterface {
	
	_btnCancelGame.backgroundColor = [Colors get].btnRed;
	_btnCancelGame.titleLabel.textColor = [UIColor whiteColor];
	_btnCancelGame.layer.cornerRadius =  _btnCancelGame.frame.size.height/2;
	_btnCancelGame.layer.masksToBounds = YES;
	
	_btnStartGame.backgroundColor = [Colors get].btnGreen;
	_btnStartGame.titleLabel.textColor = [UIColor whiteColor];
	_btnStartGame.layer.cornerRadius =  _btnStartGame.frame.size.height/2;
	_btnStartGame.layer.masksToBounds = YES;
	
	
	self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
	[self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
	
	self.navigationItem.rightBarButtonItem = nil;
	self.navigationItem.leftBarButtonItem = nil;
	self.navigationItem.hidesBackButton = YES;
}







-(IBAction)startGame:(id)sender {
	
	NSLog(@"_gameDataArray count: %lu", (unsigned long)[_gameDataArray count]);
	NSLog(@"_game_info: %@", _game_info);
	
	
	if (_game_info.gameStarted == nil || _game_info.gameStarted == 0) {
		_game_info.currentHole = [NSNumber numberWithInt: 1];

		//_game_info.gameDate = [NSDate date];
		//_game_info.gameStarted = [NSNumber numberWithInt:1];
		[self.managedObjectContext save:nil];  // write to database
	}
	
	
	// Set current tab
	self.tabBarController.selectedIndex = 0;
}

-(IBAction)cancelGameConfirm:(id)sender {
	
	UIAlertController *alert =   [UIAlertController
								  alertControllerWithTitle:nil
								  message:nil
								  preferredStyle:UIAlertControllerStyleActionSheet];
	
	[self presentViewController:alert animated:YES completion:nil];
	
	UIAlertAction *startOverAction = [UIAlertAction
								   actionWithTitle:NSLocalizedString(@"Yes, I'm Sure", @"OK action")
								   style:UIAlertActionStyleDestructive
								   handler:^(UIAlertAction *action)
								   {
									   NSLog(@"Clear game..");
									   [self cancelGame];
								   }];
	
	
	UIAlertAction *cancelAction = [UIAlertAction
								   actionWithTitle:@"Cancel"
								   style:UIAlertActionStyleCancel
								   handler:^(UIAlertAction * action)
								   {
									   // Do Nothing... user has cancelled deletion
									   NSLog(@"cancel me sucka...");
									   
									   [alert dismissViewControllerAnimated:YES completion:nil];
									   
								   }];
	
	[alert addAction:startOverAction];
	[alert addAction:cancelAction];
}


-(void)cancelGame {
	
	/*
	 * User has chosen to cancel this game
	 * Canelling game will remove all players, and reset the course
	 */
	
	/*
	 * Delete all records of GamePlayers activity
	 */
	[DeleteData deleteFromEntity:@"Players" predicate:nil predicateValue:nil];
	
	/*
	 * Delete all records of GamePlayers activity
	 */
	[DeleteData deleteFromEntity:@"Scoring" predicate:nil predicateValue:nil];
	
	/*
	 * Delete all records of Game activity
	 */
	[DeleteData deleteFromEntity:@"Games" predicate:@"gameComplete" predicateValue:@"0"];
	
	
	[self fetchGameData];
	[self setResetGame];
	[self checkGameStartStatus];
}



/*
 * Check for an active Game, if none... create one.
 */
-(void)setResetGame {
	
	// Set _game_info
	if ([_gameDataArray count] >0) {
		_game_info = [_gameDataArray objectAtIndex:0];
	} else {
		_game_info = [NSEntityDescription
					  insertNewObjectForEntityForName:@"Games"
					  inManagedObjectContext:_managedObjectContext];
	}
	
	// Set _game date
	if (_game_info.gameDate == nil) {
		_game_info.gameDate = [NSDate date];
		[self.managedObjectContext save:nil];  // write to database
	}
	if (_game_info.gameComplete == nil || _game_info.gameComplete == 0) {
		_btnPlayAgain.enabled = NO;
		[self showHidePlayAgain:0];
	}
}




-(void)fetchGameData {
	
	// Date, course name, etc...
	_gameDataArray = [FetchDataArray dataFromEntity:@"Games" predicateName:nil predicateValue:nil predicateType:nil sortName:@"gameDate" sortASC:NULL];
	
	// Number of players entered
	_playerCount = (int)[[FetchDataArray dataFromEntity:@"Players" predicateName:nil predicateValue:nil predicateType:nil sortName:@"playerName" sortASC:nil] count];
}



-(void)checkGameStartStatus {

	[self checkCourseData];
	[self checkPlayerData];
	
	if (_playersReady == YES && _courseReady == YES) {
		_clearStart.hidden = NO;
		
	} else {
		_clearStart.hidden = YES;
	}
	
	if (_game_info && [_game_info.gameComplete  isEqual: @1]) {
		[self showHidePlayAgain:1];
//		_vwPlayAgain.hidden = NO;
	} else {
		[self showHidePlayAgain:0];
	}
	
	
	_appDelegate.currentHole = 1;

	NSLog(@"_playersReady: %@", _playersReady ? @"Yes" : @"No");
}







-(void)checkPlayerData {
	

	NSString *playersToken;
	
	if (_playerCount >1) {
		playersToken = @" player's are ";
	} else {
		playersToken = @" player is ";
	}
	
	if (_playerCount >= 1) {
		
		_playersReady = YES;
//		[_btnStatusPlayers setBackgroundImage:[UIImage imageNamed:@"btn_avatar"] forState:UIControlStateNormal];
//		[_btnStatusPlayers setBackgroundImage:[UIImage imageNamed:@"btn_avatar"] forState:UIControlStateHighlighted];
//
		_lblStatusPlayers.text = [[NSString stringWithFormat:@"%d", _playerCount] stringByAppendingString: [playersToken stringByAppendingString:@"ready"]];
		
	} else {
		_playersReady = NO;
		_lblStatusPlayers.text = @"Select Players";
//		[_btnStatusPlayers setBackgroundImage:[UIImage imageNamed:@"btn_avatar"] forState:UIControlStateNormal];
//		[_btnStatusPlayers setBackgroundImage:[UIImage imageNamed:@"btn_avatar"] forState:UIControlStateHighlighted];
	}
}

-(void)checkCourseData {
	
	
	NSLog(@"\n\n\n");
	NSLog(@"checkCourseData()");
	
	NSLog(@"_game_info: %@", _game_info);
	NSLog(@"\n");
	NSLog(@"\n");
	
	
	_btnCourseIsSelected.enabled = NO;

	
	if (_game_info.course) {
		
		/*
		 * We have a selected course...change the button to refresh
		 */		
		_courseObj = _game_info.course;
		_courseReady = YES;
		_lblSelectedCourse.text = _courseObj.courseName;
		
//		_btnCourseIsSelected.enabled = YES;
//		[_btnAllStates setBackgroundImage:[UIImage imageNamed:@"btn_location"] forState:UIControlStateNormal];
//		[_btnAllStates setBackgroundImage:[UIImage imageNamed:@"btn_location"] forState:UIControlStateHighlighted];
//		[_btnLocalState setBackgroundImage:[UIImage imageNamed:@"btn_location"] forState:UIControlStateNormal];
//		[_btnLocalState setBackgroundImage:[UIImage imageNamed:@"btn_location"] forState:UIControlStateHighlighted];
//
//		NSLog(@"game_info.gameCourse: %@", _game_info.gameCourse);
//		NSLog(@"game_info.gameCourse: %@", _courseObj.courseName);

	} else {
		_courseReady = NO;
		_lblSelectedCourse.text = @"Select a Course";
		
//		_btnCourseIsSelected.enabled = NO;
//		[_btnAllStates setBackgroundImage:[UIImage imageNamed:@"btn_location"] forState:UIControlStateNormal];
//		[_btnAllStates setBackgroundImage:[UIImage imageNamed:@"btn_location"] forState:UIControlStateHighlighted];
//		[_btnLocalState setBackgroundImage:[UIImage imageNamed:@"btn_location"] forState:UIControlStateNormal];
//		[_btnLocalState setBackgroundImage:[UIImage imageNamed:@"btn_location"] forState:UIControlStateHighlighted];
	}
	
}








-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	
	if ([segue.identifier isEqualToString:@"showCourseDetail"]) {
		
		CourseDetailVC *courseDetailsVC = segue.destinationViewController;
		courseDetailsVC.selectedCourse = _courseObj;
		
	} else if ([segue.identifier isEqualToString:@"localStateSegue"]) {
		
		
	//	if (_appDelegate.locationFound == 1) {
			//_lblLocalStateLong.text = _appDelegate.currentStateLong;
			//_selectedState = _appDelegate.currentStateLong;
			
		
		
//		States *selectedState = [_fetchedResultsController objectAtIndexPath: indexPath];
		
		
		
		CoursesViewController *courseVC = segue.destinationViewController;
		courseVC.selectedState = _appDelegate.currentStateLong;
		courseVC.selectedStateAbbreviation = _appDelegate.currentStateShort;
		
		
		
		
		
	} else {
		NSLog(@"Hey! Unidentified segue attempted...");
	}
}




#pragma mark - App Setup

-(int)checkLocalData {
	
	NSLog(@"check_local_data()");
	
	// Check the Employees entity for local data...
	NSFetchRequest *local_request = [[NSFetchRequest alloc] init];
	[local_request setEntity:[NSEntityDescription entityForName:@"Courses" inManagedObjectContext:_managedObjectContext]];
	[local_request setIncludesSubentities:NO]; //Omit subentities. Default is YES (i.e. include subentities)
	
	NSError *error;
	NSUInteger count = [_managedObjectContext countForFetchRequest:local_request error:&error];
	if(count == NSNotFound) {
		//Handle error
	}
	
	if (count > 0) {
		return (int)count;
	} else {
		NSLog(@"Error: %@", error);
		return 0;
	}
}



/*
 * Check for null data and convert to nil for numbers..
 */
-(NSString *)nullValueCheck:dataValue {
	id value = dataValue;
	if (value && [value isKindOfClass:[NSNull class]]) {
		value = nil;
	}
	return value;
}




-(void)fadeIn:(UIView *)fadingView {

	[UILabel beginAnimations:NULL context:nil];
	[UILabel setAnimationDuration:0.1];
	[fadingView setAlpha:1.0];
	[UILabel commitAnimations];
}

-(void)fadeOut:(UIView *)fadingView {
	
	[UILabel beginAnimations:NULL context:nil];
	[UILabel setAnimationDuration:0.3];
	[fadingView setAlpha:0.0];
	[UILabel commitAnimations];
}


-(void)startGameOver {
	
	// reset button views and hide play again
	[self showHidePlayAgain:0];
	
	if (_game_info) {
		
		if (_game_info.pageContentLoaded != nil) {
			
			_game_info.pageContentLoaded = [NSNumber numberWithInt:0];
			_game_info.gameStarted = [NSNumber numberWithInt:0];
			_game_info.gameComplete = [NSNumber numberWithInt:0];
			[self.managedObjectContext save:nil];  // write to database
		}
	}

	
	
	/*
	 * Delete all records of GamePlayers activity
	 */
	[DeleteData deleteFromEntity:@"Scoring" predicate:nil predicateValue:nil];
	
}

-(void)showHidePlayAgain:(int)intShowHide {

	
	if (intShowHide == 0) {
		[self fadeOut:_vwPlayAgain];
		_btnPlayAgain.enabled = NO;
	}
	if (intShowHide == 1) {
		[self fadeIn:_vwPlayAgain];
		_btnPlayAgain.enabled = YES;
	}
	
//	int xPosButtons = 40;
//	[self animateStarBackground:nil
//					   finished:nil
//						context:nil
//			  biddingBackground:_vwClear
//						   xPos:(
//								 _vwPlay.frame.size.width/2 + xPosButtons
//								 )
//						   yPos:(
//								 _vwClear.frame.origin.y +
//								 _vwClear.frame.size.height/2
//								 )];
//	
//	[self animateStarBackground:nil
//					   finished:nil
//						context:nil
//			  biddingBackground:_vwPlay
//						   xPos:(
//								 _appDelegate.screenWidth -
//								 _vwPlay.frame.size.width/2 - xPosButtons
//								 )
//						   yPos:(
//								 _vwClear.frame.origin.y +
//								 _vwClear.frame.size.height/2
//								 )];
//	

}

-(IBAction)playAgain:(id)sender {

	[self clearGameAlert:nil];
	
}



# pragma mark - Animation control(s)
- (void)animateStarBackground:(NSString *)animationID
					 finished:(NSNumber *)finished
					  context:(void *)context
			biddingBackground:(UIView *)biddingBackground
						 xPos:(float)xPos
						 yPos:(float)yPos {
	
	NSLog(@"\n\n");
	NSLog(@"animateStarBackground()");
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.2];
	[UIView setAnimationDelay:0.5];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDelegate:self];
	
	biddingBackground.center = CGPointMake(xPos, yPos);
	
	[UIView commitAnimations];
}








-(void)clearGameAlert:(id)sender {
	NSString *alertMessage = @"Delete all scores and play again?";
	
	UIAlertController *alert = [UIAlertController
								alertControllerWithTitle:@"Play Again?"
								message:alertMessage
								preferredStyle:UIAlertControllerStyleAlert];

	[self presentViewController:alert animated:YES completion:nil];

	UIAlertAction *okAction = [UIAlertAction
							   actionWithTitle:NSLocalizedString(@"Start Over", @"OK action")
							   style:UIAlertActionStyleDefault
							   handler:^(UIAlertAction *action)
							   {
								   [self startGameOver];
								   NSLog(@"OK action");
							   }];
	
	UIAlertAction *cancelAction = [UIAlertAction
								   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
								   style:UIAlertActionStyleCancel
								   handler:^(UIAlertAction *action)
								   {
									   NSLog(@"Cancel action");
								   }];
	alertMessage = nil;
	[alert addAction:okAction];
	[alert addAction:cancelAction];
}


@end
