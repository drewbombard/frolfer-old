//
//  PlayerCell.h
//  Frolfer
//
//  Created by Drew Bombard on 2/22/13.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayerCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblPlayerName;
@property (strong, nonatomic) IBOutlet UILabel *lblPlayerNickName;
@property (strong, nonatomic) IBOutlet UIImageView *imgPlayerPhoto;

@end
