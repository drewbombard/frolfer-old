//
//  OverlayViewController.m
//  Frolfer
//
//  Created by Drew Bombard on 11/29/17.
//  Copyright © 2017 default_method. All rights reserved.
//

#import "OverlayViewController.h"

@interface OverlayViewController ()

@end

@implementation OverlayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)closePlayerStats:(id)sender {
	
	NSLog(@"closePlayerStats");
	[self.delegate playerStatsOverlayWasTapped:self];
}
@end
