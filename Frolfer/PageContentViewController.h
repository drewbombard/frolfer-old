//
//  PageContentViewController.h
//  PageViewDemo
//
//  Created by Simon on 24/11/13.
//  Copyright (c) 2015 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ScoringCell.h"
//#import "PlayerProfileVC.h"

#import "PlayerStatsViewController.h"
#import "OverlayViewController.h"

// Utilities
#import "CoreDataCheck.h"
#import "FetchDataArray.h"
#import "GameStats.h"

// Data
#import "Players+CoreDataClass.h"
#import "Scoring.h"
#import "Games+CoreDataClass.h"
#import "TotalScore+CoreDataClass.h"


@interface PageContentViewController : UIViewController <
										PlayerStatsControllerDelegate,
										OverlayViewControllerDelegate,
										UITableViewDataSource,
										UITableViewDelegate,
										NSFetchedResultsControllerDelegate> {
}

//@property (assign, nonatomic) CGFloat deviceHeight;
@property (assign, nonatomic) CGFloat screenHeight;
@property (assign, nonatomic) CGFloat screenWidth;


@property (strong, nonatomic) AppDelegate *appDelegate;

// Data
@property (strong, nonatomic) Games *game_info;
@property (strong, nonatomic) NSMutableArray *gameDataArray;
@property (strong, nonatomic) Players *selectedPlayer;
@property (strong, nonatomic) NSArray *playerArray;

@property (strong, nonatomic) OverlayViewController *overlayVC;
@property (strong, nonatomic) PlayerStatsViewController *playerStatsVC;

@property (strong, nonatomic) IBOutlet UIButton *btnLeaderBoard;


@property (strong, nonatomic) NSMutableDictionary *highScoreDict;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;



@property (assign, nonatomic) int pageIndex;
@property (assign, nonatomic) int tmpPageIndex;
@property (strong, nonatomic) IBOutlet UILabel *lblCurrentHole;
@property (strong, nonatomic) IBOutlet UILabel *lblScoreLeader;
@property (strong,nonatomic) IBOutlet UITableView* tableView;


// Actions
-(IBAction)decreaseScore:(id)sender;
-(IBAction)increaseScore:(id)sender;


// Misc. Methods
-(void)updateRowScore:(Players *)player_info :(NSString *)modifier :(NSInteger *)selectedPlayerInt :(NSIndexPath *)path;


-(void)controllerWillChangeContent:(NSFetchedResultsController *)controller;
-(void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath: (NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath;
-(void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type;





@end
