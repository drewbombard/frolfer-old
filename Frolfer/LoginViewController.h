//
//  LoginViewController.h
//  Frolfer
//
//  Created by Drew Bombard on 7/30/12.
//  Copyright (c) 2015 default_method All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#import "AppDelegate.h"

// Data
#import "Games+CoreDataClass.h"
#import "States.h"
#import "Courses.h"
#import "Settings.h"

// Utilities
#import "Colors.h"
#import "AppStats.h"
#import "CoreDataCheck.h"
#import "FetchDataArray.h"
#import "WBTabBarController.h"


@interface LoginViewController : UIViewController {
	
	NSTimer *activityTimer;
	BOOL gameSetupNeeded;
}

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingSpinner;

@property (strong, nonatomic) AppDelegate *appDelegate;

@property (strong, nonatomic) IBOutlet UIImageView *imgBackground;
@property (strong, nonatomic) IBOutlet UIView *updateSpinner;

@property (strong, nonatomic) Games *game_info;
@property (strong, nonatomic) NSMutableArray *gameDataArray;

@property (strong, nonatomic) Settings *settings_info;
@property (strong, nonatomic) NSMutableArray *settings_data;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UIView *activityIndicatorView;

-(NSString *)nullValueCheck:dataValue;
-(void)startViewTimer;
-(void)showMainView;

@property (strong, nonatomic) IBOutlet UILabel *lblVersionNum;




// TEMPORARY -- DEBUGG... MOVE to setup
@property (nonatomic, strong) NSDate *date;


@end
