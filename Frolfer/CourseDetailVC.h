//
//  CourseDetailVC.h
//  Frolfer
//
//  Created by Drew Bombard on 7/17/12.
//  Copyright (c) 2015 default_method All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CourseMapViewController.h"


// Data
#import "Courses.h"
#import "Games+CoreDataClass.h"


// Utilities
#import "Colors.h"
#import "FetchDataArray.h"
#import "GameStats.h"
#import "DeleteData.h"



@interface CourseDetailVC : UIViewController <
								NSFetchedResultsControllerDelegate,
								UINavigationControllerDelegate,
								UIScrollViewDelegate,
								UIAlertViewDelegate>



// Course Details

// Views
@property (strong, nonatomic) IBOutlet UIView *vHoles;
@property (strong, nonatomic) IBOutlet UIView *vTarget;
@property (strong, nonatomic) IBOutlet UIView *vTee;
@property (strong, nonatomic) IBOutlet UIView *vElevation;
@property (strong, nonatomic) IBOutlet UIView *vFoliage;
@property (strong, nonatomic) IBOutlet UIView *vLength;
@property (strong, nonatomic) IBOutlet UIView *vLengthAlt;
@property (strong, nonatomic) IBOutlet UIView *vTeeSigns;
@property (strong, nonatomic) IBOutlet UIView *vBathrooms;
@property (strong, nonatomic) IBOutlet UIView *vPrivateCourse;
@property (strong, nonatomic) IBOutlet UIView *vPayToPlay;
@property (strong, nonatomic) IBOutlet UIView *vHandicap;
@property (strong, nonatomic) IBOutlet UIView *vCoursePhone;

// Strings and Labels
@property (strong, nonatomic) NSString *locCity;
@property (strong, nonatomic) IBOutlet UILabel *lblLocCity;

@property (strong, nonatomic) NSString *courseName;
@property (strong, nonatomic) IBOutlet UILabel *lblCourseName;

@property (strong, nonatomic) NSString *numHoles;
@property (strong, nonatomic) IBOutlet UILabel *lblNumHoles;

@property (strong, nonatomic) NSString *typeTarget;
@property (strong, nonatomic) IBOutlet UILabel *lblTarget;

@property (strong, nonatomic) NSString *typeTee;
@property (strong, nonatomic) IBOutlet UILabel *lblTee;

@property (strong, nonatomic) NSString *elevation;
@property (strong, nonatomic) IBOutlet UILabel *lblElevation;

@property (strong, nonatomic) NSString *foliage;
@property (strong, nonatomic) IBOutlet UILabel *lblFoliage;

@property (strong, nonatomic) NSString *lengthMain;
@property (strong, nonatomic) IBOutlet UILabel *lblLength;

@property (strong, nonatomic) NSString *lengthAlternate;
@property (strong, nonatomic) IBOutlet UILabel *lblLengthAlt;

@property (strong, nonatomic) NSString *teeSigns;
@property (strong, nonatomic) IBOutlet UILabel *lblTeeSigns;

@property (strong, nonatomic) NSString *bathrooms;
@property (strong, nonatomic) IBOutlet UILabel *lblBathrooms;

@property (strong, nonatomic) NSString *privateCourse;
@property (strong, nonatomic) IBOutlet UILabel *lblPrivateCourse;

@property (strong, nonatomic) NSString *payToPlay;
@property (strong, nonatomic) IBOutlet UILabel *lblPayToPlay;

@property (strong, nonatomic) NSString *handicapAccessible;
@property (strong, nonatomic) IBOutlet UILabel *lblHandicap;

@property (strong, nonatomic) NSString *coursePhone;
@property (strong, nonatomic) IBOutlet UILabel *lblCoursePhone;
@property (strong, nonatomic) IBOutlet UIButton *btnCoursePhone;



@property (strong, nonatomic) NSString *locZip;
@property (strong, nonatomic) NSString *locLat;
@property (strong, nonatomic) NSString *locLong;
@property (strong, nonatomic) NSString *cityStateZip;
@property (strong, nonatomic) NSString *locCityStateZip;
@property (strong, nonatomic) IBOutlet UILabel *locZipLabel;


// Misc Properties
@property (assign) BOOL gameCourse;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;



// Actions & Buttons
-(IBAction)selectCourse:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectCourse;


// Data
@property (strong, nonatomic) Games *game_info;
@property (strong, nonatomic) NSArray *gameDataArray;

@property (strong, nonatomic) Courses *selectedCourse;
@property (strong, nonatomic) NSArray *courseData;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
