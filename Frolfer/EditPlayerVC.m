//
//  EditPlayerVC.m
//  Frolfer
//
//  Created by Drew Bombard on 8/7/12.
//  Copyright (c) 2015 default_method All rights reserved.
//

#import "EditPlayerVC.h"

@interface EditPlayerVC ()

@end

@implementation EditPlayerVC

// These parameters handle the scrolling when tabbing through input fields
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 140;



#pragma mark - View handling / data loading
-(void)viewDidLoad {
	
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	
	[_txtPlayerName addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
	
	[self customizeInterface];
	
	AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [appDelegate managedObjectContext];
	[context setUndoManager:nil];
	
	_managedObjectContext = context;
		
	_today = [NSDate date];
	
	
	#if TARGET_IPHONE_SIMULATOR
		NSLog(@"Running in Simulator: Disable camera butotn");
	#endif
	
	
	NSLog(@"player %@", _player);
	
	[self displayPlayerInfo];
}

-(void)viewWillAppear:(BOOL)animated
{	
	[super viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
	
	[self setTxtPlayerName:nil];
	[self setTxtPlayerNickName:nil];
	[self setImgPlayer:nil];
	[self setBtnSavePlayer:nil];
	[self setBtnDeletePlayer:nil];
}

- (BOOL)shouldAutorotate
{
	UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
	if (orientation == UIInterfaceOrientationPortrait) {
		// your code for portrait mode
	}
	return YES;
}

//#################################################
//#################################################

-(void)disableSaveButton {
	_btnSavePlayer.enabled = NO;
	_btnSavePlayer.backgroundColor = [Colors get].btnDisabled;
	_btnSavePlayer.tintColor = [UIColor grayColor];
}
-(void)enableButton {
	_btnSavePlayer.enabled = YES;
	_btnSavePlayer.backgroundColor = [Colors get].btnGreen;
	//	_btnSavePlayer.titleLabel.textColor = [UIColor blueColor];
	_btnSavePlayer.tintColor = [UIColor whiteColor];
	
}


-(void)textFieldChanged:(UITextField *)textField {
	
	NSString *value = [_txtPlayerName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	
	if (value.length == 0) {
		[self disableSaveButton];
	} else {
		[self enableButton];
	}
}


-(void)customizeInterface {
	
	[self enableButton];
	
	
	_btnSavePlayer.layer.cornerRadius =  _btnSavePlayer.frame.size.height/6;
	_btnSavePlayer.layer.masksToBounds = YES;
	
	
	_btnDeletePlayer.backgroundColor = [Colors get].btnRed;
	_btnDeletePlayer.titleLabel.textColor = [UIColor whiteColor];
	_btnDeletePlayer.layer.cornerRadius =  _btnDeletePlayer.frame.size.height/6;
	_btnDeletePlayer.layer.masksToBounds = YES;
	
	
	_txtPlayerName.layer.borderWidth = 1.0f;
	_txtPlayerName.layer.cornerRadius = 0.0f;
	_txtPlayerName.layer.masksToBounds = YES;
	_txtPlayerName.layer.borderColor = [[Colors get].gray999 CGColor];
	
	_txtPlayerNickName.layer.borderWidth = 1.0f;
	_txtPlayerNickName.layer.cornerRadius = 0.0f;
	_txtPlayerNickName.layer.masksToBounds = YES;
	_txtPlayerNickName.layer.borderColor = [[Colors get].gray999 CGColor];
}



-(void)displayPlayerInfo {
	
	self.txtPlayerName.text = self.player.playerName;
	self.txtPlayerNickName.text = self.player.playerNickName;
	
	if (self.player.playerPhoto != nil) {
		_imgPlayer.image = [UIImage imageWithData: self.player.playerPhoto];
		
		_lblEdit.textColor = [UIColor whiteColor];
		
		// make new layer to contain shadow and masked image
		_containerLayer = [CALayer layer];
		_containerLayer.shadowColor = [UIColor blackColor].CGColor;
		_containerLayer.shadowRadius = 4.0f;
		_containerLayer.shadowOffset = CGSizeMake(0.0f, 3.0f);
		_containerLayer.shadowOpacity = 0.6f;
		
		// Use the image's layer to mask the image into a circle
		_imgPlayer.layer.cornerRadius = roundf(_imgPlayer.frame.size.width/2.0);
		_imgPlayer.layer.masksToBounds = YES;
		
		// Add masked image layer into container layer so that it's shadowed
		[_containerLayer addSublayer:_imgPlayer.layer];
		
		[_imgPlayerContainer.layer addSublayer:_containerLayer];
	} else {
		_lblEdit.text = @"add";
	}
}








#pragma mark - Save new player
-(IBAction)savePlayer:(id)sender {

	NSLog(@"Telling the EditPlayerTVC Delegate that Save was tapped on the EditPlayerTVC");
    
    // Image data (player's photo)
    NSData * imageData = UIImageJPEGRepresentation(playerImg, 100.0);
	if (imageData) {
		_player.playerPhoto = imageData;
	}
	
	
	_player.playerName = _txtPlayerName.text;
	_player.playerNickName = _txtPlayerNickName.text;
	
	[self.delegate theSaveButtonOnThePlayerDetailTVCWasTapped:self];
	[self.managedObjectContext save:nil];  // write to database
}



-(IBAction)showActionSheetDeleteConfirm:(id)sender {
	
	UIAlertController *alert =   [UIAlertController
								  alertControllerWithTitle:nil
								  message:@"Are you sure you want\n to delete this player?"
								  preferredStyle:UIAlertControllerStyleActionSheet];
	
	[self presentViewController:alert animated:YES completion:nil];
	
	UIAlertAction *deleteAction = [UIAlertAction
								   actionWithTitle:NSLocalizedString(@"Yes, I'm Sure", @"Delete action")
								   style:UIAlertActionStyleDestructive
								   handler:^(UIAlertAction *action)
								   {
									   NSLog(@"Delete player..");
									   [self deletePlayer];
								   }];
	
	
	UIAlertAction *cancelAction = [UIAlertAction
								   actionWithTitle:@"Cancel"
								   style:UIAlertActionStyleCancel
								   handler:^(UIAlertAction * action)
								   {
									   [alert dismissViewControllerAnimated:YES completion:nil];
									   
								   }];
	
	[alert addAction:deleteAction];
	[alert addAction:cancelAction];

}

-(IBAction)showActionSheetImagePicker:(id)sender {
	
	UIAlertController *alert =   [UIAlertController
								  alertControllerWithTitle:nil
								  message:nil
								  preferredStyle:UIAlertControllerStyleActionSheet];
	
	[self presentViewController:alert animated:YES completion:nil];
	
	UIAlertAction *cameraAction = [UIAlertAction
								   actionWithTitle:NSLocalizedString(@"Camera", @"Camera action")
								   style:UIAlertActionStyleDefault
								   handler:^(UIAlertAction *action)
								   {
									   NSLog(@"Show the camera..");
									   [self showCamera:nil];
								   }];
	
	UIAlertAction *photoLibraryAction = [UIAlertAction
								   actionWithTitle:NSLocalizedString(@"Photo Library", @"Photos action")
								   style:UIAlertActionStyleDefault
								   handler:^(UIAlertAction *action)
								   {
									   [self showLibrary:nil];
									   NSLog(@"Show the photo library..");
								   }];
	
	UIAlertAction *clearPhotoAction = [UIAlertAction
								   actionWithTitle:NSLocalizedString(@"Clear Photo", @"Clear action")
								   style:UIAlertActionStyleDestructive
								   handler:^(UIAlertAction *action)
								   {
									   [self clearImage:nil];
									   NSLog(@"Clear the saved image..");
								   }];
	
	UIAlertAction *cancelAction = [UIAlertAction
								   actionWithTitle:@"Cancel"
								   style:UIAlertActionStyleCancel
								   handler:^(UIAlertAction * action)
								   {
									   [alert dismissViewControllerAnimated:YES completion:nil];
									   
								   }];
	
	[alert addAction:cameraAction];
	[alert addAction:photoLibraryAction];
	[alert addAction: clearPhotoAction];
	[alert addAction:cancelAction];
}



-(void)deletePlayer {
	
	/*
	 * User has chosen to delete this player
	 */
	[_managedObjectContext deleteObject:_player];
	
	[self.delegate theDeleteButtonOnEditPlayerVCWasTapped:self];
	[self.managedObjectContext save:nil];  // write to database
}


# pragma mark - Photo handling
-(void)clearImage:(id) sender {
	if (_player.playerPhoto) {
		
		_player.playerPhoto = nil;
		playerImg = nil;
		
		_lblEdit.textColor = [UIColor grayColor];
		_imgPlayer.image = [UIImage imageNamed:@"player_empty"];

		// make new layer to contain shadow and masked image
		_containerLayer.shadowColor = [UIColor clearColor].CGColor;
		_containerLayer.shadowRadius = 0.0f;
		_containerLayer.shadowOffset = CGSizeMake(0.0f,0.0f);
		_containerLayer.shadowOpacity = 0.0f;
	}
}

-(void)showCamera:(id) sender {
	[self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
}

-(void)showLibrary:(id) sender {
	[self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}

- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType {
	
	if (self.imageView.isAnimating) {
		[self.imageView stopAnimating];
	}
	
	if (self.capturedImages.count > 0) {
		[self.capturedImages removeAllObjects];
	}
	
	UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];

	// This makes it go _under_ the tab bar.
	// imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
	imagePickerController.sourceType = sourceType;
	imagePickerController.delegate = self;
	imagePickerController.allowsEditing = YES;

	
	if (sourceType == UIImagePickerControllerSourceTypeCamera) {
		/*
		 The user wants to use the camera interface. Set up our custom overlay view for the camera.
		 */
		imagePickerController.showsCameraControls = YES;
		
		/**
		 * Load the overlay view from the OverlayView nib file.
		 * Self is the File's Owner for the nib file, so the overlayView outlet is set to the main
		 * view in the nib. Pass that view to the image picker controller to use as its overlay view, 
		 * and set self's reference to the view to nil.
		 */
		UIImageView *imgCamera_overlay = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"camera_overlay"]];

		UIView *fullscreenView = [[UIView alloc] initWithFrame:CGRectZero];
		fullscreenView.backgroundColor = [UIColor clearColor];
		fullscreenView.center = CGPointMake((self.view.frame.size.width / 2) - imgCamera_overlay.frame.size.width/2 ,
											(self.view.frame.size.height / 2) - imgCamera_overlay.frame.size.height/2);
		[fullscreenView addSubview:imgCamera_overlay];
		imagePickerController.cameraOverlayView = fullscreenView;
	}
	
	self.imagePickerController = imagePickerController;
	[self.navigationController presentViewController:imagePickerController animated:YES completion:nil];
}






-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {

	// Get the image from the picker
	playerImg = [info objectForKey:UIImagePickerControllerOriginalImage];
	
	// Fix image orientation..
	playerImg = [playerImg fixOrientation];
	
	// Scale the image down to size..
	//playerImg = [self imageWithImage:playerImg scaledToSize:CGSizeMake(90,129)];
	
	// Kill the camera and display in the view..
	_imgPlayer.image = playerImg;
	_imgPlayer.layer.cornerRadius = _imgPlayer.frame.size.width / 2;
	_imgPlayer.clipsToBounds = YES;
//	_imgPlayer.layer.borderWidth = 7.0f;
//	_imgPlayer.layer.borderColor = [UIColor whiteColor].CGColor;
	
	
	
	_lblEdit.textColor = [UIColor whiteColor];
	

	[self dismissViewControllerAnimated:YES completion:NULL];
}


-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {

	[self dismissViewControllerAnimated:YES completion:NULL];
}


-(UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize; {
	
	// Create a graphics image context
	UIGraphicsBeginImageContext(newSize);
	
	// Tell the old image to draw in this new context, with the desired new size
	[image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
	
	// Get the new image from the context
	UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
	
	// End the context
	UIGraphicsEndImageContext();
	
	// Return the new image.
	return newImage;
}




#pragma mark - Keyboard controls

/* Setup the keyboard controls */
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
	NSLog(@"blah....");
	return YES;
}

-(void)setupKeyboardControls {
	
	// Initialize the keyboard controls
	self.keyboardControls = [[BSKeyboardControls alloc] init];
	
	// Set the delegate of the keyboard controls
	self.keyboardControls.delegate = self;
	
	// Add all text fields you want to be able to skip between to the keyboard controls
	// The order of thise text fields are important. The order is used when pressing "Previous" or "Next"
	self.keyboardControls.textFields = [NSArray arrayWithObjects:
										self.txtPlayerName,
										self.txtPlayerNickName,nil ];
	
	// Set the style of the bar. Default is UIBarStyleBlackTranslucent.
	self.keyboardControls.barStyle = UIBarStyleBlackTranslucent;
	
	// Set the tint color of the "Previous" and "Next" button. Default is black.
	self.keyboardControls.previousNextTintColor = [UIColor blackColor];
	
	// Set the tint color of the done button. Default is a color which looks a lot like the original blue color for a "Done" butotn
	self.keyboardControls.doneTintColor = [UIColor colorWithRed:34.0/255.0 green:164.0/255.0 blue:255.0/255.0 alpha:1.0];
	
	// Set title for the "Previous" button. Default is "Previous".
	self.keyboardControls.previousTitle = @"Previous";
	
	// Set title for the "Next button". Default is "Next".
	self.keyboardControls.nextTitle = @"Next";
	
	// Add the keyboard control as accessory view for all of the text fields
	// Also set the delegate of all the text fields to self
	
	
	[self.keyboardControls reloadTextFields];
}

/* Scroll the view to the active text field */
-(void)scrollViewToTextField:(id)textField {
	UITableViewCell *cell = nil;
	if ([textField isKindOfClass:[UITextField class]])
		cell = (UITableViewCell *) ((UITextField *) textField).superview.superview;
	else if ([textField isKindOfClass:[UITextView class]])
		cell = (UITableViewCell *) ((UITextView *) textField).superview.superview;
	//    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
	//    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}

/*
 * The "Done" button was pressed
 * We want to close the keyboard
 */
-(void)keyboardControlsDonePressed:(BSKeyboardControls *)controls {
	[controls.activeTextField resignFirstResponder];
}
/* Either "Previous" or "Next" was pressed
 * Here we usually want to scroll the view to the active text field
 * If we want to know which of the two was pressed, we can use the "direction" which will have one of the following values:
 * KeyboardControlsDirectionPrevious "Previous" was pressed
 * KeyboardControlsDirectionNext "Next" was pressed
 */
-(void)keyboardControlsPreviousNextPressed:(BSKeyboardControls *)controls withDirection:(KeyboardControlsDirection)direction andActiveTextField:(id)textField {
	[textField becomeFirstResponder];
	[self scrollViewToTextField:textField];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
	
	if ([self.keyboardControls.textFields containsObject:textField])
		self.keyboardControls.activeTextField = textField;
	[self scrollViewToTextField:textField];
	
	CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
	CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
	CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
	CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
	CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
	CGFloat heightFraction = numerator / denominator;
	
	if (heightFraction < 0.0) {
		heightFraction = 0.0;
	} else if (heightFraction > 1.0) {
		heightFraction = 1.0;
	}
	
	UIInterfaceOrientation orientation =
	[[UIApplication sharedApplication] statusBarOrientation];
	if (orientation == UIInterfaceOrientationPortrait ||
		orientation == UIInterfaceOrientationPortraitUpsideDown) {
		animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
	} else {
		animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
	}
	CGRect viewFrame = self.view.frame;
	viewFrame.origin.y -= animatedDistance;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
	[self.view setFrame:viewFrame];
	[UIView commitAnimations];
}

-(void)textFieldDidEndEditing:(UITextField *)textField  {
	CGRect viewFrame = self.view.frame;
	viewFrame.origin.y += animatedDistance;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
	[self.view setFrame:viewFrame];
	[UIView commitAnimations];
}


-(IBAction)dismissKeyboard:(id)sender {
	
	NSLog(@"\n\n");
	NSLog(@"dissmissKeyboard()");
	
	[self.txtPlayerName resignFirstResponder];
	[self.txtPlayerNickName resignFirstResponder];
	
	if ([_txtPlayerName.text length] > 0) {
		[self enableButton];
	} else {
		[self disableSaveButton];
	}
}


@end
