//
//  StatesViewController.m
//  Frolfer
//
//  Created by Drew Bombard on 10/20/14.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import "StatesViewController.h"

@interface StatesViewController ()

@end

@implementation StatesViewController


- (void)viewDidLoad {
	
	[super viewDidLoad];
	// Do any additional setup after loading the view.
		
	_appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [_appDelegate managedObjectContext];
	[context setUndoManager:nil];
	_managedObjectContext = context;
	
	
	
	NSLog(@"_gameCourse: %@",_gameCourse ? @"Yes" : @"No");
	
	[self customizeInterface];
}


-(void)viewWillAppear:(BOOL)animated {
	
	[super viewWillAppear:animated];
	
	NSError *error;
	if (![[self fetchedResultsController] performFetch:&error]) {
		// Update to handle the error appropriately.
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		exit(-1);  // Fail
	}
}
-(void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
	self.fetchedResultsController = nil;
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

-(void)customizeInterface {
	self.navigationController.navigationBar.barTintColor = [[Colors get] darkBlue];
	self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
	[self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
}







#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	// Return the number of sections.
	return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
	
	if ([[_fetchedResultsController sections] count] > 0) {
		id <NSFetchedResultsSectionInfo> sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
		return [sectionInfo numberOfObjects];
	} else {
		return 0;
	}
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
	return [sectionInfo name];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
	return [self.fetchedResultsController sectionIndexTitles];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
	return [self.fetchedResultsController sectionForSectionIndexTitle:title atIndex:index];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *simpleTableIdentifier = @"stateCell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
	
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
	}
	
	// Configure the cell...
	States *state_info = [_fetchedResultsController objectAtIndexPath:indexPath];
	cell.textLabel.text = state_info.state;
	
	return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSLog(@"\n didSelectRowAtIndexPath ()\n");
}


// copied from courses
//#pragma makr - Search methods
//- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope {
//	NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchText];
//	searchResults = [courses filteredArrayUsingPredicate:resultPredicate];
//}





#pragma mark - Table data setup
#pragma mark - Data Retreival (fetchedResultsController)
-(NSFetchedResultsController *)fetchedResultsController {
	
	if (_fetchedResultsController != nil) {
		return _fetchedResultsController;
	}
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription
								   entityForName:@"States" 
								   inManagedObjectContext:_managedObjectContext];
	[fetchRequest setEntity:entity];
	
	// Sort it
	NSSortDescriptor *sort = [[NSSortDescriptor alloc]
							  initWithKey:@"state"
							  ascending:YES
							  selector:@selector(localizedCaseInsensitiveCompare:)];

	[fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
	[fetchRequest setFetchBatchSize:20];
 
 	// Setup Fetch
	NSFetchedResultsController *theFetchedResultsController =
	[[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
										managedObjectContext:_managedObjectContext
										  sectionNameKeyPath:@"section"
												   cacheName:nil];
	
	// Fetch it and set results
	self.fetchedResultsController = theFetchedResultsController;
	_fetchedResultsController.delegate = self;
	
	
	return _fetchedResultsController;
}



#pragma mark - View Transitions

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	
	//NSLog(@"Let's move, we are in the segue....");
	NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
	
	if ([segue.identifier isEqualToString:@"localStateSegue"]) {
		
		States *selectedState = [_fetchedResultsController objectAtIndexPath: indexPath];
		
		CoursesViewController *courseVC = segue.destinationViewController;
		courseVC.selectedState = selectedState.state;
		courseVC.selectedStateAbbreviation = selectedState.abbreviation;
		courseVC.gameCourse = _gameCourse;
		
	} else {
		NSLog(@"Unidentified Segue Attempted!");
	}
}

@end
