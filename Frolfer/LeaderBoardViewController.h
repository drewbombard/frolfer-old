//
//  LeaderBoardViewController.h
//  Frolfer
//
//  Created by Drew Bombard on 11/3/17.
//  Copyright (c) 2017 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScoringCompleteCell.h"

// Data
#import "CoreDataCheck.h"
#import "FetchDataArray.h"
#import "Players+CoreDataClass.h"
#import "Scoring.h"
#import "Courses.h"

// Utilities
#import "Colors.h"
#import "FetchDataArray.h"
#import "GameStats.h"
#import "DeleteData.h"


@interface LeaderBoardViewController : UIViewController <
										UITableViewDataSource,
										UITableViewDelegate,
										UIAlertViewDelegate,
										NSFetchedResultsControllerDelegate>

-(IBAction)dismissModal:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *modalTitleBar;

@property NSUInteger pageIndex;
@property (strong,nonatomic) IBOutlet UITableView* tableView;

@property (strong, nonatomic) IBOutlet UIImageView *imgPlayer;
@property (weak, nonatomic) IBOutlet UILabel *lblPlayerName;
@property (weak, nonatomic) IBOutlet UILabel *lblPlayerNickName;

@property (strong, nonatomic) IBOutlet UILabel *lblParLow;
@property (strong, nonatomic) IBOutlet UILabel *lblParAverage;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalPoints;


@property (assign, nonatomic) int totalHoles;

@property (strong, nonatomic) AppDelegate *appDelegate;

@property (strong, nonatomic) NSMutableDictionary *gameStatsDict;
@property (strong, nonatomic) Games *game_info;
@property (strong, nonatomic) NSMutableArray *gameDataArray;


@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;


@end
