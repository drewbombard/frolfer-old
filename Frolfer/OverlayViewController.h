//
//  OverlayViewController.h
//  Frolfer
//
//  Created by Drew Bombard on 11/29/17.
//  Copyright © 2017 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OverlayViewController;
@protocol OverlayViewControllerDelegate <NSObject>
-(void)playerStatsOverlayWasTapped:(OverlayViewController *)controller;
@end

@interface OverlayViewController : UIViewController

- (IBAction)closePlayerStats:(id)sender;

@property (weak, nonatomic) id <OverlayViewControllerDelegate> delegate;


@end
