//
//  LoginViewController.m
//  Frolfer
//
//  Created by Drew Bombard on 7/30/12.
//  Copyright (c) 2015 default_method All rights reserved.
//

#import "LoginViewController.h"



@interface LoginViewController ()

@end

@implementation LoginViewController



- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.

	
	if (_managedObjectContext == nil) {
		_appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
		NSManagedObjectContext *context = [_appDelegate managedObjectContext];
		[context setUndoManager:nil];
		
		_managedObjectContext = context;
	}

	
	// Debugging:
	// Display the documents directory
	#if TARGET_IPHONE_SIMULATOR
		[self temporaryGameSetup];
	#endif
	
	[self customizeInterface];
}


- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
	
	[self setActivityIndicator:nil];
	[self setActivityIndicatorView:nil];
	[self setLblVersionNum:nil];
}


-(void)viewWillAppear:(BOOL)animated {
	
	[super viewWillAppear:animated];
	
	[self loadSettingsData];
	
	int course_count = [CoreDataCheck checkEntity:@"Courses"];
	int settings_count = [CoreDataCheck checkEntity:@"Settings"];
	

	if (
		course_count == 0
		|| settings_count == 0
		|| (_settings_info.lastBuild < [AppStats versionNumber])
		) {
		_updateSpinner.hidden = NO;
	}
}


-(void)viewDidAppear:(BOOL)animated {
	
	[super viewDidAppear:animated];

	
	NSLog(@"\n\n");
	NSLog(@"settings lastBuild: %@",	_settings_info.lastBuild);
	NSLog(@"versionNumber %@",[AppStats versionNumber]);
	//	NSLog(@"build %@",[AppStats build]);
	//	NSLog(@"versionAndBuild %@",[AppStats versionAndBuild]);
	//	NSLog(@"strVersionAndBuild %@",[AppStats strVersionAndBuild]);
	//	NSLog(@"version %@",[AppStats version]);
	NSLog(@"\n\n");
	
	
	
	// Verify non-empty State data
	int state_count = [CoreDataCheck checkEntity:@"States"];
	if (state_count == 0 || state_count == (int)nil) {
		NSLog(@"State count: %i", state_count);
		[self loadStates];
	}
	
	
	int course_count = [CoreDataCheck checkEntity:@"Courses"];
	int settings_count = [CoreDataCheck checkEntity:@"Settings"];
	
	
	NSLog(@"_settings_info.lastBuild: %@",	(NSNumber*)_settings_info.lastBuild);
	NSLog(@"AppStats versionNumber: %@",		(NSNumber*)[AppStats versionNumber]);
	NSLog(@"course_count: %d",				course_count);
	NSLog(@"Asettings_count: %d",			settings_count);
	
	// course_count 0			= NEW app install
	// settings_count 0			= 1.1 app update
	// lastBuild < versionNumber	= Update build... refresh course list
	if (
		((NSNumber *)_settings_info.lastBuild < (NSNumber *)[AppStats versionNumber])
		 || course_count == 0
		|| settings_count == 0
		) {
		
		
		NSLog(@"Course count: %i",		course_count);
		NSLog(@"Settings count: %i",	settings_count);
		
		
		// Delete all records of Courses activity
		// **Note: NEW installs will still be running the Delete function
		// even though there is nothing to delete. Should be fine...
		[DeleteData deleteFromEntity:@"Courses" predicate:nil predicateValue:nil];
		
		// Load a fresh set of course data
		[self loadCourseData];
		
		// Update settings to today, and current build number
		[self updateSettingsData];
		
	} else {

		// No updates needed..
		[self showMainView];
	}
}


- (BOOL)shouldAutorotate
{
	UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
	if (orientation == UIInterfaceOrientationPortrait) {
		// your code for portrait mode
	}
	return YES;
}

//#################################################
//#################################################


-(void)customizeInterface {
	
	_updateSpinner.layer.cornerRadius = 10.0f; // set cornerRadius as you want.
	_updateSpinner.layer.masksToBounds = YES;
	
	
	CAGradientLayer *gradient = [CAGradientLayer layer];
	gradient.frame = self.view.bounds;
	gradient.colors = [NSArray arrayWithObjects:
					   (id)[[UIColor colorWithRed:0.53 green:0.82 blue:0.99 alpha:1] CGColor],
					   (id)[[UIColor colorWithRed:0.9 green:0.95 blue:1 alpha:1] CGColor],
					   nil];
	[self.view.layer insertSublayer:gradient atIndex:0];
}



#pragma mark - App Setup (load data)

-(void)updateSettingsData {
	
	NSLog(@"Settings count: %lu", (unsigned long)[_settings_data count]);

	
	if ([_settings_data count] >0 ) {
		
		_settings_info = [_settings_data objectAtIndex:0];
		
		// Set updated values
		_settings_info.lastUpdated = [NSDate date];
		_settings_info.lastBuild = [AppStats versionNumber];
		
	} else {
	
		Settings *settings = [NSEntityDescription
							  insertNewObjectForEntityForName:@"Settings"
							  inManagedObjectContext:_managedObjectContext];
		
		// Set updated values
		settings.lastUpdated = [NSDate date];
		settings.lastBuild = [AppStats versionNumber];
	}

	[self.managedObjectContext save:nil];  // write to database

	
	// Reload settings data
	[self loadSettingsData];
}

-(void)loadSettingsData {
	
	_settings_data = [FetchDataArray dataFromEntity:@"Settings" predicateName:nil predicateValue:nil predicateType:nil sortName:nil sortASC:nil];

	if ([_settings_data count] >0 ) {
		
		_settings_info = [_settings_data objectAtIndex:0];
		
	}
//	else {
//		
//		// This is presumably the app's first run, or the
//		// first run since adding the "Settings" entity.
//		_settings_info = [NSEntityDescription
//						  insertNewObjectForEntityForName:@"Settings"
//						  inManagedObjectContext:_managedObjectContext];
//		
//		// Set default values
//		//_settings_info.lastUpdated = [NSDate date];
//		//_settings_info.lastBuild = [AppStats versionNumber];
//		
//		[self.managedObjectContext save:nil];  // write to database
//	}
}


//-(void)loadContentUpdates {
//	
//	
//	/*
//	 * Update method only called on apps that have previously
//	 * been launched and need updated data.
//	 */
//	
//	
//
//	NSLog(@"versionNumber %@",[AppStats versionNumber]);
//	NSLog(@"build %@",[AppStats build]);
//	NSLog(@"versionAndBuild %@",[AppStats versionAndBuild]);
//	NSLog(@"strVersionAndBuild %@",[AppStats strVersionAndBuild]);
//	NSLog(@"version %@",[AppStats version]);
//
//	NSLog(@"\n");
//	
//	
//	if ([_settings_info.lastBuild intValue] < [[AppStats versionNumber] intValue]) {
//		
//		// Run update
//		NSLog(@"\n Run data updates...");
//	}
//	
//
//}








-(void)loadStates {
	
	NSError *error = nil;
	NSString *dataPathStates = [[NSBundle mainBundle]
								pathForResource:@"states"
								ofType:@"json"];
	NSMutableArray* jsonDataStates = [NSJSONSerialization
									  JSONObjectWithData:[NSData dataWithContentsOfFile:dataPathStates]
									  options:kNilOptions
									  error:&error];
	
	
	if (![_managedObjectContext save:&error]) {
		NSLog(@"Whoops, couldn't save state data: %@", [error localizedDescription]);
	}
	
	for (NSDictionary *state in [jsonDataStates valueForKey:@"states"]) {
		
		NSFetchRequest *request = [[NSFetchRequest alloc] init];
		NSEntityDescription *entityCourses = [NSEntityDescription
											  entityForName:@"States"
											  inManagedObjectContext:_managedObjectContext];
		[request setEntity:entityCourses];
		
		NSArray *objects = [_managedObjectContext
							executeFetchRequest:request
							error:&error];
		NSLog(@"object count: %lu",(unsigned long)[objects count]);
		
		if (objects == nil || error) {
			NSLog(@"There was an error!");
		}
		
		States *_states = (States *)[NSEntityDescription
									 insertNewObjectForEntityForName:@"States"
									 inManagedObjectContext:_managedObjectContext];
		
		[_states setState:			[state objectForKey:@"name"]];
		[_states setSection:		[state objectForKey:@"section"]];
		[_states setAbbreviation:	[state objectForKey:@"abbreviation"]];
		[_managedObjectContext save:&error];
	}
}


-(void)loadCourseData {
	
	
	NSError *error = nil;
	NSString *dataPath = [[NSBundle mainBundle] pathForResource:@"course_data" ofType:@"json"];
	NSMutableArray *jsonData = [NSJSONSerialization
								JSONObjectWithData:[NSData dataWithContentsOfFile:dataPath]
								options:kNilOptions
								error:&error];

	
	if (![_managedObjectContext save:&error]) {
		NSLog(@"Whoops, couldn't save first run data: %@", [error localizedDescription]);
		NSLog(@"\n\n");
	}
	
	NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
	[numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
	
	NSLog(@"\n");
	

	
	@autoreleasepool {
	
	
		for (NSDictionary *course in [jsonData valueForKey:@"courses"]) {
			
			
			@autoreleasepool {
			
				NSFetchRequest *request = [[NSFetchRequest alloc] init];
				NSEntityDescription *entityCourses = [NSEntityDescription
													  entityForName:@"Courses"
													  inManagedObjectContext:_managedObjectContext];
				[request setEntity:entityCourses];
				
				NSArray *objects = [_managedObjectContext executeFetchRequest:request error:&error];
				if (objects == nil || error) {
					NSLog(@"There was an error!");
				}
				
				Courses *_courses = (Courses *)[NSEntityDescription insertNewObjectForEntityForName:@"Courses"
																			 inManagedObjectContext:_managedObjectContext];
				
				NSLog(@"\n Course Name: %@",[course objectForKey:@"courseName"]);
				
				NSNumber *nmb_numHoles = [numberFormatter numberFromString: [self nullValueCheck:[course objectForKey:@"numHoles"]]];
				NSNumber *nmb_lengthMain = [numberFormatter numberFromString: [self nullValueCheck:[course objectForKey:@"lengthMain"]]];
				NSNumber *nmb_lengthAlt= [numberFormatter numberFromString: [self nullValueCheck:[course objectForKey:@"lengthAlternate"]]];
				
				[_courses setCourseName:			[course objectForKey:@"courseName"]];
				[_courses setCourseSectionHead:		[course objectForKey:@"courseSectionHead"]];
				
				[_courses setNumHoles:				nmb_numHoles];
				[_courses setTypeTarget:			[self nullValueCheck:[course objectForKey:@"typeTarget"]]];
				[_courses setTypeTee:				[self nullValueCheck:[course objectForKey:@"typeTee"]]];
				[_courses setLengthMain:			nmb_lengthMain];
				[_courses setLengthAlternate:		nmb_lengthAlt];
				[_courses setFoliage:				[self nullValueCheck:[course objectForKey:@"foliage"]]];
				[_courses setElevation:				[self nullValueCheck:[course objectForKey:@"elevation"]]];
				[_courses setLocAddress:			[self nullValueCheck:[course objectForKey:@"locAddress"]]];
				[_courses setLocCity:				[course objectForKey:@"locCity"]];
				[_courses setLocState:				[course objectForKey:@"locState"]];
				[_courses setLocStateAbbreviation:	[course objectForKey:@"locStateAbbreviation"]];
				[_courses setLocZip:				[course objectForKey:@"locZip"]];
				[_courses setLocLat:				[course objectForKey:@"locLat"]];
				[_courses setLocLong:				[course objectForKey:@"locLong"]];

				// NEW entities
				[_courses setBathrooms:				[course objectForKey:@"bathrooms"]];
				[_courses setHandicapAccessible:	[course objectForKey:@"handicapAccessible"]];
				[_courses setPayToPlay:				[course objectForKey:@"payToPlay"]];
				[_courses setPrivateCourse:			[course objectForKey:@"privateCourse"]];
				[_courses setTeeSigns:				[course objectForKey:@"teeSigns"]];
				[_courses setCoursePhone:			[course objectForKey:@"coursePhone"]];
				
			}
		}
	}
	
	NSError *err;
	[_managedObjectContext save:&err];
	
	
		
	NSLog(@"\n\n\nI'm all done... data is saved\n\n\n");
	
	
	#if TARGET_IPHONE_SIMULATOR
		// where are you?
		NSLog(@"Documents Directory: %@", [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);
	#endif
		
	
	
	
	[self showMainView];
}











-(void)temporaryGameSetup {
	// where are you?
	NSLog(@"Documents Directory: %@", [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);
}




/*
 * Check for null data and convert to nil for numbers..
 */
-(NSString *)nullValueCheck:dataValue {
	id value = dataValue;
	if (value && [value isKindOfClass:[NSNull class]]) {
		value = nil;
	}
	return value;
}






-(void)startViewTimer {
	[_activityIndicator setAlpha:0];
	activityTimer = [NSTimer scheduledTimerWithTimeInterval:1.75 target:self selector:@selector(showMainView) userInfo:nil repeats:NO];
}


-(void)showMainView {
	
	_updateSpinner.hidden = YES;
	
	gameSetupNeeded = NO;
	_gameDataArray = [FetchDataArray dataFromEntity:@"Games" predicateName:nil predicateValue:nil predicateType:nil sortName:nil sortASC:nil];
	
	NSLog(@"_gameDataArray: %lu", (unsigned long)[_gameDataArray count]);
	
	if ([_gameDataArray count] == 0) {
		gameSetupNeeded = YES;
	} else {
		_game_info = [_gameDataArray objectAtIndex:0];
		
		NSLog(@"game_info: %@", _game_info.gameStarted);
		if (_game_info.gameStarted == 0) {
			gameSetupNeeded = NO;
		}
	}
	
	
	[self performSegueWithIdentifier:@"loginSegue" sender:nil];
}



// This will get called too before the view appears
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

	//NSLog(@"Let's move, we are in the segue....");
	
	
	if ([[segue identifier] isEqualToString:@"loginSegue"]) {
		
		WBTabBarController *destController = segue.destinationViewController;		
		if (gameSetupNeeded == YES) {
			destController.setupNeeded = YES;
		}
        [destController setManagedObjectContext:_managedObjectContext];
	}
}






@end
