//
//  IntroPages.h
//  Frolfer
//
//  Created by Drew Bombard on 4/17/15.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroPages : UIPageViewController <UIPageViewControllerDelegate, UIPageViewControllerDataSource>

@end
