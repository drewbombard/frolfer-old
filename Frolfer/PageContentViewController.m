//
//  PageContentViewController.m
//  PageViewDemo
//
//  Created by Simon on 24/11/13.
//  Copyright (c) 2015 Appcoda. All rights reserved.
//

#import "PageContentViewController.h"

@interface PageContentViewController ()

@end

@implementation PageContentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}


-(void)viewDidLoad
{
	[super viewDidLoad];

	NSLog(@"\n\n\nviewDidLoad()");
	
	_appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [_appDelegate managedObjectContext];
	[context setUndoManager:nil];
	_managedObjectContext = context;
	
	_screenHeight = _appDelegate.screenHeight;
	_screenWidth = _appDelegate.screenWidth;
	
	self.tableView.delegate = self;
	[self setNeedsStatusBarAppearanceUpdate];
	
	
	_btnLeaderBoard.layer.cornerRadius =  _btnLeaderBoard.frame.size.height/6;
	_btnLeaderBoard.layer.masksToBounds = YES;
	
}


-(void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
	
	self.fetchedResultsController = nil;
	_gameDataArray = nil;
}


- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	NSLog(@"\n\n\n PageContentViewController: viewWillAppear()");
	NSLog(@"pageIndex: %lu",(unsigned long)_pageIndex);

	
	_gameDataArray = [FetchDataArray dataFromEntity:@"Games"
									  predicateName:nil
									 predicateValue:nil
									  predicateType:nil
										   sortName:nil
											sortASC:nil];
	
	NSError *error;
	if (![[self fetchedResultsController] performFetch:&error]) {
		// Update to handle the error appropriately.
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		exit(-1);  // Fail
	}
	
	// Add 1 to the page index since it's zero based.
	_tmpPageIndex = _pageIndex + 1;
	
	//_lblCurrentHole.text = [NSString stringWithFormat:@"%d",_tmpPageIndex];
	_lblCurrentHole.text = [NSString stringWithFormat: @"Hole # %@",[NSString stringWithFormat:@"%d",_tmpPageIndex]];
	
	
	if (_gameDataArray == nil) {
		_gameDataArray = [FetchDataArray
						  dataFromEntity:@"Games"
						  predicateName:nil
						  predicateValue:nil
						  predicateType:nil
						  sortName:nil
						  sortASC:nil];
	}
	
	if ([_gameDataArray count] >= 1) {
		_game_info = [_gameDataArray objectAtIndex:0];
		_game_info.currentHole = [NSNumber numberWithInt:_tmpPageIndex];
	
		[self.managedObjectContext save:nil];  // write to database
	}
}


-(void)resetViews
{
	[self.tableView beginUpdates];
	[self.tableView endUpdates];
	
	// Reload table data
	[self.tableView reloadData];
	[self.view setNeedsDisplay];
}





-(void)gameLeaderCheck
{
	_highScoreDict = [GameStats gameLeader:_fetchedResultsController];
	
	NSLog(@"\n_highScoreDict: %@",_highScoreDict);
	NSLog(@"\n");

	NSString *statusMessage;

	if ([[_highScoreDict objectForKey:@"gameStarted"] intValue] == 1) {
		
		switch ([[_highScoreDict objectForKey:@"tie"] intValue]) {
			case 0:
				statusMessage = [NSString stringWithFormat: @"%@ is in the lead \n with %@ total points.",[_highScoreDict objectForKey:@"lowScoringPlayerName"],[[_highScoreDict objectForKey:@"lowScoreTotal"] stringValue]];
				break;
			case 1:
				statusMessage = [NSString stringWithFormat: @"Tie game at %@ points.",[[_highScoreDict objectForKey:@"lowScoreTotal"] stringValue]];
				break;
		}
	} else {
		statusMessage = @"No score yet.\nGet throwing!";
	}

	_lblScoreLeader.text = statusMessage;
}













#pragma mark - Data Retreival (fetchedResultsController)
-(NSFetchedResultsController *)fetchedResultsController
{
	NSLog(@"NSFetchedResultsController()");
	
	if (_fetchedResultsController != nil) {
		return _fetchedResultsController;
	}
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription
								   entityForName:@"Players" inManagedObjectContext:_managedObjectContext];
	[fetchRequest setEntity:entity];
	
	NSSortDescriptor *sort = [[NSSortDescriptor alloc]
							  initWithKey:@"playerName" ascending:YES];
	
	[fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
		
	NSFetchedResultsController *theFetchedResultsController =
	[[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
										managedObjectContext:_managedObjectContext
										  sectionNameKeyPath:nil
												   cacheName:nil];
	
	self.fetchedResultsController = theFetchedResultsController;
	_fetchedResultsController.delegate = self;
	
	NSError *error;
	NSArray *results = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
	_playerArray = [[NSMutableArray alloc]initWithArray:results];
	NSLog(@"_playerArray count: %lu", (unsigned long)[_playerArray count]);
	
	
	return _fetchedResultsController;
}


-(int)playerScoreCount:(Players *)player_predicate :(NSString *)entityName
{
	NSError *error = nil;
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:entityName
											  inManagedObjectContext:_managedObjectContext];
	[request setEntity:entity];
	NSPredicate *predicate;
	if ([entityName isEqual: @"Scoring"]) {
		predicate = [NSPredicate
					 predicateWithFormat:@"(scoredBy == %@ AND holeNumber == %@)",
					 [player_predicate objectID], _game_info.currentHole];
	} else {
		predicate = [NSPredicate predicateWithFormat:@"(scoredBy == %@)", [player_predicate objectID]];
	}
	[request setPredicate:predicate];
	
	NSUInteger count = [_managedObjectContext countForFetchRequest:request error:&error];
	
	if (count == NSNotFound) {
		NSLog(@"Error: %@", error);
		return 0;
	} else {
		return (int)count;
	}
}


















-(void)showPlayerStats:(id)selected_player
{
	// #######################
	// ### Player Stats
	// #######################
	_playerStatsVC = (PlayerStatsViewController *)[[UIStoryboard storyboardWithName:@"Main" bundle:nil]
											 instantiateViewControllerWithIdentifier:@"PlayerStats"];
	_playerStatsVC.delegate = self;
	_playerStatsVC.player = selected_player;
	_playerStatsVC.view.backgroundColor = [UIColor clearColor];
	
	// Create the view off-screen at the bottom
	// Without this, it will animate in from the top
	_playerStatsVC.view.frame = CGRectMake(0,
										   _screenHeight,
										   _playerStatsVC.view.frame.size.width,
										   _playerStatsVC.view.frame.size.height);
	_playerStatsVC.view.alpha = 0.0;
	
	UIBezierPath *maskPath = [UIBezierPath
							  bezierPathWithRoundedRect:_playerStatsVC.statsView.bounds
							  byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight)
							  cornerRadii:CGSizeMake(10.0, 10.0)];
	
	CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
	maskLayer.masksToBounds = NO;
	maskLayer.frame = self.view.bounds;
	maskLayer.path  = maskPath.CGPath;
	maskLayer.shadowOffset = CGSizeMake(0.0f, 1.5f);
	maskLayer.shadowRadius = 2.0;
	maskLayer.shadowOpacity = 1.0;
	_playerStatsVC.statsView.layer.mask = maskLayer;
	[self.view addSubview:_playerStatsVC.view];

	
	
	// #######################
	// ### Background Overlay
	// #######################
	_overlayVC = (OverlayViewController *)[[UIStoryboard storyboardWithName:@"Main" bundle:nil]
										   instantiateViewControllerWithIdentifier:@"Overlay"];
	_overlayVC.view.backgroundColor = [UIColor clearColor];
	_overlayVC.delegate = self;
	
	// Fade in overlay
	[UIView animateWithDuration:0.5f
					 animations:^{self->_overlayVC.view.alpha = 1.0;}
					 completion:nil];
//					 completion:^(BOOL finished){ [self.view addSubview:_overlayVC.view]; }];
	
	UIWindow* window = [UIApplication sharedApplication].keyWindow;
	if (!window) {
		window = [[UIApplication sharedApplication].windows objectAtIndex:0];
	}
	[[[window subviews] objectAtIndex:0] addSubview:_overlayVC.view];
	

	// Fade in stats
	CGFloat viewY = 0;
	switch ((int)_appDelegate.deviceHeight) {
		case 568:
			NSLog(@"iPhone 5/iPhone SE");
			viewY = 10;
			viewY = 74;
			break;
		case 667:
			NSLog(@"iPhone 6/7/8");
			viewY = 60;
			viewY = 124;
			break;
		case 736:
			NSLog(@"iPhone 6/7/8 Plus");
			viewY = 95;
			viewY = 159;
			break;
		case 812:
			NSLog(@"iPhone X");
			viewY = 75;
			viewY = 163;
			break;
		default:
			break;
	}

	[UIView animateWithDuration:0.3f
						  delay:0
						options:UIViewAnimationOptionCurveEaseInOut
					 animations:^{
						 self->_playerStatsVC.view.alpha = 1.0;
						 self->_playerStatsVC.view.frame = CGRectMake(
																0
																,viewY
																	  ,self->_playerStatsVC.view.frame.size.width
																	  ,self->_playerStatsVC.view.frame.size.height);
					 }
					//completion:^(BOOL finished){[self.view addSubview:_playerStatsVC.view];}];
					 completion:^(BOOL finished){[[[window subviews] objectAtIndex:0] addSubview:self->_playerStatsVC.view];}];
	
	
	[self addChildViewController:_playerStatsVC];
	
	//	[playerStats willMoveToParentViewController:self];
	//	[playerStats didMoveToParentViewController:self];
	
	//	NSLog(@"_playerStatsVC before y: %f", _playerStatsVC.statsView.frame.origin.y);
	//	NSLog(@"screen height: %f", _screenHeight);
	//	NSLog(@"view height: %f", _playerStatsVC.view.frame.size.height);
	//	NSLog(@"statsView height: %f", _playerStatsVC.statsView.frame.size.height);
	//	NSLog(@"statsView x: %f", (_screenHeight - _playerStatsVC.statsView.frame.size.height));
	//	NSLog(@"_playerStatsVC before y: %f", _playerStatsVC.statsView.frame.origin.y);
}

-(void)hidePlayerStats:(id)selected_player
{
	NSLog(@"selected_player: %@", selected_player);
	NSLog(@"hidePlayerStats");
	
	[UIView animateWithDuration:0.45f
					 animations:^{self->_overlayVC.view.alpha = 0.0;}
					 completion:^(BOOL finished){ [self->_overlayVC.view removeFromSuperview]; }];
	
	
	[UIView animateWithDuration:0.5f
						  delay:0
						options:UIViewAnimationOptionCurveEaseInOut
					 animations:^{
						 self->_playerStatsVC.view.frame = CGRectMake(0,self->_screenHeight,self->_playerStatsVC.view.frame.size.width,self->_playerStatsVC.view.frame.size.width);
					 }
					 completion:^(BOOL finished){ [self->_playerStatsVC.view removeFromSuperview]; }];

}


#pragma mark - Close Player Stats "modal" view
-(void)playerStatsCloseButtonWasTapped:(PlayerStatsViewController *)statsController
{
	[self hidePlayerStats:nil];
}

-(void)playerStatsOverlayWasTapped:(OverlayViewController *)statsController
{
	[self hidePlayerStats:nil];
}



#pragma mark - Table Data
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	
	Players *selected_player = [_fetchedResultsController objectAtIndexPath:indexPath];
	
	NSLog(@"cell: %@", cell);
	NSLog(@"player_info: %@", selected_player);
	NSLog(@"\n");
	
	[self showPlayerStats:selected_player];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 70;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	id  sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
	return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *cellIdentifier = @"playerCell";
	ScoringCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	
	if (cell == nil) {
		cell = [[ScoringCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
	}
	
	// Alternate row colors
//	if (indexPath.row % 2 == 0) {
//		cell.backgroundColor = [UIColor colorWithWhite:0.980 alpha:0.890];
//	} else {
//		cell.backgroundColor = [UIColor whiteColor];
//	}
	
	// "Clear" row color
	cell.backgroundColor = [UIColor clearColor];
	
	
	[self configureCell:cell atIndexPath:indexPath];
	return cell;
}


-(void)configureCell:(ScoringCell *)cell atIndexPath:(NSIndexPath *)indexPath {
	
	NSLog(@"\n\n\nconfigureCell()");
	
	NSLog(@"indexPath for cell: %@", indexPath);
	NSLog(@"ScoringCell : %@", cell);
	NSLog(@"\n");

	NSString *cell_score;
	Players *player_info = [_fetchedResultsController objectAtIndexPath:indexPath];
	NSSet *scoreSet = player_info.scores;
	
	int holeLeaderCheck = 0;
	for (Scoring *scoreObj in scoreSet){

		NSLog(@"cell_score BEFORE %@: ", cell_score);
		NSLog(@"scoreObj.holeNumber %@: ", _game_info.currentHole);
		NSLog(@"scoreObj.holeNumber %@: ", scoreObj.holeNumber);


		if ([scoreObj.holeNumber intValue] == [_game_info.currentHole intValue]) {
			cell_score = [scoreObj.score stringValue];
			cell.lblPlayerScore.text = cell_score;
			holeLeaderCheck = [GameStats holeLeader:cell_score currentHole:_game_info.currentHole fetchedResults:_fetchedResultsController];
		}
		
		
		NSLog(@"cell_score AFTER %@: ", cell_score);
	}
	NSLog(@"holeLeaderCheck: %d:", holeLeaderCheck);
	NSLog(@"\n");

	if (holeLeaderCheck == 1) {
		cell.imgLeadPlayerCheck.image = [UIImage imageNamed:@"row_check_orange"];
	} else {
		cell.imgLeadPlayerCheck.image = nil;
	}
	
	// Reset back to "--" for display purposes
	if ([cell_score isEqual: @"0"]) {
		cell.lblPlayerScore.text = @"--";
	}
	
	
	
	
	NSString *nickName;
	if ([player_info.playerNickName isEqual: @""]) {
		nickName = nil;
	} else {
		nickName = [@"\"" stringByAppendingFormat:@"%@%@", player_info.playerNickName, @"\""];
	}
	
	cell.lblPlayerName.text = player_info.playerName;
	cell.lblPlayerNickName.text = nickName;
	cell.lblPlayerName.adjustsFontSizeToFitWidth = YES;
	
	
	cell.btnIncreaseScore.tag = indexPath.row;
	cell.btnDecreaseScore.tag = indexPath.row;
	
	UIImage *cdImage = [UIImage imageWithData: player_info.playerPhoto];
	if (cdImage != nil) {
		cell.imgPlayerPhoto.image = cdImage;
		cell.imgPlayerPhoto.layer.cornerRadius = cell.imgPlayerPhoto.frame.size.width / 2;
		cell.imgPlayerPhoto.clipsToBounds = YES;
	} else {
		cell.imgPlayerPhoto.image = [UIImage imageNamed:@"player_row_empty"];
	}
	
	// TRASH ?
	NSLog(@"Score for player %@: %@", player_info.playerName, cell_score);
	NSLog(@"\n");
	
	
	[self gameLeaderCheck];
}




//-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//
//	NSLog(@"Let's move, we are in the segue....");
//	UITableViewCell *cell = (UITableViewCell*)sender;
//	NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
//
//	if ([segue.identifier isEqualToString:@"playerProfileSegue"]) {
//		NSLog(@"playerProfileSegue");
//		NSLog(@"\n\n");
//
//		PlayerProfileVC *profileVC = segue.destinationViewController;
//		profileVC.managedObjectContext = self.managedObjectContext;
//		profileVC.player = [_fetchedResultsController objectAtIndexPath:indexPath];
//
//		profileVC.view.backgroundColor = [[UIColor orangeColor] colorWithAlphaComponent:0.5f];
//		profileVC.modalPresentationStyle = UIModalPresentationFormSheet;
//		profileVC.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
//
//		profileVC.currentHole = _tmpPageIndex;
//
//		NSLog(@"\n\n");
//
//	} else if ([segue.identifier isEqualToString:@"playerProfileTotal"]) {
//
////		CoursesViewController *courseVC = segue.destinationViewController;
////		courseVC.selectedState = _appDelegate.currentStateLong;
////		courseVC.selectedStateAbbreviation = _appDelegate.currentStateShort;
//
//		//self.halfModalTransitioningDelegate = HalfModalTransitioningDelegate(viewController: self, presentingViewController: segue.destination)
//
////		segue.destination.modalPresentationStyle = .custom
////		segue.destination.transitioningDelegate = self.halfModalTransitioningDelegate;
//	} else {
//		NSLog(@"Unidentified Segue Attempted!");
//	}
//
//}



























#pragma mark - Update Score(s)
-(IBAction)increaseScore:(id)sender {

	NSLog(@"PageContentViewController: increaseScore()");

	UIButton *senderButton = (UIButton *)sender;
	NSInteger selectedPlayerTag = senderButton.tag;
	NSIndexPath *path = [NSIndexPath indexPathForRow:senderButton.tag inSection:0];
	
	NSLog(@"current Row=%ld",(long)senderButton.tag);
	NSLog(@"path: %@",path);
	NSLog(@"selectedPlayerTag: %ld",(long)selectedPlayerTag);

	Players *player_info = [_playerArray objectAtIndex:selectedPlayerTag];

	[self updateRowScore:player_info:@"add":&selectedPlayerTag:path];
}

-(IBAction)decreaseScore:(id)sender {

	NSLog(@"PageContentView: decreaseScore()");

	UIButton *senderButton = (UIButton *)sender;
	NSInteger selectedPlayerTag = senderButton.tag;


	NSIndexPath *path = [NSIndexPath indexPathForRow:senderButton.tag inSection:0];
	
	NSLog(@"current Row=%ld",(long)senderButton.tag);
	NSLog(@"path: %@",path);
	
	Players *player_info = [_playerArray objectAtIndex:selectedPlayerTag];
	
	[self updateRowScore:player_info:@"subtract":&selectedPlayerTag:path];
}

-(void)setNewScore:(Players *)player_info :(NSString *)entityName :(NSString *)modifier :(NSInteger *)selectedPlayerInt :(NSIndexPath *)path
{
	NSLog(@"\n\nupdateRowScore()");
	NSLog(@"Update score (%@) for: %@\n", modifier, player_info.playerName);
	NSLog(@"_currentHole: %@",_game_info.currentHole);
	
	// NEW score... we've never scored before.
	Scoring *scoring_data = [NSEntityDescription
							 insertNewObjectForEntityForName:entityName
							 inManagedObjectContext:_managedObjectContext];
	
	//Since this is the first score, always set it to 1
	scoring_data.score = [NSNumber numberWithInt:1];
	scoring_data.scoredBy = player_info;
	if ([entityName isEqual: @"Scoring"]) {
		scoring_data.holeNumber = _game_info.currentHole;
	}
}

-(void)updateExistingScore:(Players *)player_info :(NSString *)entityName :(NSString *)modifier :(NSInteger *)selectedPlayerInt :(NSIndexPath *)path
{
	NSLog(@"\n\nupdateRowScore()");
	NSLog(@"Update score (%@) for: %@\n", modifier, player_info.playerName);
	NSLog(@"_currentHole: %@",_game_info.currentHole);
	
	NSArray *scoreDataArray;
	
	NSError *error = nil;
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	NSEntityDescription *BEntity = [NSEntityDescription entityForName:entityName inManagedObjectContext:_managedObjectContext];
	[fetchRequest setEntity:BEntity];
	
	if ([entityName isEqual: @"Scoring"]) {
		NSPredicate *predicate = [NSPredicate
					 predicateWithFormat:@"(holeNumber = %d AND scoredBy = %@)",
					 [_game_info.currentHole intValue],
					 [player_info objectID]];
		
		[fetchRequest setPredicate:predicate];
		
		NSArray *results = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
		scoreDataArray = [[NSMutableArray alloc]initWithArray:results];
		
		Scoring *score_update = [scoreDataArray objectAtIndex:0];
		int currentScore = [score_update.score intValue];
	
		NSLog(@"current score: %d", currentScore);
		
		if ([modifier isEqual: @"add"]) {
			currentScore++;
		} else {
			// Don't allow negative scores.
			if (currentScore >= 1) {
				currentScore--;
			} else {
				currentScore = 0;
			}
		}
		
		score_update.score = [NSNumber numberWithInt:currentScore];
		score_update.holeNumber = _game_info.currentHole;
		
		
	} else {
		NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(scoredBy = %@)", [player_info objectID]];
		[fetchRequest setPredicate:predicate];
		NSArray *results = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
		scoreDataArray = [[NSMutableArray alloc]initWithArray:results];
		
		TotalScore *total_score_update = [scoreDataArray objectAtIndex:0];
		int currentScore = [total_score_update.score intValue];
		
		NSLog(@"current total score for %@: %d ", player_info.playerName, currentScore);
		
		if ([modifier isEqual: @"add"]) {
			currentScore++;
		} else {
			// Don't allow negative scores.
			if (currentScore >= 1) {
				currentScore--;
			} else {
				currentScore = 0;
			}
		}
		
		NSLog(@"current score: %d", currentScore);

		
		total_score_update.score = [NSNumber numberWithInt:currentScore];
	}
	
	
}

-(void)updateRowScore:(Players *)player_info :(NSString *)modifier :(NSInteger *)selectedPlayerInt :(NSIndexPath *)path
{
	NSLog(@"\n\nupdateRowScore()");
	NSLog(@"Update score (%@) for: %@\n", modifier, player_info.playerName);
	NSLog(@"_currentHole: %@",_game_info.currentHole);
	
	NSLog(@"playerScoreCount: %d", [self playerScoreCount:player_info :@"Scoring"]);
	NSLog(@"TotalScoreCount: %d", [self playerScoreCount:player_info :@"TotalScore"]);
	
	// Check if we've set a score yet... if so, update instead.
	if ([self playerScoreCount:player_info :@"Scoring"] == 0) {
		[self setNewScore:player_info :@"Scoring" :modifier :selectedPlayerInt :path];
	} else {
		[self updateExistingScore:player_info :@"Scoring" :modifier :selectedPlayerInt :path];
	}
	
	// Check if we've set a TotalScore yet
	if ([self playerScoreCount:player_info :@"TotalScore"] == 0) {
		[self setNewScore:player_info :@"TotalScore" :modifier :selectedPlayerInt :path];
	} else {
		[self updateExistingScore:player_info :@"TotalScore" :modifier :selectedPlayerInt :path];
	}
	
	
	// write to database
	[self.managedObjectContext save:nil];
	
	
	// Update cell text..
	NSLog(@"updateRowScore ----> now configureCell ");
	ScoringCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"playerCell"];
	[self configureCell:cell atIndexPath:path];
	

	NSIndexPath *durPath = [NSIndexPath indexPathForRow:path.row inSection:0];
	NSArray *paths = [NSArray arrayWithObject:durPath];

	NSLog(@"durPath: %@", durPath);
	NSLog(@"paths: %@", paths);
	
	
	[self.tableView beginUpdates];
	
	// This was causing rows 'beneath the fold' to load scores before they were set.
	//	[self.tableView reloadRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationNone];
	[self.tableView endUpdates];
	
	[self resetViews];
}











#pragma mark - Boilerplat stuff from Apple/Ray Wenderlich
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
	// The fetch controller is about to start sending change notifications, so prepare the table view for updates.
	[self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
	UITableView *tableView = self.tableView;
 
	switch(type) {
			
		case NSFetchedResultsChangeInsert:
			[tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeDelete:
			[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeUpdate:
			[self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
			break;
			
		case NSFetchedResultsChangeMove:
			[tableView deleteRowsAtIndexPaths:[NSArray
											   arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
			[tableView insertRowsAtIndexPaths:[NSArray
											   arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
	}
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
	switch(type) {
			
		case NSFetchedResultsChangeInsert:
			[self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeDelete:
			[self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
			break;
		case NSFetchedResultsChangeMove:
			NSLog(@"A table item was moved");
			break;
		case NSFetchedResultsChangeUpdate:
			NSLog(@"A table item was updated");
			break;
	}
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
	// The fetch controller has sent all current change notifications, so tell the table view to process all updates.
	[self.tableView endUpdates];
}






// TODO
// Trash ME
//- (IBAction)halfModal:(id)sender {
//    UIView *modalView = [[[NSBundle mainBundle] loadNibNamed:@"TextureView" owner:self options:nil] objectAtIndex:0];
//    [modalView setFrame:CGRectMake(0, CGRectGetHeight(self.view.bounds), CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds))];
//    [self.view addSubview:modalView];
//    [UIView animateWithDuration:1 animations:^{
//        CGRect currentRect = modalView.frame;
//        currentRect.origin.y = 0;
//        [modalView setAlpha:1];
//        [modalView setFrame:currentRect];
//        [self.view bringSubviewToFront:modalView];
//    }];
//
//    [UIView animateWithDuration:1 animations:^{
//        CGRect currentRect = modalView.frame;
//        currentRect.origin.y = self.view.frame.size.width/2;
//        [modalView setAlpha:0.5];
//        [modalView setFrame:currentRect];
//        [modalView removeFromSuperview];
//    }];
//}




// TODO
// TRASH later
//-(IBAction)PlayerStats:(id)sender
//{
//    UIViewController *playerStatsBlurVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayerStatsBlur"];
//
//    UIWindow* window = [UIApplication sharedApplication].keyWindow;
//    if (!window) {
//        window = [[UIApplication sharedApplication].windows objectAtIndex:0];
//    }
//    [[[window subviews] objectAtIndex:0] addSubview:playerStatsBlurVC.view];
//
//
//    UIViewController *playerStatsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayerStatsVC"];
//    [playerStatsVC.view setFrame:CGRectMake(
//                                  20,
//                                  self.view.frame.size.height,
//                                  self.view.frame.size.width,
//                                  self.view.frame.size.height)];
//
//
//
//    [self addChildViewController:playerStatsVC];
//    [self.view addSubview:playerStatsVC.view];
//
//    [[[UIApplication sharedApplication] keyWindow] addSubview:playerStatsVC.view];
//
//
//    [playerStatsVC didMoveToParentViewController:self];
//
//    [UIView animateWithDuration:0.1 animations:^{
//        [playerStatsVC.view setFrame:CGRectMake(
//                                      20,
//                                      250,
//                                      self.view.frame.size.width,
//                                      self.view.frame.size.height)];
//    }];
//}
@end
