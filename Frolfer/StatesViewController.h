//
//  StatesViewController.h
//  Frolfer
//
//  Created by Drew Bombard on 10/20/14.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "CoursesTableViewController.h"
#import "CoursesViewController.h"
#import "Courses.h"
#import "States.h"


@class CourseStateVC;
@protocol CourseStateVCDelegate <NSObject>
@end


@interface StatesViewController : UITableViewController <NSFetchedResultsControllerDelegate> {
	
	NSMutableArray *stateList;
	NSMutableArray *stateIndex;
	
	NSArray *statesArray;
	NSArray *searchResults;
}

@property (assign) BOOL gameCourse;

@property (strong, nonatomic) AppDelegate *appDelegate;


@property (strong, nonatomic) id <CourseStateVCDelegate> delegate;

@property (nonatomic,strong) NSArray *courseArray;
@property (nonatomic,strong) NSArray *courseStateArray;

@property (strong, nonatomic) Courses *selectedCourse;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;




@end
