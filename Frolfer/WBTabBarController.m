//
//  WBTabBarController.m
//  CustomTabBar
//
//  Created by Tito Ciuro on 4/21/12.
//
// Copyright (c) 2012 Webbo, L.L.C. All rights reserved.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE
//

#import "WBTabBarController.h"



@interface WBTabBarController ()

@end

@implementation WBTabBarController

@synthesize plusController;
@synthesize centerButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

// In the initialization section, set the delegate
- (id) init {
	self = [super init];
	if (self) {
		self.delegate = self;
	}
	return self;
}






- (void)viewDidLoad
{
	[super viewDidLoad];

//	[self addCenterButtonWithImage:[UIImage imageNamed:@"btn_playTEMP.png"] highlightImage:[UIImage imageNamed:@"btn_playTEMP.png"] target:self action:@selector(buttonPressed:)];

	
	UIViewController *StatesViewController = [[UIViewController alloc] init];
	UIViewController *SetupViewController = [[UIViewController alloc] init];
	UIViewController *InfoViewController = [[UIViewController alloc] init];


	
	// Set unselected states on tab bar items
	UITabBarItem *item0 = [self.tabBar.items objectAtIndex:0];
	item0.image = [[UIImage imageNamed:@"tab_play-off"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
	
	UITabBarItem *item1 = [self.tabBar.items objectAtIndex:1];
	item1.image = [[UIImage imageNamed:@"tab_gear-off"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
	
	UITabBarItem *item2 = [self.tabBar.items objectAtIndex:2];
	item2.image = [[UIImage imageNamed:@"tab_location-off"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];

	UITabBarItem *item3 = [self.tabBar.items objectAtIndex:3];
	item3.image = [[UIImage imageNamed:@"tab_more-off"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];

	
	NSArray *array = [NSArray arrayWithObjects: StatesViewController, SetupViewController, InfoViewController, nil];

	UITabBarController* tab = [[UITabBarController alloc] init];
	tab.viewControllers = array;
	tab.delegate = self; // <-- don't forget set delegate for TabBarController

	
	// if no game data... send user to setup screen.
	if (_setupNeeded) {
		[self setSelectedIndex:1];
	}
}


- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}


- (BOOL)shouldAutorotate
{
	UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
	if (orientation == UIInterfaceOrientationPortrait) {
		// your code for portrait mode
	}
	return YES;
}

// Create a custom UIButton and add it to the center of our tab bar
- (void)addCenterButtonWithImage:(UIImage *)buttonImage highlightImage:(UIImage *)highlightImage target:(id)target action:(SEL)action
{
	UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
	button.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
	button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
	[button setBackgroundImage:buttonImage forState:UIControlStateNormal];
	[button setBackgroundImage:highlightImage forState:UIControlStateHighlighted];

	CGFloat heightDifference = buttonImage.size.height - self.tabBar.frame.size.height;
	if (heightDifference < 0) {
		button.center = self.tabBar.center;
	} else {
		CGPoint center = self.tabBar.center;
		center.y = center.y - heightDifference/2.0;
		//center.x = 0;
		center.x = 50;
		center.x = (buttonImage.size.width/2.0);
		button.center = center;
		
		
		

	}

	[button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];

	[self.view addSubview:button];
	self.centerButton = button;
}

- (void)buttonPressed:(id)sender {
	[self setSelectedIndex:0];
	[self performSelector:@selector(doHighlight:) withObject:sender afterDelay:0];
	
	NSLog(@"\n");
	NSLog(@"\n");

	NSLog(@"navcontroller: %@",self.navigationController);
	
//	[[self navigationController] popViewControllerAnimated:YES];
//	[self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:2] animated:YES];

	
	//SetupViewController *setupVC;
//	[setupVC.view setNeedsDisplay];
	
	//	[viewController.view setNeedsDisplay];
}




- (void)doHighlight:(UIButton*)b {
	[b setHighlighted:YES];
}

- (void)doNotHighlight:(UIButton*)b {
	[b setHighlighted:NO];
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
	if(self.tabBarController.selectedIndex != 1){
		[self performSelector:@selector(doNotHighlight:) withObject:centerButton afterDelay:0];
	}
}

- (BOOL)tabBarHidden {
	return self.centerButton.hidden && self.tabBar.hidden;
}

- (void)setTabBarHidden:(BOOL)tabBarHidden {
	self.centerButton.hidden = tabBarHidden;
	self.tabBar.hidden = tabBarHidden;
}

@end
