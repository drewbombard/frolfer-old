//
//  PlayerStatsViewController.h
//  Frolfer
//
//  Created by Drew Bombard on 11/28/17.
//  Copyright © 2017 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>

// Data
#import "CoreDataCheck.h"
#import "FetchDataArray.h"
#import "Players+CoreDataClass.h"
#import "Scoring.h"
#import "Courses.h"
#import "TotalScore+CoreDataClass.h"

// Utilities
#import "Colors.h"
#import "FetchDataArray.h"
#import "GameStats.h"


@class PlayerStatsViewController;
@protocol PlayerStatsControllerDelegate <NSObject>
-(void)playerStatsCloseButtonWasTapped:(PlayerStatsViewController *)controller;
@end



@interface PlayerStatsViewController : UIViewController <NSFetchedResultsControllerDelegate> {}


- (IBAction)closePlayerStats:(id)sender;
@property (weak, nonatomic) id <PlayerStatsControllerDelegate> delegate;

@property (strong, nonatomic) IBOutlet UIView *statsView;

@property (assign, nonatomic) int currentHole;
@property (assign, nonatomic) int totalHoles;

@property (nonatomic, strong) IBOutlet UILabel *progressLabel;
@property (strong, nonatomic) IBOutlet UIProgressView *progressView;


@property (strong, nonatomic) Games *game_info;
@property (strong, nonatomic) NSArray *gameDataArray;
@property (strong, nonatomic) NSMutableDictionary *gameStatsDict;

@property (strong, nonatomic) Players *player;
@property (strong, nonatomic) NSArray *playerData;
@property (strong, nonatomic) NSArray *selectedGamePlayerData;

@property (strong, nonatomic) NSArray *allPlayersArr;
@property (strong, nonatomic) NSMutableDictionary *allPlayersDict;

@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (strong, nonatomic) IBOutlet UIImageView *imgPlayer;
@property (strong, nonatomic) IBOutlet UILabel *lblPlayerName;
@property (weak, nonatomic) IBOutlet UILabel *lblPlayerNickName;

@property (strong, nonatomic) IBOutlet UILabel *lblTotalPoints;
@property (strong, nonatomic) IBOutlet UILabel *lblRank;
@property (strong, nonatomic) IBOutlet UILabel *lblParLow;
@property (strong, nonatomic) IBOutlet UILabel *lblParAverage;


@end
