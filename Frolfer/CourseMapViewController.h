//
//  CourseMapViewController.h
//  Frolfer
//
//  Created by Drew Bombard on 11/3/12.
//  Copyright (c) 2016 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>


@interface CourseMapViewController : UIViewController <MKMapViewDelegate,CLLocationManagerDelegate>

@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) CLLocationManager *locationManager;


@property (strong, nonatomic) NSString *locLat;
@property (strong, nonatomic) NSString *locLong;
@property (strong, nonatomic) NSString *courseName;
@property (strong, nonatomic) NSString *locCityState;

@end
