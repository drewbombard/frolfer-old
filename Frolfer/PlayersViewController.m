//
//  PlayersViewController.m
//  Frolfer
//
//  Created by Drew Bombard on 8/10/12.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import "PlayersViewController.h"

@interface PlayersViewController ()

@end

@implementation PlayersViewController


-(void)viewDidLoad {
	
	[super viewDidLoad];
	
	AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [appDelegate managedObjectContext];
	[context setUndoManager:nil];
	_managedObjectContext = context;

	
	[self customizeInterface];
	
	self.tableView.delegate = self;
}

-(void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
	self.fetchedResultsController = nil;
}



- (void)viewWillAppear:(BOOL)animated {
	
	[super viewWillAppear:animated];
	
	NSError *error;
	if (![[self fetchedResultsController] performFetch:&error]) {
		// Update to handle the error appropriately.
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		exit(-1);  // Fail
	}
	
	[self setupPlayerCount];
	
	// Google Analytics
//	id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//	[tracker set:kGAIScreenName value:@"PlayersList"];
//	[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}



-(void)resetViews {
	
	NSLog(@"\n\nresetViews()");
	
	[self.tableView beginUpdates];
	[self.tableView endUpdates];
	
	
	// Reload table data
	[self.tableView reloadData];
	[self.view setNeedsDisplay];
}


-(void)setupPlayerCount {
	
	// Number of players entered
	_playerCount = (int)[_fetchedResultsController.fetchedObjects count];

	if (_playerCount >= 1) {
		_lblPlayerCount.text = [NSString stringWithFormat:@"%d", _playerCount];
		
	} else {
		_lblPlayerCount.text = @"- -";
	}
}


#pragma mark - Data Retreival (fetchedResultsController)
-(NSFetchedResultsController *)fetchedResultsController {
	
	if (_fetchedResultsController != nil) {
		return _fetchedResultsController;
	}
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription
								   entityForName:@"Players" inManagedObjectContext:_managedObjectContext];
	[fetchRequest setEntity:entity];
	
	NSSortDescriptor *sort = [[NSSortDescriptor alloc]
							  initWithKey:@"playerName"
							  ascending:YES
							  selector:@selector(localizedCaseInsensitiveCompare:)];
	
	// Sort it
	[fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
	[fetchRequest setFetchBatchSize:20];

	// Setup and fetch
	NSFetchedResultsController *theFetchedResultsController =
	[[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
										managedObjectContext:_managedObjectContext
										  sectionNameKeyPath:nil
												   cacheName:nil];
	
	// Set results
	self.fetchedResultsController = theFetchedResultsController;
	_fetchedResultsController.delegate = self;
	
	return _fetchedResultsController;
}



#pragma mark - Table Data

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
		[cell setSeparatorInset:UIEdgeInsetsZero];
	}
	
	if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
		[cell setLayoutMargins:UIEdgeInsetsZero];
	}
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 70;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	id  sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
	return [sectionInfo numberOfObjects];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *cellIdentifier = @"playerCell";
	PlayerCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

	if (cell == nil) {
		cell = [[PlayerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
	}
	if (indexPath.row % 2 == 0) {
		cell.backgroundColor = [UIColor colorWithWhite:0.980 alpha:0.890];
	} else {
		cell.backgroundColor = [UIColor whiteColor];
	}

	
	[self configureCell:cell atIndexPath:indexPath];
	return cell;
}

-(void)configureCell:(PlayerCell *)cell atIndexPath:(NSIndexPath *)indexPath {
	
	NSLog(@"\n\n\nconfigureCell()");
	
	NSLog(@"indexPath for cell: %@", indexPath);
	NSLog(@"PlayerCell : %@", cell);
	NSLog(@"\n");
	

	Players *player_info = [_fetchedResultsController objectAtIndexPath:indexPath];
	
	NSString *nickName;
	if ([player_info.playerNickName isEqual: @""]) {
		nickName = nil;
	} else {
		nickName = [@"\"" stringByAppendingFormat:@"%@%@", player_info.playerNickName, @"\""];
	}
	
	cell.lblPlayerName.text = player_info.playerName;
	cell.lblPlayerNickName.text = nickName;
	cell.lblPlayerName.adjustsFontSizeToFitWidth = YES;
	
	
	UIImage *cdImage = [UIImage imageWithData: player_info.playerPhoto];
	if (cdImage != nil) {
		cell.imgPlayerPhoto.image = cdImage;
		cell.imgPlayerPhoto.layer.cornerRadius = cell.imgPlayerPhoto.frame.size.width / 2;
		cell.imgPlayerPhoto.clipsToBounds = YES;
	} else {
		cell.imgPlayerPhoto.image = [UIImage imageNamed:@"player_row_empty"];
	}
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {

    if (editingStyle == UITableViewCellEditingStyleDelete) {

        [self.tableView beginUpdates]; // Avoid  NSInternalInconsistencyException

        // Delete the role object that was swiped
		Players *playerToDelete = [_fetchedResultsController objectAtIndexPath: indexPath];
        [self.managedObjectContext deleteObject:playerToDelete];
        [self.managedObjectContext save:nil];

        // Delete the (now empty) row on the table
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
							  withRowAnimation:UITableViewRowAnimationFade];
		
		//NSLog(@"count: %lu", [_fetchedResultsController.fetchedObjects count]);
		if (!_fetchedResultsController ||
			[_fetchedResultsController.fetchedObjects count] == 0) {
			
			[self setupPlayerCount];
		}
        [self.tableView endUpdates];
		
		
    }
}

#pragma mark - Boilerplat stuff from Apple/Ray Wenderlich
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
	// The fetch controller is about to start sending change notifications, so prepare the table view for updates.
	[self.tableView beginUpdates];
}
- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
 
	UITableView *tableView = self.tableView;
 
	switch(type) {
			
		case NSFetchedResultsChangeInsert:
			[tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeDelete:
			[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeUpdate:
			[self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
			break;
			
		case NSFetchedResultsChangeMove:
			[tableView deleteRowsAtIndexPaths:[NSArray
											   arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
			[tableView insertRowsAtIndexPaths:[NSArray
											   arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
	}
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
 
	switch(type) {
			
		case NSFetchedResultsChangeInsert:
			[self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeDelete:
			[self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
			break;
		case NSFetchedResultsChangeMove:
			NSLog(@"A table item was moved");
			break;
		case NSFetchedResultsChangeUpdate:
			NSLog(@"A table item was updated");
			break;
	}
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
	// The fetch controller has sent all current change notifications, so tell the table view to process all updates.
	[self.tableView endUpdates];
}




-(void)customizeInterface {

	[self.tableView setSeparatorInset:UIEdgeInsetsZero];

	
	self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}



//@property (strong, nonatomic) IBOutlet UILabel *lblStatusPlayers;















#pragma mark - Segue's
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	
	// note that "sender" will be the tableView cell that was selected
	UITableViewCell *cell = (UITableViewCell*)sender;
	NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
	
	
	
	NSLog(@"Let's move, we are in the segue....");
	NSLog(@"Segue: %@", segue.identifier);
	NSLog(@"indexPath: %@", indexPath);
	NSLog(@"\n");
	
	
	if ([segue.identifier isEqualToString:@"addPlayer"]) {
		
		NSLog(@"Setting PlayersViewController as a delegate of AddPlayerVC");
        
        AddPlayerVC *AddPlayerVC = segue.destinationViewController;
        AddPlayerVC.delegate = self;
        AddPlayerVC.managedObjectContext = self.managedObjectContext;
		
	} else if ([segue.identifier isEqualToString:@"editPlayer"]) {
		
		NSLog(@"Setting PlayersViewController as a delegate of EditPlayerVC");
		
		EditPlayerVC *EditPlayerVC = segue.destinationViewController;
		EditPlayerVC.delegate = self;
		EditPlayerVC.managedObjectContext = self.managedObjectContext;
		
// TRASH
//		self.selectedPlayer = [_playerArray objectAtIndex:indexPath.row];
		
		self.selectedPlayer = [_fetchedResultsController objectAtIndexPath: indexPath];
		EditPlayerVC.player = self.selectedPlayer;
		
		NSLog(@"Passing selected player (%@) to EditPlayerVC", self.selectedPlayer.playerName);
		
	} else {
		NSLog(@"Hey! Unidentified segue attempted!");
		NSLog(@"\n");
	}
}



#pragma mark - Add
- (void)theSaveButtonOnThePlayerAddTVCWasTapped:(AddPlayerVC *)controller {
    // do something here like refreshing the table or whatever
    
    // close the delegated view
    [controller.navigationController popViewControllerAnimated:YES];
}
- (void)theSaveAndAddButtonWasTapped:(AddPlayerVC *)controller {
    // do something here like refreshing the table or whatever
    
    // close the delegated view
    [controller.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Edit 
- (void)theSaveButtonOnThePlayerDetailTVCWasTapped:(EditPlayerVC *)controller {
    // do something here like refreshing the table or whatever
    
    // close the delegated view
    [controller.navigationController popViewControllerAnimated:YES];
}
-(void)theAddButtonOnEditPlayerVCWasTapped:(EditPlayerVC *)controller {
	// do something here like refreshing the table or whatever
    
    // close the delegated view
    [controller.navigationController popViewControllerAnimated:YES];
}
-(void)theDeleteButtonOnEditPlayerVCWasTapped:(EditPlayerVC *)controller {
	// do something here like refreshing the table or whatever
    
    // close the delegated view
    [controller.navigationController popViewControllerAnimated:YES];
}
-(void)theRemoveButtonOnEditPlayerVCWasTapped:(EditPlayerVC *)controller {
    // do something here like refreshing the table or whatever
    
    // close the delegated view
    [controller.navigationController popViewControllerAnimated:YES];
}



@end
