//
//  SetupViewController.h
//  Frolfer
//
//  Created by Drew Bombard on 8/21/13.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>


#import "CourseDetailVC.h"
#import "WBTabBarController.h"
#import "CoursesViewController.h"

// Data
#import "Courses.h"
#import "Games+CoreDataClass.h"
#import "Players+CoreDataClass.h"

// Utilities
#import "DeleteData.h"
#import "CoreDataCheck.h"
#import "FetchDataArray.h"


@interface SetupViewController : UIViewController <NSFetchedResultsControllerDelegate,UIActionSheetDelegate> {
	
}

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;


@property (strong, nonatomic) IBOutlet UIButton *btnClear;


@property (strong, nonatomic) IBOutlet UIView *vwClear;
@property (strong, nonatomic) IBOutlet UIView *vwPlay;
@property (weak, nonatomic) IBOutlet UIView *vwPlayAgain;


@property (strong, nonatomic) NSDate *today;

@property (strong, nonatomic) IBOutlet UIView *clearStart;


@property (strong, nonatomic) Games *game_info;
@property (strong, nonatomic) Courses *courseObj;
@property (strong, nonatomic) NSArray *gameDataArray;

@property (assign, nonatomic) int playerCount;
@property (assign) BOOL playersReady;
@property (assign) BOOL courseReady;


@property (strong, nonatomic) AppDelegate *appDelegate;


@property (strong, nonatomic) IBOutlet UIButton *btnStartGame;
@property (strong, nonatomic) IBOutlet UIButton *btnCancelGame;
@property (strong, nonatomic) IBOutlet UIButton *btnPlayAgain;

@property (strong, nonatomic) IBOutlet UIButton *btnLocalState;
@property (strong, nonatomic) IBOutlet UIButton *btnAllStates;

@property (strong, nonatomic) IBOutlet UIButton *btnCourseIsSelected;


@property (strong, nonatomic) IBOutlet UIButton *btnStatusPlayers;





@property (strong, nonatomic) IBOutlet UILabel *lblSelectedCourse;
@property (strong, nonatomic) IBOutlet UILabel *lblStatusPlayers;


-(void)checkPlayerData;
-(void)checkCourseData;
-(void)checkGameStartStatus;

-(IBAction)startGame:(id)sender;
-(IBAction)cancelGameConfirm:(id)sender;




-(NSString *)nullValueCheck:dataValue;

@end
