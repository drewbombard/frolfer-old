//
//  AppDelegate.h
//  Frolfer
//
//  Created by Drew Bombard on 8/21/13.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "CLPlacemark+ShortState.h"

// Data
#import "Games+CoreDataClass.h"

// Utilities
#import "Colors.h"
#import "FetchDataArray.h"


// Google
@import Firebase;





@interface AppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate> {
	
	CLLocationManager *locationManager;
	CLGeocoder *geocoder;
	CLPlacemark *placemark;
}



// Current location of user
@property (nonatomic) int locationFound;
@property (strong, nonatomic) NSString *currentZip;
@property (strong, nonatomic) NSString *currentStateLong;
@property (strong, nonatomic) NSString *currentStateShort;

@property (strong, nonatomic) NSString *currentLat;
@property (strong, nonatomic) NSString *currentLong;



@property (strong, nonatomic) UIWindow *window;



@property (nonatomic) int currentHole;

// TRASH / TEMP ?!?!
@property (strong, nonatomic) Games *game_info;
@property (strong, nonatomic) NSArray *gameDataArray;


@property (assign) CGFloat deviceHeight;
@property (assign) CGFloat screenHeight;
@property (assign) CGFloat screenWidth;

@property (strong, nonatomic) NSString *imgBackground;
@property (assign) BOOL isPhoneLegacy;
@property (assign) BOOL isPhone5;
@property (assign) BOOL isPhone6;
@property (assign) BOOL isPhone6Plus;
@property (assign) BOOL isPhoneX;


@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;



@end
