//
//  PlayersViewController.h
//  Frolfer
//
//  Created by Drew Bombard on 8/10/12.
//  Copyright (c) 2015 default_method. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AddPlayerVC.h"
#import "EditPlayerVC.h"

// Data
#import "Players+CoreDataClass.h"
#import "PlayerCell.h"

// Utilities
#import "DeleteData.h"
#import "CoreDataCheck.h"
#import "FetchDataArray.h"



@interface PlayersViewController : UITableViewController
					<AddPlayerVCDelegate,
					EditPlayerVCDelegate,NSFetchedResultsControllerDelegate>


@property (assign, nonatomic) int playerCount;
@property (strong, nonatomic) IBOutlet UILabel *lblPlayerCount;


// Player data
@property (strong, nonatomic) Players *selectedPlayer;
@property (assign) NSInteger selectedIndex;
@property (nonatomic,strong) NSArray *playerArray;


@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;


@end
